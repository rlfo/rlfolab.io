import * as parser from '../parser/index.js'
import {commFunc} from '../state/util.js'
import {replaceExt} from '../lib/index.js'
import path from 'path'
import {mix} from '../lib/index.js'

var Frm = superclass => class extends superclass {
	async loadFrm(sptContent, arr) {
		var frmContent = []
		for (let val of arr) {
			let coll = this.sortFrm(sptContent, val)
			frmContent = frmContent.concat(coll)
		}
		for (let {pID} of frmContent) {
			this.game.load.image(pID)
		}
		await this.loadComplete()
		return frmContent
	}
	
	addFrm(frmContent, arr, sptFilename) {
		var {spriteSheet, frameData} = Spt.genBitmap(this.game, `${sptFilename} -> ${arr.toString()}`, frmContent)
		var sprite = this.game.add.sprite(this.data.x, this.data.y, spriteSheet, 0, this)
		for (let val of arr) {
			let indexOfFrame = this.getIndexOfFrm(frmContent, val)
			let duration = 1000 / ((frmContent[indexOfFrame[0]].duration / this.originFps) * 1000)
			sprite.animations.add(val, indexOfFrame, duration, true)
		}
		this.sprite = sprite
		this.game.physics.arcade.enable(sprite)
		sprite.body.collideWorldBounds = true
	}
	
	getIndexOfFrm(frmContent, val) {
		var arr = this.sortFrm(frmContent, val)
		return _.map(arr, obj => {
			return frmContent.findIndex(({fID}) => fID == obj.fID)
		})
	}
	
	sortFrm(content, val) {
		return _(content)
			.filter({attr: 'frm', comment: val})
			.sortBy(['fID'])
			.value()
	}
}

export default class Spt extends mix(Phaser.Group).with(commFunc, Frm) {
	constructor(game, data) {
		super(game)
		this.setData(data)
	}
	
	async construct() {
		var {filename} = this.data
		var {sptFilename, pakFilename, basename} = this.makeFilename(filename)
		var [sptContent, pakContent] = await this.loadMultiText(sptFilename, pakFilename)
		pakContent = parser.pak(pakContent, basename)
		sptContent = parser.spt(sptContent, {basename: basename, logWarning: false, pak: pakContent})
		var arr = ['STAND', 'WALK']
		var frmContent = await this.loadFrm(sptContent, arr)
		this.addFrm(frmContent, arr, sptFilename)
		this.setFlip()
		this.appendData({sptFilename, pakFilename, sptContent, pakContent, frmContent})
		this.addNickname('')
		this.triggerUpdate()
	}
	
	async loadMultiText(...arr) {
		arr = arr.length > 1 ? arr : arr[0]
		for (let filename of arr) {
			this.game.load.text(filename)
		}
		await this.loadComplete()
		return _.map(arr, filename => this.game.cache.getText(filename))
	}
	
	addNickname(val) {
		var text = val
		var style = { font: "16px Arial", fill: "#ffffff", align: "center" }
		this.nickname = this.game.add.text(0, 0, text, style, this)
	}
	
	makeFilename(filename) {
		var sptFilename = replaceExt(filename, '.spt')
		var pakFilename = replaceExt(filename, '.pak')
		var basename = path.basename(sptFilename, '.spt')
		return {sptFilename, pakFilename, basename}
	}
	
	setFlip() {
		this.multiplyAll('scale.x', -1)
	}
	
	turnLeft() {
		if (this.sprite.scale.x != -1) {
			this.sprite.x += this.turnAdjust()
			this.sprite.scale.x = -1
		}
	}
	
	turnRight() {
		if (this.sprite.scale.x != 1) {
			this.sprite.x -= this.turnAdjust()
			this.sprite.scale.x = 1
		}
	}
	
	turnAdjust() {
		var frame = this.sprite.frame
		var frmContent = this.data.frmContent
		var {drawX, drawY, _x, _y, _w, _h} = frmContent[frame]
		return (drawX + _w / 2) * 2
	}
	
	adjustBody() {
		var frame = this.sprite.frame
		var frmContent = this.data.frmContent
		var {drawX, drawY, _x, _y, _w, _h} = frmContent[frame]
		var bdy = frmContent[frame].sub[0]
		var {x, y, w, h} = bdy
		this.sprite.body.setSize(w, h, x, y)
		this.nickname.x = this.sprite.x
		if (this.sprite.scale.x != -1) {
			this.nickname.x += this.turnAdjust() / 2
		} else {
			this.nickname.x -= this.turnAdjust() / 2
		}
		this.nickname.y = this.sprite.y
	}
	
	_update() {
		this.adjustBody()
	}
}