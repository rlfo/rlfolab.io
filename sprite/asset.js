import {commFunc} from '../state/util.js'
import {mix, moment} from '../lib/index.js'

export default class Asset extends mix(Phaser.Group).with(commFunc) {
	constructor(game) {
		super(game)
	}
	
	async construct() {
		this.setData({})
		await this.loadBasedAsset()
		await this.loadLobbyContent()
	}
	
	async loadBasedAsset() {
		this.game.load.images(_.flatten(this.getButton(true)))
		await this.loadComplete()
	}
	
	async loadLobbyContent(lobbyName) {
		var lobbyColl = [{
			lobby: 'normal',
			bgm: ['bgm/lfo_lobby.ogg', 'bgm/lfo_lobby.mp3'],
			sty: 'text/story/lobby/lobby.sty',
			map: 'text/map/lobby/newlobby.map'
		},{
			lobby: 'halloween',
			bgm: ['bgm/lfo_devil5.ogg', 'bgm/lfo_devil5.mp3'],
			dur: ['10-22', '11-10'],
			sty: 'text/story/lobby/lobby_halloween.sty',
			map: 'text/map/lobby/newlobby_halloween.map'
		},{
			lobby: 'newyear',
			bgm: ['bgm/lfo_nyear.ogg', 'bgm/lfo_nyear.mp3'],
			dur: ['01-10', '02-15'],
			sty: 'text/story/lobby/lobby_newyear.sty',
			map: 'text/map/lobby/newlobby_newyear.map'
		},{
			lobby: 'xmas',
			bgm: ['bgm/lfo_xmas.ogg', 'bgm/lfo_xmas.mp3'],
			dur: ['12-01', '12-31'],
			sty: 'text/story/lobby/lobby_xmas.sty',
			map: 'text/map/lobby/newlobby_xmas.map'
		}]
		
		var lobbyContent = lobbyColl[0]
		
		if (lobbyName) {
			lobbyContent = _.find(lobbyColl, {lobby: lobbyName})
		} else {
			for (let obj of lobbyColl) {
				if (obj.dur && this.getIsDur(obj.dur)) {
					lobbyContent = obj
					break
				}
			}
		}
		
		var {bgm, map} = lobbyContent
		this.game.load.audio(bgm)
		this.game.load.text(map)
		await this.loadComplete()
		this.appendData({lobbyContent})
		return lobbyContent
	}
	
	getButton(arr) {
		var ARROW = ['asset/arrow.png', 'asset/arrowdown.png']
		var ATTACK = ['asset/attack.png', 'asset/attackdown.png']
		var JUMP = ['asset/jump.png', 'asset/jumpdown.png']
		var DEFEND = ['asset/defend.png', 'asset/defenddown.png']
		return arr ? [ARROW, ATTACK, JUMP, DEFEND] : {ARROW, ATTACK, JUMP, DEFEND}
	}
	
	getIsDur(dur) {
		var today = moment.utc()
		var startDate = moment.utc(dur[0], 'MM-DD')
		var endDate = moment.utc(`${dur[1]} 23:59:59`, 'MM-DD hh:mm:ss')
		var isDur = today.isBetween(startDate, endDate, 'second')
		return isDur
	}
	
	makeKeycap() {
		var {ARROW, ATTACK, JUMP, DEFEND} = this.getButton()
		var keycapColl = [{
		keycap: 'UP', x: 0, y: 0, angle: 0, key: ARROW
		}, {
		keycap: 'DOWN', x: 0, y: 150, angle: 180, key: ARROW
		}, {
		keycap: 'LEFT', x: -150, y: 150, angle: -90, key: ARROW
		}, {
		keycap: 'RIGHT', x: 150, y: 150, angle: 90, key: ARROW
		}, {
		keycap: 'ATTACK', x: 350, y: 150, angle: 0, key: ATTACK
		}, {
		keycap: 'JUMP', x: 500, y: 150, angle: 0, key: JUMP
		}, {
		keycap: 'DEFEND', x: 650, y: 150, angle: 0, key: DEFEND}]
		for (let obj of keycapColl) {
			var {angle, keycap, key, x, y} = obj
			let {spriteSheet} = Asset.genBitmap(this.game, keycap, key)
			let button = this.game.make.button(x, y, spriteSheet, null, this, 0, 0, 1, 0, this)
			button.angle = angle
			button.alpha = 0.5
			button.anchor.setTo(0.5, 0.5)
			obj.button = button
		}
		return keycapColl
	}
	
	makeKeycapToggle() {
		var icon = this.game.add.group() 
		var x = 0
		var y = 0
		var text = '\uf11c'
		var style = { fill : '#ffffff', font : '36px fontAwesome' }
		var child = this.game.make.text(x, y, text, style)
		child.anchor.setTo(0.5, 0.5)
		icon.addChild(child)
		text = '\uf0c8'
		style = { fill : '#3b5998', font : '72px fontAwesome' }
		child = this.game.make.text(x, y, text, style)
		child.anchor.setTo(0.5, 0.5)
		child.inputEnabled = true
		icon.addChild(child)
		icon.reverse()
		icon.x = 64
		icon.y = 150
		return icon
	}
	
	makeLoadingText() {
		var x = this.game.width - 40
		var y = this.game.height - 20
		var text = 
			`
			載入中，請稍候...
			
			Loading, please wait...
			`
		var style = { font: "16px Arial", fill: "#ffffff", align: "center" }
		var loadingText = this.game.make.text(x, y, text, style)
		loadingText.anchor.setTo(1, 1)
		return loadingText
	}
	
	makeLobbyContent(lobbyContent) {
		lobbyContent = lobbyContent ? lobbyContent : this.data.lobbyContent
		var {bgm, map} = lobbyContent
		return {bgm: this.game.make.audio(bgm.toString(), 1, true), map: this.game.cache.getText(map)}
	}
}