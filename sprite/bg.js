import * as parser from '../parser/index.js'
import {commFunc} from '../state/util.js'
import {mix} from '../lib/index.js'
import {Spt} from './index.js'

class Bgp extends mix(Phaser.Group).with(commFunc) {
	constructor(game, data, parent) {
		super(game, parent)
		var {filename, flip, x, y, z, Span, Xoffset, Xratio, Yoffset} = data
		this.addMultiImage(filename, x, y + z, Span, Xoffset, Yoffset)
		this.setFlip(flip)
		this.setXratio(Xratio)
		this.triggerUpdate()
	}
	
	addImage(filename, x, y) {
		this.game.add.image(x, y, filename, 0, this)
	}
	
	addMultiImage(filename, x, y, Span, Xoffset, Yoffset) {
		for (let i = 0; i < Span; i++) {
			this.addImage(filename, x + i * Xoffset, y + i * Yoffset)
		}
	}
	
	setFlip(val) {
		if (val) {
			this.multiplyAll('scale.x', -1)
		}
	}
	
	setXratio(val) {
		this.Xratio = val
	}
	
	_update() {
		this.x = this.game.camera.x * (1 - this.Xratio)
	}
}

export default class Bg extends mix(Phaser.Group).with(commFunc) {
	constructor(game, data) {
		super(game)
		this.setData(data)
	}
	
	async construct() {
		var {filename} = this.data
		var content = await this.loadText(filename)
		content = parser.bg(content, {filename})
		this.appendData({content})
		this.setBound(content)
		var arr = await this.loadBgp(content)
		this.addBgp(arr)
	}
	
	async loadBgp(content) {
		var arr = _.filter(content, {attr: 'bgp'})
		arr = _.sortBy(arr, ['z'])
		for (let {filename} of arr) {
			this.game.load.image(filename)
		}
		await this.loadComplete()
		return arr
	}
	
	async loadText(filename) {
		this.game.load.text(filename)
		await this.loadComplete()
		return this.game.cache.getText(filename)
	}
	
	addBgp(arr) {
		for (let data of arr) {
			new Bgp(this.game, data, this)
		}
	}
	
	setBound(content) {
		var {upper, lower, w} = _.find(content, {attr: 'bgd'})
		this.game.world.setBounds(0, 0, w, lower)
		this.game.physics.setBoundsToWorld()
	}
}