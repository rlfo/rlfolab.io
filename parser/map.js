import {stripComment, textpath, replaceExt} from './util.js'
import path from 'path'

export default function map(text) {
	var arr = stripComment(text)
	
	var current = 0
	var tokens = []
	
	while (current < arr.length) {
		
		var line = arr[current]
		
		if (!line.length) {
			current++
			continue
		}
		
		var attr = line[0]
		
		if (attr == 'stp') {
			var value = {
				_raw: line,
				_lineNum: current,
				attr: attr,
				bg: line[1],
				x: line[2],
				facing: line[3]
			}
			
			tokens.push(value)
			current++
			continue
		}
		
		if (attr == 'bgd') {
			var value = {
				_raw: line,
				_lineNum: current,
				attr: attr,
				id: line[1],
				filename: path.join(textpath, replaceExt(line[2], '.bg')).toLowerCase(),
				sub: []
			}
			
			line = arr[++current]
			
			while (line && line.length){
				attr = line[0]
				
				if (attr == 'grp') {
					value.sub.push({
						_raw: line,
						_lineNum: current,
						attr: attr,
						x: line[1],
						y: line[2],
						w: line[3],
						h: line[4]
					})
					
					line = arr[++current]
					continue
				}
				
				if (attr == 'lnk') {
					value.sub.push({
						_raw: line,
						_lineNum: current,
						attr: attr,
						id: line[1],
						toBg: line[2],
						toLink: line[3]
					})
					
					line = arr[++current]
					continue
				}
				
				warn('parser -> map -> bgd', '_raw: ' + js(line), '_lineNum: ' + current)
				current++
			}
			
			tokens.push(value)
			continue
		}
		
		warn('parser -> map', '_raw: ' + js(line), '_lineNum: ' + current)
		current++
	}
	
	return tokens
}