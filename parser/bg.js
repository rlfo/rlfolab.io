import {stripComment, imgpath, textpath} from './util.js'
import path from 'path'

export default function bg(text, {filename}) {
	var arr = stripComment(text)
	
	var current = 0
	var tokens = []
	
	while (current < arr.length) {
		
		var line = arr[current]
		
		if (!line.length) {
			current++
			continue
		}
		
		var attr = line[0]
		
		if (attr == 'bgd') {
			var value = {
				_raw: line,
				_lineNum: current,
				attr: attr,
				name: line[1],
				id: parseInt(line[2]),
				w: parseInt(line[3]),
				upper: parseInt(line[4]),
				lower: parseInt(line[5]),
				friction: parseFloat(line[6]),
				speedup: parseFloat(line[7]),
				gravity: parseFloat(line[8]),
				windspeed: parseFloat(line[9]),
				watertype: parseInt(line[10]),
				waterStartX: parseInt(line[11]),
				waterEndX: parseInt(line[12])
			}
			
			tokens.push(value)
			current++
			continue
		}
		
		if (attr == 'bgo') {
			var value = {
				_raw: line,
				_lineNum: current,
				attr: attr,
				filename: path.join(path.dirname(filename), line[1]).toLowerCase(),
				x: parseInt(line[2]),
				y: parseInt(line[3]),
				z: parseInt(line[4]),
				Xratio: parseFloat(line[5]),
				Span: parseInt(line[6]),
				Xoffset: parseInt(line[7]),
				Yoffset: parseInt(line[8]),
				behaviourFunction: parseInt(line[9]),
				portal: parseInt(line[10]),
				flip: parseInt(line[11])
			}
			
			tokens.push(value);
			current++;
			continue;
		}
		
		if (attr == 'bgp') {
			var value = {
				_raw: line,
				_lineNum: current,
				attr: attr,
				filename: path.join(imgpath, line[1]).toLowerCase(),
				x: parseInt(line[2]),
				y: parseInt(line[3]),
				z: parseInt(line[4]),
				Xratio: parseFloat(line[5]),
				Span: parseInt(line[6]),
				Xoffset: parseInt(line[7]),
				Yoffset: parseInt(line[8]),
				behaviourFunction: parseInt(line[9]),
				portal: parseInt(line[10]),
				flip: parseInt(line[11])
			}
			
			tokens.push(value)
			current++
			continue
		}
		
		warn('parser -> bg', '_raw: ' + js(line), '_lineNum: ' + current)
		current++
	}
	
	return tokens
}