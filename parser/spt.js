import {stripComment, imgpath} from './util.js'
import path from 'path'

export default function spt(text, {basename, logWarning = true, pak}) {
	var arr = stripComment(text)
	
	var current = 0
	var tokens = []
	
	while (current < arr.length) {
		
		var line = arr[current]
		
		if (!line.length) {
			current++
			continue
		}
		
		var attr = line[0]
		
		if (attr == 'bgd') {
			var value = {
				_raw: line,
				_lineNum: current,
				attr: attr,
				name: line[1],
				id: parseInt(line[2]),
				w: parseInt(line[3]),
				upper: parseInt(line[4]),
				lower: parseInt(line[5]),
				friction: parseFloat(line[6]),
				speedup: parseFloat(line[7]),
				gravity: parseFloat(line[8]),
				windspeed: parseFloat(line[9]),
				watertype: parseInt(line[10]),
				waterStartX: parseInt(line[11]),
				waterEndX: parseInt(line[12])
			}
			
			tokens.push(value)
			current++
			continue
		}
		
		if (attr == 'pic') {
			var value = {
				_raw: line,
				_lineNum: current,
				attr: attr,
				sub: []
			}
			
			line = arr[++current]
			
			while (line[0] != '!'){
				value.sub.push({
					_raw: line,
					_lineNum: current,
					pID: parseInt(line[0]),
					filename: line[1]
				})
					
				line = arr[++current]
			}
			
			tokens.push(value)
			current++
			continue
		}
		
		if (attr.toLowerCase() == 'frm') {
			var value = {
				_raw: line,
				_lineNum: current,
				attr: attr.toLowerCase(),
				comment: line[1],
				fID: parseInt(line[2]),
				fState: line[3],
				pID: basename ? path.join(imgpath, basename, line[4] + '.png') : line[4],
				cx: line[5],
				cy: line[6],
				duration: parseInt(line[7]),
				newAttack: line[8],
				attackMany: line[9],
				next: line[10],
				dvx: line[11],
				dvy: line[12],
				stepx: line[13],
				stepy: line[14],
				dvz: line[15],
				face: line[16],
				sub: []
			}
			
			if (pak) {
				var {drawX, drawY, x, y, w, h} = _.find(pak, {pID: value.pID})
				value.drawX = drawX
				value.drawY = drawY
				value._x = x
				value._y = y
				value._w = w
				value._h = h
			}
			
			tokens.push(value)
			current++
			continue
		}
		
		if (attr == 'bdy') {
			var value = {
				_raw: line,
				_lineNum: current,
				attr: attr,
				body: line[1],
				x: parseInt(line[2]),
				y: parseInt(line[3]),
				w: parseInt(line[4]),
				h: parseInt(line[5]),
				g0: line[6],
				g1: line[7],
				bstate: line[8],
				SumSprite: line[9]
			}
			
			tokens.push(value)
			current++
			continue
		}
		
		if (attr == 'wpn') {
			var value = {
				_raw: line,
				_lineNum: current,
				attr: attr,
				weapon: line[1],
				type: line[2],
				x: line[3],
				y: line[4],
				z: line[5],
				o: line[6],
				dx: line[7],
				dy: line[8],
				dz: line[9],
				aID: line[10],
				facing: line[11]
			}
			
			tokens.push(value)
			current++
			continue
		}
		
		if (attr == 'act') {
			var value = {
				_raw: line,
				_lineNum: current,
				attr: attr,
				comment: line[1],
				aID: line[2],
				aState: line[3],
				fID: line[4],
				nextAction: line[5],
				blast_attack_point: line[6],
				blast_rebound_ST: line[7],
				TotalTimeLive: line[8],
				AttackHPPercentage: line[9]
			}
			
			tokens.push(value)
			current++
			continue
		}
		
		if (attr == '!') break
		
		if (logWarning) warn('parser -> spt', '_raw: ' + js(line), '_lineNum: ' + current)
		current++
	}
	
	tokens = tokens.reverse()
	
	for (var i = 0; i < tokens.length; i++) {
		let obj = tokens[i]
		if (obj.attr == 'bdy') {
			let {sub} = _.find(tokens, {attr: 'frm'}, i)
			sub.unshift(obj)
		}
	}
	
	tokens = tokens.reverse()
	
	return tokens
}