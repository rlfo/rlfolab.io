import {stripComment, imgpath} from './util.js'
import path from 'path'

export default function pak(text, basename) {
	var arr = stripComment(text)
	
	var current = 0
	var tokens = []
	
	while (current < arr.length) {
		
		var line = arr[current]
		
		if (!line.length || line.length == 1) {
			current++
			continue
		}
		
		if (line.length == 10) {
			var value = {
				_raw: line,
				_lineNum: current,
				attr: null,
				pID: basename ? path.join(imgpath, basename, line[0] + '.png') : line[0],
				fileID: parseInt(line[1]),
				unknown1: parseInt(line[2]),
				unknown2: parseInt(line[3]),
				x: parseInt(line[4]),
				y: parseInt(line[5]),
				w: parseInt(line[6]) - parseInt(line[4]),
				h: parseInt(line[7]) - parseInt(line[5]),
				drawX: parseInt(line[8]),
				drawY: parseInt(line[9])
			}
			
			tokens.push(value)
			current++
			continue
		}
		
		warn('parser -> pak', '_raw: ' + js(line), '_lineNum: ' + current)
		current++
	}
	
	return tokens
}