import {stripComment, textpath, replaceExt} from './util.js'
import path from 'path'

export default function loadsprite(text) {
	var arr = stripComment(text)
	
	var tokens = []
	
	_.forEach(arr, line => {
		if (line[1]) {
			var filepath = path.join(textpath, line[1]).toLowerCase()
			tokens.push(replaceExt(filepath, '.spt'))
		}
	})
	
	return tokens
}