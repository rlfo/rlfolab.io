export {replaceExt} from '../lib/index.js'

var tab = /\r?\n/
var comment = function(line) {
	if (_.trimStart(line).startsWith('/')) return ''
	if (!_.includes(line, '//')) return line
	return line.slice(0, line.indexOf('//'))
}
var space = function(line) {
	return _(line)
	.split(/\s+/)
	.compact()
	.value()
}

export function stripComment(text) {
	return _(text)
	.split(tab)
	.map(comment)
	.map(space)
	.value()
}

export var textpath = 'text'
export var imgpath = 'img'