import {commFunc} from '../state/util.js'
import {mix} from '../lib/index.js'

export default class Control extends mix(Phaser.Group).with(commFunc) {
	constructor(game, asset, spt, bg) {
		super(game)
		this.setFixedToCamera()
		this.setAsset(asset)
		this.setSpt(spt)
		this.setKey()
		this.setKeycap()
		this.setKeycapToggle()
		this.triggerUpdate()
	}
	
	isKeycapDown(val) {
		var {button} = _.find(this.keycapColl, {keycap: val})
		return button.frame ? true : false
	}
	
	get noKeyDown() {
		if (this.leftIsDown || this.rightIsDown || this.upIsDown || this.downIsDown) {
			return false
		}
		return true
	}
	
	setAsset(obj) {
		this.asset = obj
	}
	
	setFixedToCamera() {
		this.fixedToCamera = true
	}
	
	setSpt(obj) {
		this.spt = obj
	}
	
	setKey() {
		this.key = {
			W: this.game.input.keyboard.addKey(Phaser.Keyboard.W),
			S: this.game.input.keyboard.addKey(Phaser.Keyboard.S),
			A: this.game.input.keyboard.addKey(Phaser.Keyboard.A),
			D: this.game.input.keyboard.addKey(Phaser.Keyboard.D),
			UP: this.game.input.keyboard.addKey(Phaser.Keyboard.UP),
			DOWN: this.game.input.keyboard.addKey(Phaser.Keyboard.DOWN),
			LEFT: this.game.input.keyboard.addKey(Phaser.Keyboard.LEFT),
			RIGHT: this.game.input.keyboard.addKey(Phaser.Keyboard.RIGHT)
		}
	}
	
	setKeycap() {
		var keycapColl = this.asset.makeKeycap()
		this.keycapColl = keycapColl
		var keycap = this.game.add.group()
		this.keycap = keycap
		this.addChild(keycap)
		for (let obj of keycapColl) {
			keycap.addChild(obj.button)
		}
		keycap.x = 250
		keycap.y = 350
		this.toggleKeycap()
	}
	
	setKeycapToggle() {
		var icon = this.asset.makeKeycapToggle()
		icon.onChildInputDown.add(function() {
			this.toggleKeycap()
		}, this)
		this.addChild(icon)
	}
	
	toggleKeycap() {
		this.keycap.visible = !this.keycap.visible
	}
	
	get leftIsDown() {
		if (this.isKeycapDown('LEFT') || this.key.LEFT.isDown || this.key.A.isDown) {
			return true
		}
		return false
	}
	
	get rightIsDown() {
		if (this.isKeycapDown('RIGHT') || this.key.RIGHT.isDown || this.key.D.isDown) {
			return true
		}
		return false
	}
	
	get upIsDown() {
		if (this.isKeycapDown('UP') || this.key.UP.isDown || this.key.W.isDown) {
			return true
		}
		return false
	}
	
	get downIsDown() {
		if (this.isKeycapDown('DOWN') || this.key.DOWN.isDown || this.key.S.isDown) {
			return true
		}
		return false
	}
	
	_update() {
		if (this.leftIsDown) {
			this.spt.x -= 5
			this.spt.turnLeft()
			this.spt.sprite.play('WALK')
		}
		
		if (this.rightIsDown) {
			this.spt.x += 5
			this.spt.turnRight()
			this.spt.sprite.play('WALK')
		}
		
		if (this.upIsDown) {
			this.spt.y -= 5
			this.spt.sprite.play('WALK')
		}
		
		if (this.downIsDown) {
			this.spt.y += 5
			this.spt.sprite.play('WALK')
		}
		
		if (this.noKeyDown) {
			this.spt.sprite.play('STAND')
		}
	}
}