System.config({
	map: {
		'plugin-babel': 'lib/plugin-babel-master/plugin-babel.js',
		'systemjs-babel-build': 'lib/plugin-babel-master/systemjs-babel-browser.js',
		'path': 'lib/path-browserify-master/index.js'
		},
	transpiler: 'plugin-babel'
});

System
.import('index.js')
.catch(function(e) {
	warn(e)
})