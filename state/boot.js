import path from 'path'

export default class Boot extends Phaser.State {
	init() {
		this.stage.disableVisibilityChange = true
		this.game.time.advancedTiming = true
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL
		this.load.image = this.patchLoad(this.load.image)
		this.load.audio = this.patchLoad(this.load.audio)
		this.load.text = this.patchLoad(this.load.text)
		this.load.images = this.patchMultiLoad(this.load.images)
		this.state.start('Lobby')
	}
	
	patchLoad(original) {
		return function (...args) {
			switch (args.length) {
				case 1:
				args = [args[0].toString(), _.isArray(args[0]) ? _.map(args[0], resolveImgpath) : resolveImgpath(args[0])]
				break
				case 2:
				if (_.isBoolean(args[1])) newArgs = [args[0].toString(), _.isArray(args[0]) ? _.map(args[0], resolveImgpath) : resolveImgpath(args[0]), args[1]]
				break
				case 3:
				args[1] = _.isArray(args[1]) ? _.map(args[1], resolveImgpath) : resolveImgpath(args[1])
				break
				default:
				return warn('boot -> this.game.load', 'args exceed')
			}
			var result = original.apply(this, args)
			return result
		}
	}
	
	patchMultiLoad(original) {
		return function (...args) {
			switch (args.length) {
				case 1:
				args = [args[0], _.map(args[0], resolveImgpath)]
				break
				case 2:
				args[1] = _.map(args[1], resolveImgpath)
				break
				default:
				return warn('boot -> this.game.load(s)', 'args exceed')
			}
			var result = original.apply(this, args)
			return result
		}
	}
}

function resolveImgpath(filepath) {
		if (!_.includes(location.hostname, 'rlfo')) {
			return filepath
		}
		var args = filepath.split('/')
		if (args[0] != 'img') {
			return filepath
		}
		if (args.length == 2) {
			return path.join('rlfo-img-00', filepath)
		}
		if (args[1][0].match(/[a-c]/)) {
			return path.join('rlfo-img-01', filepath)
		} else {
			return path.join('rlfo-img-02', filepath)
		}
}