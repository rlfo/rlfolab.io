import {GrowingPacker} from '../lib/index.js'

export var commFunc = superclass => class extends superclass {
	appendData(obj) {
		for (let key in obj) {
			this.data[key] = obj[key]
		}
	}
	
	setData(val = {}) {
		this.data = val
	}
	
	loadComplete() {
		return new Promise(res => {
			this.game.load.start()
			this.game.load.onLoadComplete.add(res, this)
		})
	}
	
	delay(ms) {
		return new Promise(res => {
			setTimeout(res, ms)
		})
	}
	
	triggerUpdate() {
		this.update = this._update
	}
	
	get originFps() {
		return 36
	}
	
	static genBitmap(game, name, arr) {
		arr = _.map(arr, obj => {
			if (_.isString(obj)) return {pID: obj}
			return obj
		})
		var packer = new GrowingPacker()
		var frame = 0
		var blocks = _.map(arr, obj => {
			var {pID, drawX = 0, drawY = 0, fID = 0} = obj
			var {width, height} = game.cache.getImage(pID)
			return {w: width + drawX, h: height + drawY, fID: frame++, pID: pID, drawX: drawX, drawY: drawY}
		})
		blocks.sort(function(a,b) { return (b.h < a.h) })
		packer.fit(blocks)
		var [width, height] = (() => {
			var maxW = _.map(blocks, obj => {
				var {fit: {x, y}, w, h} = obj
				return x + w
			})
			maxW = _.max(maxW)
			var maxH = _.map(blocks, obj => {
				var {fit: {x, y}, w, h} = obj
				return y + h
			})
			maxH = _.max(maxH)
			if (maxW % 2) {
				maxW++
			}
			if (maxH % 2) {
				maxH++
			}
			return [maxW, maxH]
		})()
		var spriteSheet = new Phaser.BitmapData(game, name, width, height)
		var frameData = new Phaser.FrameData()
		for(let block of blocks) {
			if (block.fit) {
				let {pID, fID, fit: {x, y}, w, h, drawX, drawY} = block
				let image = game.make.sprite(0, 0, pID)
				spriteSheet.draw(image, x + drawX, y + drawY)
				let frame = new Phaser.Frame(fID, x, y, w, h, pID)
				frameData.addFrame(frame)
			}
		}
		game.cache.addBitmapData(name, spriteSheet, frameData)
		return {spriteSheet, frameData}
	}
}