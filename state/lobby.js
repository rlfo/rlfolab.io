import * as parser from '../parser/index.js'
import {Bg, Spt, Asset} from '../sprite/index.js'
import {commFunc} from './util.js'
import {mix} from '../lib/index.js'
import {Control} from '../touch/index.js'

export default class Lobby extends mix(Phaser.State).with(commFunc) {
	async create() {
		try{
		this.game.physics.startSystem(Phaser.Physics.ARCADE)
		this.setData()
		this.setGroup()
		var asset = new Asset(this.game)
		var loadingText = asset.makeLoadingText()
		this.addChild(loadingText)
		await asset.construct()
		var {bgm, map} = asset.makeLobbyContent()
		var text = asset.makeLoadingText()
		bgm.loopFull()
		map = parser.map(map)
		var bgd = _.find(map, {attr: 'bgd'})
		var bg = new Bg(this.game, {filename: bgd.filename})
		await bg.construct()
		var dat = 'text/loadsprite/loadsprite.dat'
		this.game.load.text(dat)
		await this.loadComplete()
		dat = parser.loadsprite(this.game.cache.getText(dat))
		var spt = await this.loadChar(dat)
		var control = new Control(this.game, asset, spt, bg)
		this.game.camera.follow(spt.nickname, Phaser.Camera.FOLLOW_LOCKON, 0.1, 0.1)
		this.removeChild(loadingText)
		}catch (e) {
			warn(e)
		}
	}
	
	async loadChar(dat) {
		try{
		var random = this.game.rnd.between(-1, dat.length)
		var spt = new Spt(this.game, {filename: dat[random], x: 500, y: 400})
		await spt.construct()
		return spt
		}catch (e) {
			return await this.loadChar(dat)
		}
	}
	
	render() {
		this.game.debug.cameraInfo(this.game.camera, 32, 32)
	}
	
	addChild(obj) {
		this.group.addChild(obj)
	}
	
	removeChild(obj) {
		this.group.removeChild(obj)
	}
	
	setGroup() {
		this.group = this.game.add.group()
	}
}