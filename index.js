import Boot from './state/boot.js'
import Lobby from './state/lobby.js'

class Game extends Phaser.Game {
	constructor() {
		super('100%', 600, Phaser.AUTO, 'GAME', null)
		this.state.add('Boot', Boot, false)
		this.state.add('Lobby', Lobby, false)
		this.state.start('Boot')
	}
}

new Game()