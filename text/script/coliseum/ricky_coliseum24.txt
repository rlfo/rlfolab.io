; this file must be in UNICODE format
; Ricky defeated Man Doi Chuen.
; Then he go see Milo's match.

begin
background 23

playmusic "bgm/lfo_courage.ogg"

; background ppl
; girl students A
add 9531 5003 1600 0 260
add 9532 5003 950 0 300
add 9533 5003 600 0 330

; girl students with glasses
add 9541 5004 1700 0 500
add 9542 5004 420 0 370
faceleft 9542

; PE girl students
add 9551 5005 1720 0 280
faceleft 9551
doaction 9551 1
add 9552 5005 1550 0 280
add 9553 5005 690 0 315
faceleft 9553
doaction 9553 2
add 9554 5005 1385 0 470
faceleft 9554

; bold PE students
add 9521 5002 1680 0 260
add 9522 5002 600 0 450
faceleft 9522
faceleft 9521

; normal students
add 9511 5001 1030 0 300
faceleft 9511
add 9512 5001 300 0 480


; judo a's
add 9600 60 1800 0 500
faceleft 9600
add 9601 60 1300 0 470
add 9602 60 400 0 480
faceleft 9602

; judo b's 
add 9611 61 635 0 295

;man doi chuen
add 9621 62 1050 0 390

;ricky
add 9055 55 1120 0 390
faceleft 9055

camera_pos 630

doaction 9055 510
wait_complete

doaction 9055 520
wait_complete

doaction 9621 730
talkEx 2001 0 1
wait_complete

;man gets up, escape
doaction 9621 740
talkEx 2001 2 2
wait_complete
run 9621 550 390

;ricky talks
talkEx 2001 3 4
wait_complete

end