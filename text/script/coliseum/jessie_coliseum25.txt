; this file must be in UNICODE format
begin
background 25
playmusic "bgm/lfo_stadium3.ogg"
; davis = 9000
add 9000 71  3350 0 600
; enemy
add 9002 60 5030 0 430
faceleft 9002

;============== Part 3 ==================
camera_pos 2900
wait_complete
move 9000 3800 550
wait_complete
fadein 0 3
remove 9000
camera_pos 4450
add 9100 3333 4250 0  430
wait_complete
move 9100 4650 430
wait_complete
talk 3003
wait_complete
end



