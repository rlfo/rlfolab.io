; this file must be in UNICODE format
begin
background 12
playmusic "bgm/lfo_normal.ogg"

add 9061 61 2300 0 260 
add 9060 60 2000 0 300

add 9162 60 1950 0 310
add 9262 62 1900 0 300
add 9362 62 1850 0 310
add 9062 52 1800 0 300

add 9260 52 1720 0 300
add 9360 52 1760 0 310


add 9103 71 1600 0 400 
faceleft 9003

;============== Ying Pang at the doorway ==================
camera_pos 1700

move 9103 2000 400
wait_complete

talkEX 3001 0 0
wait_complete

move 9103 2100 360
faceleft 9061
talkEX 3001 1 6
wait_complete

end