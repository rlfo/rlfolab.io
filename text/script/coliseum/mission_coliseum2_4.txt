; this file must be in UNICODE format
begin
background 10

camera_pos 500

add 9065   65 1100 0 420
faceleft 9065
add 9070   70 1000 0 440
faceleft 9070
add 9021 3021 1050 0 450
faceleft 9021
add 9405  405 1050 0 450
changeholder 9405 9021

add 9002    2  800 0 430 
add 9001    1  750 0 440 
wait_complete

playmusic "bgm/lfo_humor.ogg"
talkEX 1006 0 2 
wait_complete

add 9319 3019 1540 0 430
add 9077   77 1500 0 440
add 9329 3019 1570 0 450

move 9319 1240 430
move 9077 1200 440
move 9329 1270 450
wait_complete

playmusic "bgm/lfo_rage.ogg"
faceright 9065
talkEX 1006 3 3
wait_complete

talkEX 1006 4 5
wait_complete

faceleft 9065
talkEX 1006 6 10
wait_complete

end

