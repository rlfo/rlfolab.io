; this file must be in UNICODE format
begin
background 1

playmusic "bgm/lfo_stadium3.ogg"
; victor = 9051  yourself = 9000
camera_pos 4430

add 9001 5011 3720 0 500
add 9002 5007 3800 0 500
add 9003 5008 3680 0 540

add 9010 2 4950 0 430

faceleft 9010

add 9000 3333 4700 0 430

talkEX 1000 43 43
wait_complete

move 9010 4850 430
talkEX 1000 44 44
;add 9000 3333 3750 0 530
wait_complete

Fadeout 0 1
end