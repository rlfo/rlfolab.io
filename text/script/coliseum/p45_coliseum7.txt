begin

background 38

playmusic "bgm/lfo_chi3.ogg"

background_offsetx 3000

camera_pos 0

addex 9012 13 300 0 330 1
addex 9013 5013 450 0 280 1
addex 9014 59 250 0 381 1

addex 9005 17 200 0 320 0

addex 9002 3019 220 0 280 0
addex 9003 3019 170 0 270 0
addex 9004 3019 130 0 320 0

faceleft 9012
faceleft 9013
faceleft 9014

talkex 1000 54 56
wait_complete

Doaction 9005 8000
wait_complete

runex 9005 180 380 0
wait_complete

Doaction 9005 8100
wait_complete

faceright 9013

runex 9005 550 300 1
wait_complete

talkex 1000 57 59
wait_complete

Doaction 9012 720
wait_complete

runex 9012 670 312 1
wait_complete

faceright 9005

talkex 1000 60 63
wait_complete

runex 9005 580 312 0
wait_complete

Doaction 9012 8100
wait_complete
Doaction 9005 200
wait_complete

talkex 1000 64 64
wait_complete

Doaction 9005 720
wait_complete

talkex 1000 65 65
wait_complete

Doaction 9012 8100
wait_complete
Doaction 9005 200
wait_complete

talkex 1000 66 66
wait_complete

runex 9014 460 290 1
wait_complete
runex 9013 -100 480 1
runex 9014 -100 480 1
runex 9012 -100 480 1
wait_complete

Doaction 9005 720
wait_complete

end