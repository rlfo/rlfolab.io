begin

background 3

playmusic "bgm/lfo_chi3.ogg"

background_offsetx 100

camera_pos 100

addex 9012 5013 630 0 400 1
addex 9013 13 750 0 390 1
addex 9014 59 550 0 381 1

runex 9012 330 400 0
runex 9013 450 390 1
runex 9014 250 381 0
wait_complete

talkex 1000 67 72
wait_complete

runex 9013 1000 250 1
runex 9012 0 450 1
runex 9014 0 450 1
wait_complete


end