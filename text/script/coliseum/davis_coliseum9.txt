; this file must be in UNICODE format
begin
background 10
playmusic "bgm/lfo_rage.ogg"
; stinky = 9000
add 9000  2 3200 0 410
add 9001  1 2500 0 400
add 9005  5 2500 0 400
add 9004  4 2500 0 400
add 9003  3 3050 0 380
DoAction 9003 833

add 9017 17 3550 0 410
faceleft 9017
wait_complete

;============== Part 5 ==================
camera_pos 2900
talk 95
wait_complete
run 9017 3350 410
wait_complete
DoAction 9017 8000
DoAction 9000 210
wait_complete
talk 96
wait_complete
DoAction 9000 700
run 9005 3175 405
run 9001 3150 410
run 9004 3110 415
talk 125
wait_complete
talk 97
wait_complete
talk 434
wait_complete
end


