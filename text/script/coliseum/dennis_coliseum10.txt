; this file must be in UNICODE format
begin
background 10
playmusic "bgm/lfo_rage.ogg"
; stinky = 9000
add 9004  4 3250 0 500
add 9000  2 3600 0 410
add 9003  3 3500 0 450

add 9017 17 2550 0 450
add 9021 3019 3600 0 380
add 9022 3019 3500 0 410
add 9023 3019 3700 0 550
add 9024 3019 3350 0 500
faceleft 9021
faceleft 9023
faceleft 9024
faceleft 9000
faceleft 9003
wait_complete

;============== Part 5 ==================
camera_pos 3000
DoAction 9000 8100
DoAction 9004 8100
wait_complete
camera_follow 9017
talk 90
wait_complete


; bat attacks cutie
DoAction 9017 8160
wait_complete
add 9027 17 3600 0 450
faceleft 9027
camera_pan 3000
remove 9017
wait_complete
DoAction 9027 8000
wait_complete
talk 91
faceleft 9004
wait_complete

; stinky and dennis run to cutie then bat
talk 120
run 9000 3250 445
run 9004 3290 455
wait_complete
talk 122
faceright 9000
faceright 9004
wait_complete
run 9000 3500 445
run 9004 3480 455
wait_complete
talk 92
DoAction 9027 8180
wait_complete
talk 140
DoAction 9000 733
DoAction 9004 733
wait_complete
talk 93
DoAction 9000 0
DoAction 9004 0
faceright 9000
faceright 9004
wait_complete
run 9004 3050 350
wait_complete
faceright 9004
talk 123
wait_complete
run 9004 2800 350
wait_complete
end


