; this file must be in UNICODE format
begin
background 34
playmusic "bgm/lfo_stadium3.ogg"
; davis = 9000
add 9000 72 720 0 330
faceleft 9000
add 9004 71 570 0 340 
add 9003 61 530 0 350 
add 9010 61 480 0 370
add 9011 61 440 0 350
add 9012 61 400 0 370


;============== Part 1 ==================
camera_pos 100   
talkEX 3002 0 6
wait_complete

add 9061 61 50 0 350
run 9061 250 350
faceleft 9000
faceleft 9003
faceleft 9004
faceleft 9010
faceleft 9011
faceleft 9012
wait_complete

talkEX 3002 7 8
wait_complete

faceright 9003
faceright 9004
faceright 9010
faceright 9011
faceright 9012
talkEX 3002 9 10
wait_complete
end

