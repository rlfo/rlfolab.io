; this file must be in UNICODE format
begin
background 25
playmusic "bgm/lfo_bliss.ogg"
; stinky = 9000
add 9000 3333 4650 0 430
; enemy 9001
add 9001 62 5030 0 430
faceleft 9001
DoAction 9001 733

; cutie, dennis, team
add 9003 5005 3760 0 440
add 9004 5002 3800 0 420
add 9010 5006 3865 0 430
add 9011 5005 3830 0 440
add 9012 5002 3905 0 440
DoAction 9011 1
DoAction 9012 1



;============== Meet Milo ==================
camera_pos 4450
talkEX 505 0 2
wait_complete

fadein 0 5
stopMusic

;move down to the stage
camera_follow 9000
character_pos 9000 3690 430
faceleft 9003
faceleft 9004
faceleft 9010
faceleft 9011
faceleft 9012
wait_complete

playmusic "bgm/lfo_stadium3.ogg"
talkEX 505 3 7
wait_complete

;walk back to the stage
move 9000 3695 550
wait_complete
faceright 9000
faceright 9003
faceright 9004
faceright 9010
faceright 9011
faceright 9012
move 9000 3850 550
wait_complete
move 9000 4040 480
wait_complete
move 9001 5400 430
move 9000 4650 430
wait_complete
camera_pan 4450
wait_complete
add 9002 54 5900 0 430
wait_complete
move 9002 5030 430
wait_complete

talkEX 505 8 9
wait_complete
end


