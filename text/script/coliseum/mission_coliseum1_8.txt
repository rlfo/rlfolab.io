; this file must be in UNICODE format
begin
background 1

playmusic "bgm/lfo_stadium3.ogg"
; victor = 9051  yourself = 9000
camera_pos 4430

add 9001 5011 3720 0 500
add 9002 5007 3800 0 500
add 9003 5008 3680 0 540

add 9010 2 4950 0 430

DoAction 9010 910
faceleft 9010

add 9000 3333 4700 0 430

talkEX 1000 45 45
;add 9000 3333 3750 0 530
wait_complete

camera_follow 9000
move 9000 4100 430
wait_complete

move 9000 3750 530
wait_complete

faceleft 9002
talkEX 1000 46 51
wait_complete

faceright 9002
add 9010 5 4950 0 430
faceleft 9010

move 9000 3800 550
wait_complete

move 9000 4700 430
wait_complete

camera_pan 4430
talkEX 1000 52 52
wait_complete

Fadeout 0 1
end