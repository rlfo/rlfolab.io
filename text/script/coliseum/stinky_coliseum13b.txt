; this file must be in UNICODE format
begin
background 37
playmusic "bgm/lfo_humor.ogg"
; stinky = 9000
add 9000 2 550 0 400
add 9010 62 560 0 320
add 9011 52 845 0 300
add 9012 52 490 0 270
add 9013 52 770 0 390
add 9014 52 900 0 280
add 9015 52 380 0 340

faceleft 9010
faceleft 9011
faceleft 9012
;faceleft 9013
faceleft 9014
faceleft 9015
DoAction 9010 733
DoAction 9011 733
DoAction 9012 833
DoAction 9013 833
DoAction 9014 833
DoAction 9015 733
;faceleft 9000

;============== Part 1 ==================
camera_pos 100                                                            
camera_follow 9000
wait_complete
talk 153
;run 9000 50 350
wait_complete

end

