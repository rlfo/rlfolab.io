; this file must be in UNICODE format
begin
background 6 
playmusic "bgm/lfo_humor.ogg"
; stinky = 9000
add 9000 2 2100 0 450
add 9002 51 2300 0 480
add 9010 55 1900 0 430
add 9011 55 2000 0 390
add 9012 63 2030 0 490
add 9013 63 2150 0 430
add 9014 63 2200 0 510
add 9015 63 2200 0 340
;add 9016 63 2340 0 380
add 9017 63 2450 0 505
add 9018 63 2480 0 430
;add 9019 63 2060 0 300
add 9020 63 2400 0 320
;wait_complete
DoAction 9002 733 
DoAction 9010 733
DoAction 9011 833
DoAction 9012 833
DoAction 9013 733
DoAction 9014 833
DoAction 9015 733
DoAction 9016 733
DoAction 9017 733
DoAction 9018 833
DoAction 9019 733
DoAction 9020 833
faceright 9000
faceleft 9002
faceleft 9013
faceleft 9014
faceleft 9015
faceleft 9016
faceleft 9019
faceleft 9017
faceleft 9018
faceleft 9019
faceleft 9020
wait_complete

;============== Part 2 ==================
camera_pos 1800
wait_complete
talk 72
wait_complete
end