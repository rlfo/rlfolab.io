; this file must be in UNICODE format
begin
background 34
playmusic "bgm/lfo_stadium3.ogg"
; davis = 9000
add 9164 72 790 0 330
faceleft 9164
add 9005 5  70 0 360 
add 9061 61 530 0 350 
add 9161 61 480 0 370
add 9162 61 440 0 350
add 9163 61 400 0 370


;============== Part 1 ==================
camera_pos 100   
wait_complete                                                           
talkEX 2003 0 2 
run 9005 600 340
wait_complete

faceleft 9005
talkEX 2003 3 4
wait_complete

faceright 9005
talkEX 2003 5 10
wait_complete
end

