; this file must be in UNICODE format
begin
background 10

camera_pos 500

addEX 9070   70 620 0 410 1
addEX 9065   65 580 0 440 1
addEX 9021 3021 650 0 450 1

addEX 9002    2  800 0 440 2
addEX 9001    1  700 0 440 2
faceleft 9001

addEX 9319 3019 900 0 430 1
addEX 9329 3019 950 0 450 1
faceleft 9319
faceleft 9329

addEX 9077   77 1040 0 400 1
faceleft 9077


playmusic "bgm/lfo_rage.ogg"
talkEX 1007 0 0 
wait_complete

DoAction 9001 210  
DoAction 9021 8000
DoAction 9065 8000
DoAction 9002 8100
wait_complete

run 9077 1100 430
run 9329 1070 450
faceleft 9002
talkEX 1007 3 3
wait_complete
talkEX 1007 1 1
wait_complete

faceright 9001 
DoAction 9001 100
wait_complete

camera_pan 600
DoAction 9077 8000
DOAction 9001 8100
faceright 9002
wait_complete
talkEX 1007 2 2
wait_complete

playmusic "bgm/lfo_bliss.ogg"
run 9077 1500 430 
run 9319 1500 430
run 9329 1500 450
run 9065 400 440
talkEX 1007 4 4
wait_complete


; ============ gangs give back out the phone ===========
add 9405 405 9021 1000 440
changeholder 9405 9021
move 9001 1150 420
move 9002 1100 440
run 9070 1050 430
run 9021 1000 440
wait_complete

changeholder 9405 9002
talkEX 1007 6 7
wait_complete



run 9070 500 430
run 9021 500 450
add 9502 5002 400 0 430 
add 9512 5012 500 0 440 
add 9511 5011 450 0 450 

move 9001 1050 420
move 9502 850 430 
move 9512 950 440 
move 9511 900 450 
talkEX 1007 8 9
wait_complete

move 9002 1000 440
wait_complete


changeholder 9405 9021
moveEX 9002 1100 440 1
wait_complete
talkEX 1007 10 13
wait_complete

move 9502 500 430 
move 9512 500 440 
move 9511 500 450 
moveEX 9001 900 420 0 
talkEX 1007 14 18
wait_complete


move 9001 500 420
talkEX 1007 19 19
wait_complete



end

