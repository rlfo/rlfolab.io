; this file must be in UNICODE format
begin
background 25
playmusic "bgm/lfo_stadium3.ogg"

; stinky = 9000
add 9000 2 3350 0 600
; enemy
add 9002 60 5030 0 430
; cutie, dennis, team

add 9003 3 3750 0 440
add 9004 4 3790 0 420
add 9010 5006 3865 0 430
add 9011 5005 3830 0 440
add 9012 5002 3905 0 440
faceleft 9003
faceleft 9004
faceleft 9010
faceleft 9011
faceleft 9012
faceleft 9002


;============== stinky arrives ==================

camera_pos 3400
run 9000 3670 450 
wait_complete
talk 112
wait_complete
camera_follow 9000
move 9000 3695 550
wait_complete
move 9000 3850 550
wait_complete
faceright 9000
faceright 9003
faceright 9004
faceright 9010
faceright 9011
faceright 9012
move 9000 4040 480
wait_complete
move 9001 5400 430
move 9000 4650 430
wait_complete

camera_pan 4450
wait_complete
talk 74
wait_complete

run 9000 4990 430
wait_complete
DoAction 9000 520
wait_complete

;============== stinky comes back ==================
camera_follow 9000
move 9000 4200 464
wait_complete
move 9000 4040 464
wait_complete
move 9000 3680 535
wait_complete
faceleft 9003
faceleft 9004
faceleft 9010
faceleft 9011
faceleft 9012
move 9000 3690 430
wait_complete
talk 75
wait_complete

;walk back to the stage
move 9000 3695 550
wait_complete
faceright 9000
faceright 9003
faceright 9004
faceright 9010
faceright 9011
faceright 9012
move 9000 3850 550
wait_complete
move 9000 4040 480
wait_complete
move 9001 5400 430
move 9000 4650 430
wait_complete
camera_pan 4450
wait_complete
add 9002 61 5900 0 430
wait_complete
move 9002 5030 430
wait_complete
talk 76
wait_complete


; woody 
add 9005 5 1448 0 430

;;;;;;;;;;;walk to other stage
;camera_pos 2200
camera_follow 9004

faceleft 9010
talk 506
wait_complete

move 9004 2550 430
move 9003 2630 430
move 9010 2577 410
move 9011 2840 440
move 9012 2780 440
wait_complete

add 9100 1 3100 0 500
move 9100 2480 410
wait_complete
faceright 9100
talk 415
wait_complete
;;;;;;;;;dennis goes to the stage
camera_follow 9004
move 9004 2700 550
wait_complete
move 9004 2592 525
wait_complete
move 9004 2360 455
wait_complete
faceleft 9100
move 9004 1748 430
wait_complete
camera_pan 1200
talk 416
wait_complete

;;;;;;;;woody fight dennis
run 9004 1600 430
wait_complete
fadein 0 3
character_pos 9004 1650 430
wait_complete
DoAction 9004 8100
DoAction 9005 210
wait_complete
camera_pan 2200
talk 417
wait_complete

;;;;;;;;;;stinky run to cutie
add 9002 2 3200 0 500
remove 9000
wait_complete
run 9002 2700 430
remove 9004
remove 9005
wait_complete
add 9004 4 1817 0 430
add 9005 5 1448 0 430
faceleft 9004
wait_complete
talk 77
wait_complete

;;;;;;;;;dennis fights woody (dennis lost)
camera_follow 9004
wait_complete
DoAction 9004 8200
DoAction 9005 8200
wait_complete
talk 113
wait_complete

;;;;;;;;;dennis comes
move 9004 2690 500
wait_complete
move 9004 2690 435
wait_complete
move 9004 2520 430
faceright 9100
wait_complete
faceright 9004
talk 100
wait_complete

;;;;;;;;woody come
playmusic "bgm/lfo_devil1.ogg"
camera_follow 9005
move 9005 2400 450
wait_complete
faceleft 9004
faceleft 9100
camera_pan 2200
talk 78
wait_complete 
talk 126
move 9002 2560 450
wait_complete

;;;;;;;;woody leaves
move 9005 2700 525
wait_complete
faceright 9002
faceright 9003
faceright 9004
faceright 9100
wait_complete
move 9005 3200 525
wait_complete
playmusic "bgm/lfo_courage.ogg"
remove 9005
faceright 9003
talk 79
wait_complete

;;;;;;;;;milo appears
talk 111
faceright 9002
faceright 9004
faceright 9100
faceright 9010
faceright 9011
faceright 9012
wait_complete

;stinky run back up to the stage
camera_follow 9002
run 9002 3850 550
move 9100 3180 510
move 9003 3280 510
move 9004 3220 510
move 9010 3285 510
move 9011 3200 510
move 9012 3325 510
wait_complete
run 9002 4040 480
add 9054 54 5030 0 430
wait_complete
remove 9001
;move 9001 5400 430
move 9002 4650 430
faceleft 9054
wait_complete
camera_pan 4450
wait_complete
talk 80
wait_complete


fadein 0 5

;;;;;;;;;;walk to other stage

camera_pos 3500
; cutie, dennis, team
add 9003 3 3910 0 440
add 9004 4 3950 0 420
add 9010 5006 3865 0 420
;add 9011 5002 3750 0 440
add 9011 5002 3400 0 500
add 9012 5005 3790 0 420
wait_complete
move 9100 3830 440
add 9002 2 4650 0 430
add 9054 54 4830 0 430
wait_complete
faceright 9100
faceleft 9054
talk 420
wait_complete

;;;;;;;;;stinky fights milo
camera_pan 4450
talk 421
wait_complete
DoAction 9002 8100
DoAction 9054 470
talk 449
wait_complete
camera_pos 3500
talk 451
wait_complete
run 9011 3700 460
faceleft 9100
faceleft 9003
faceleft 9004
faceleft 9010
faceleft 9012
talk 422
wait_complete
move 9100 3000 500
faceright 9003
faceright 9004
faceright 9010
faceright 9012
wait_complete




end





