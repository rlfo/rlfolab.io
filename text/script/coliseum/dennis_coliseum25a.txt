; this file must be in UNICODE format
begin
background 25
playmusic "bgm/lfo_stadium3.ogg"

; stinky = 9000
add 9000 2 3350 0 600
; enemy
add 9002 60 5030 0 430
; cutie, dennis, team

add 9003 3 3750 0 440
add 9004 4 3790 0 420
add 9010 5006 3865 0 430
add 9011 5005 3830 0 440
add 9012 5002 3905 0 440
faceleft 9003
faceleft 9004
faceleft 9010
faceleft 9011
faceleft 9012
faceleft 9002


;============== stinky arrives ==================

camera_pos 3400
run 9000 3670 450 
wait_complete
talk 112
wait_complete
camera_follow 9000
move 9000 3695 550
wait_complete
move 9000 3850 550
wait_complete
faceright 9000
faceright 9003
faceright 9004
faceright 9010
faceright 9011
faceright 9012
move 9000 4040 480
wait_complete
move 9001 5400 430
move 9000 4650 430
wait_complete

camera_pan 4450
wait_complete
talk 74
wait_complete

run 9000 4990 430
wait_complete
DoAction 9000 520
wait_complete

;============== stinky comes back ==================
camera_follow 9000
move 9000 4200 464
wait_complete
move 9000 4040 464
wait_complete
move 9000 3680 535
wait_complete
faceleft 9003
faceleft 9004
faceleft 9010
faceleft 9011
faceleft 9012
move 9000 3690 430
wait_complete
talk 75
wait_complete

;walk back to the stage
move 9000 3695 550
wait_complete
faceright 9000
faceright 9003
faceright 9004
faceright 9010
faceright 9011
faceright 9012
move 9000 3850 550
wait_complete
move 9000 4040 480
wait_complete
move 9001 5400 430
move 9000 4650 430
wait_complete
camera_pan 4450
wait_complete
add 9002 61 5900 0 430
wait_complete
move 9002 5030 430
wait_complete
talk 76
wait_complete


; woody 
add 9005 5 1448 0 430

;;;;;;;;;;;walk to other stage
;camera_pos 2200
camera_follow 9004

faceleft 9010
talk 506
wait_complete

move 9004 2550 430
move 9003 2630 430
move 9010 2577 410
move 9011 2840 440
move 9012 2780 440
wait_complete

add 9100 1 3100 0 500
move 9100 2480 410
wait_complete
faceright 9100
talk 415
wait_complete

;;;;;;;;;dennis goes to the stage
camera_follow 9004
move 9004 2700 550
wait_complete
move 9004 2592 525
wait_complete
move 9004 2360 455
wait_complete
faceleft 9100
move 9004 1748 430
wait_complete
camera_pan 1200

talk 702
wait_complete

talk 416
wait_complete

;;;;;;;;woody fight dennis
run 9004 1600 430
wait_complete
end






