; this file must be in UNICODE format
begin
background 12
playmusic "bgm/lfo_courage.ogg"

; cutie 
add 9003 3 2490 0 380
faceright 9003
add 9020 70 2600 0 370
add 9021 3021 2400 0 390
faceleft 9020
;faceleft 9021

;============== Cutie at the doorway ==================
camera_pos 2100

add 9004 4 2200 0 200
moveEX 9004 2300 250 0
wait_complete

faceleft 9003
faceleft 9020
faceleft 9021
talkEX 504 5 5
wait_complete

runEX 9003 2250 250 0
move 9020 2550 370 
moveEX 9021 2500 390 1
talkEX 504 6 8
wait_complete

move 9004 2330 260
talkEX 504 9 9
wait_complete

playmusic "bgm/lfo_humor.ogg"
talkEX 504 10 13
wait_complete

run 9020 2900 370
run 9021 2850 390
wait_complete

camera_pan 1800
faceleft 9004
;move 9003 2270 250 
talkEX 504 14 15
wait_complete

add 9000 2 1600 0 440
faceleft 9003

;~~~~~~~~~ stinky arrives
;camera_follow 9000
wait_complete
run 9000 1950 380 
wait_complete
run 9000 2150 295 
wait_complete
talk 73
wait_complete
run 9000 2209 200 
wait_complete
remove 9000
move 9003 2200 200 
move 9004 2250 210 
wait_complete
remove 9003
move 9004 2200 200 
wait_complete
end