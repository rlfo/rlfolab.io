; this file must be in UNICODE format
begin
background 25
playmusic "bgm/lfo_bliss.ogg"
; stinky = 9000
add 9000 3333 4650 0 430
; enemy 9001
add 9001 5 5030 0 430
faceleft 9001
DoAction 9001 733

; cutie, dennis, team
add 9003 5006 3730 0 450
add 9004 5002 3790 0 420
add 9010 5005 3875 0 420
add 9011 5005 3830 0 440
add 9012 5002 3920 0 440
DoAction 9010 1
DoAction 9012 1

;============== Beaten Woody==================
camera_pos 4450

;walk down the stage
camera_follow 9000
wait_complete
move 9000 4200 464
wait_complete
move 9000 4040 464
wait_complete
move 9000 3680 535
wait_complete
faceleft 9003
faceleft 9004
faceleft 9010
faceleft 9011
faceleft 9012
moveEX 9000 3600 460 0
wait_complete

talk 508
wait_complete
end


