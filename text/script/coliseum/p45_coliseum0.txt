begin

background 15

playmusic "bgm/lfo_chi1.ogg"

background_offsetx 200

camera_pos 200

talkex 1000 123 123
wait_complete

talkex 1000 122 122
wait_complete

addex 9002 59 530 0 300 1
addex 9003 13 620 0 310 1
addex 9004 5013 700 0 330 0

faceleft 9003
faceleft 9004

talkex 1000 0 5
wait_complete

addex 9011 98 200 0 400 1
addex 9012 156 100 0 410 1
addex 9013 156 170 0 430 1
addex 9014 156 240 0 440 1
addex 9015 156 30 0 440 1

faceleft 9002

camera_pan 50

moveex 9011 400 410 0
moveex 9012 200 420 0
moveex 9013 270 430 0
moveex 9014 340 440 0
moveex 9015 130 440 0
wait_complete

talkex 1000 6 7
wait_complete

end
