begin
background 49

;beginskiptalk

; =========================== by the river ========================
background_offsetx 900

camera_pos 500
playmusic "bgm/lfo_china2.ogg"

add 9011  11 950 0 360
add 9002  2  900 0 310
add 9004  4  820 0 330

add 9001  1  820 0 390
add 9012 12  900 0 410
add 9005  5  780 0 430

add 9506 13  710 0 340
add 9014 14  770 0 360
add 9051 51  730 0 380

add 9007  7  670 0 370
add 9006  6  610 0 350
add 9008  8  580 0 400
add 9009  9  540 0 370

add 9064 84 1200 0 350
faceleft 9064

talkEX 1000 118 119
wait_complete


camera_pan 1000
run 9064 1500 350 
character_pos 9009 800 350
run 9009 1400 350
wait_complete
                 
                 
background_offsetx 1100                 
DoaCtion 9009 8000
wait_complete
add 9715  715 1550 0 350
add 9337 3037 1550 0 350
Doaction 9337 30

remove 9064
character_pos 9011 950 320
character_pos 9002 900 270
character_pos 9004 820 290
character_pos 9001 820 350
character_pos 9012 900 370
character_pos 9005 780 390
character_pos 9506 710 300
character_pos 9014 770 320
character_pos 9051 730 340
character_pos 9007 670 330
character_pos 9006 610 310
character_pos 9008 580 360

background_offsetx 1500
run 9011 950 320
run 9002 900 270
run 9004 820 290
run 9001 820 350
run 9012 900 370
run 9005 780 390
run 9506 710 300
run 9014 770 320
run 9051 730 340
run 9007 670 330
run 9006 610 310
run 9008 580 360
talkEX 1000 120 126
wait_complete
end