begin
background 32

;beginskiptalk

; =========================== by the river ========================
background_offsetx 1100

camera_follow 9014
playmusic "bgm/lfo_china2.ogg"

add 9011  11 950 0 360
add 9002  2  900 0 310
add 9004  4  820 0 330

add 9001  1  820 0 390
add 9012 12  900 0 410
add 9005  5  780 0 430

add 9506 13  660 0 340
add 9014 14  720 0 360
add 9051 51  680 0 380

add 9007  7  620 0 370
add 9006  6  560 0 350
add 9008  8  510 0 400
add 9009  9  460 0 370

camera_follow 9014
move 9011 1250 380
move 9002 1200 350
move 9004 1160 370
move 9001 1120 420
move 9012 1200 440
move 9005 1080 460
move 9506 960 370
move 9014 1020 390
move 9051 980 410
move 9007 920 400
move 9006 860 380
move 9008 810 430
move 9009 760 400
talkEX 1000 79 79
wait_complete

faceleft 9001
faceleft 9002
faceleft 9004
faceleft 9005
faceleft 9011
faceleft 9012
talkEX 1000 80 88
wait_complete
end