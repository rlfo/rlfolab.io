begin
background 64

;beginskiptalk

; =========================== by the river ========================
background_offsetx 700

camera_pos 100
camera_follow 9004
playmusic "bgm/lfo_heart.ogg"

add 9008    8  750 0 350
add 9051   51  680 0 380
add 9004    4  620 0 370
add 9002    2  560 0 350
add 9513   86  300 0 370


background_offsetx 900

add 9001  1 820  0 390
add 9005  5 860  0 430
add 9007  7 1000 0 360
add 9009  9 1050 0 380
add 9011 11 880  0 360
add 9012 12 920  0 410
add 9506 13 980  0 420
add 9014 14 950  0 380

background_offsetx 100

move 9008 1050 370
move 9051 980 390
move 9004 900 380
move 9002 820 360
moveEX 9513 710 380 0 
talkEX 1000 138 139
wait_complete

talkEX 1000 140 140
wait_complete


;======== enemies appear =========
playmusic "bgm/lfo_rage.ogg"
camera_pan 700
character_pos 9513 1450 350
faceleft 9513
talkEX 1000 141 142
wait_complete


faceright 9002
talkEX 1000 143 144
wait_complete

faceright 9004
faceright 9051
faceright 9008
talkEX 1000 145 149
wait_complete

;========= other heros appear =========
playmusic "bgm/lfo_china1.ogg"
camera_pan 1300
background_offsetx 900

add 9901 93 700 -300 320
add 9902 93 740 -300 370
add 9903 93 650 -300 400
add 9904 93 850 -300 420
DoAction 9901 733
DoAction 9902 733
DoAction 9903 733
DoAction 9904 733

faceright 9513

faceleft 9001  
faceleft 9005  
faceleft 9007  
faceleft 9009  
faceleft 9011  
faceleft 9012  
faceleft 9506  
faceleft 9014  

Doaction 9001  450
Doaction 9005  450
Doaction 9007  450
Doaction 9009  450
Doaction 9011  450
Doaction 9012  450
Doaction 9506  450
Doaction 9014  450
wait_complete

talkEX 1000 150 151
wait_complete

camera_pan 300

remove 9901
remove 9902
remove 9903
remove 9904
add 9006 6 300 0 360
run 9006 510 360  
run 9002 440 380
run 9008 390 390
run 9051 340 420
run 9004 490 400


endskiptalk

faceleft 9513
talkEX 1000 152 152
wait_complete

faceright 9513
talkEX 1000 153 157
wait_complete

faceleft 9513
talkEX 1000 158 158
wait_complete

faceright 9513
talkEX 1000 159 162
wait_complete

;========== Snake appears =========
camera_pan 250

faceleft 9513
faceleft 9002
faceleft 9004
faceleft 9006
faceleft 9008
faceleft 9051
DoAction 9002 470
DoAction 9004 470
DoAction 9006 470
DoAction 9008 470
runEX 9051 1020 450 1
add 9083 83 100 0 350

move 9083 450 350
talkEX 1000 163 163
character_pos 9513 450 460
faceright 9513
wait_complete

talkEX 1000 164 164
wait_complete


end