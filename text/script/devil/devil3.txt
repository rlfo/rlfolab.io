begin
background 2

;beginskiptalk

background_offsetx 100

camera_follow 9008
playmusic "bgm/lfo_china2.ogg"

add 9008  3333 890 0 420
add 9007  92 840 0 460

move 9008 1350 420   
move 9007 1310 460
wait_complete


background 3
playsound "sfx/julian_sp4-3.wav" 0 100 0

talkEX 1000 3 3
wait_complete


fadein 0 5
background 8
camera_pos 0
wait_complete


add 9008  3333 290 -300 220
add 9007  92 190 -300 260
DoAction 9007 733
wait_complete

DoAction 9007 0
talkEX 1000 4 4
wait_complete

faceleft 9008
talkEX 1000 5 6
wait_complete


camera_follow 9008
move 9004 630 380 
move 9007 700 350  
move 9008 750 320
move 9002 590 360
move 9051 540 370
wait_complete

camera_pan 400
playmusic "bgm/lfo_rage.ogg"
add  9901 150 1200 0 320
add  9904 150 1230 0 350
add  9902 93 1230 0 390
add  9905 150 1260 0 420
add  9903 93  1260 0 440
add  9906 150 1280 0 460

move 9901  970 320
move 9904 1030 350
move 9902  940 390
move 9905 1030 420
move 9903  960 440
move 9906 1050 460
wait_complete

talkEX 1000 7 7
wait_complete

end