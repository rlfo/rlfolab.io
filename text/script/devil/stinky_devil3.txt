begin
background 2

;beginskiptalk

background_offsetx 100

camera_follow 9001
playmusic "bgm/lfo_china2.ogg"

add 9002  2 1390 0 460
add 9051 51 1360 0 470
add 9008  8 1290 0 420
add 9004  4 1300 0 490      
add 9007  7 1240 0 460

add 9001  1 1190 0 480
add 9005  5 1160 0 440
add 9006  6 1060 0 450
add 9014 14 1120 0 470
add 9009  9  990 0 470

talkEX 1000 8 15
wait_complete

move 9002 1490 460
move 9051 1460 470
move 9008 1350 420
move 9004 1370 490      
move 9007 1310 460

move 9001 1250 480
move 9005 1220 440
move 9006 1160 450
move 9014 1200 470
move 9009 1090 470
wait_complete


background 3
playsound "sfx/julian_sp4-3.wav" 0 100 0
add 9001  1 1220 0 480
add 9005  5 1190 0 440
add 9006  6 1120 0 450
add 9014 14 1160 0 470
add 9009  9 1090 0 470 
DoAction 9001 470
DoAction 9005 470
DoAction 9006 470
DoAction 9009 470
DoAction 9014 470

talkEX 1000 16 16
wait_complete

run 9005 1220 440
run 9014 1190 470
run 9001 1170 500
run 9006 1150 450
run 9009 1120 470
talkEX 1000 17 17
wait_complete

fadein 0 5
background 8
camera_pos 0
wait_complete


add 9008  8 290 -300 220
add 9004  4 240 -300 290      
wait_complete
add 9007  7 190 -300 260

add 9002  2 390 -300 260
add 9051 51 340 -300 270
DoAction 9002 910
talkEX 1000 18 21
wait_complete

DoAction 9002 0
faceleft 9002
faceleft 9008
faceleft 9051
talkEX 1000 22 27
wait_complete

moveEX 9007  470 250 1
moveEX 9004  430 280 1
talkEX 1000 28 36
wait_complete

move 9007  420 250 
wait_complete
moveEX 9007  550 250 1 
wait_complete
talkEX 1000 37 45
wait_complete

faceright 9002
faceright 9008
faceright 9051
talkEX 1000 46 47
wait_complete

endskiptalk


camera_follow 9002
move 9004 630 380 
move 9007 750 350  
move 9008 490 320
move 9002 590 360
move 9051 540 370
talkEX 1000 48 50
wait_complete

camera_pan 300
playmusic "bgm/lfo_rage.ogg"
add  9901 150 1200 0 320
add  9904 150 1230 0 350
add  9902 93 1230 0 390
add  9905 150 1260 0 420
add  9903 93  1260 0 440
add  9906 150 1280 0 460

move 9901  970 320
move 9904 1030 350
move 9902  940 390
move 9905 1030 420
move 9903  960 440
move 9906 1050 460

talkEX 1000 52 55
wait_complete

end