begin
background 11

background_offsetx 2300

camera_follow 9004
playmusic "bgm/lfo_bliss.ogg"


add 9008  8 290 0 380
add 9004  4 240 0 450      
add 9007  7 190 0 420
add 9002  2 390 0 420
add 9051 51 340 0 430


background_offsetx 2900
runEX 9008 290 380 1
runEX 9004 240 450 1     
runEX 9007 190 420 1
runEX 9002 390 420 1
runEX 9051 340 430 1

talkEX 1000 79 80
wait_complete

faceright 9004
faceright 9007
faceright 9008
faceright 9051
talkEX 1000 81 82
wait_complete

end