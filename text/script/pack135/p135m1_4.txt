; this file must be in UNICODE format
begin

playmusic "BGM/lfo_beast_2.ogg"

background 26
camera_pos 500
addex 9002 12  850 0 400 0 
addex 9004 7   750 0 400 0 
addex 9005 196 770 0 370 0 
addex 9006 75  630 0 430 0 
addex 9007 77  670 0 420 0 
addex 9003 11  670 0 450 0 
wait_complete
addex 9100 800 850 -50 200 0
doaction 9100 730
playsound "SFX/julian_sp4-3.wav" 0 100 1
wait_timeex 730
addex 9101 800 1050 -250 350 0
doaction 9101 730
playsound "SFX/julian_sp4-3.wav" 0 100 1
wait_timeex 730
addex 9102 800 950 -160 400 0 
doaction 9102 730
playsound "SFX/julian_sp4-3.wav" 0 100 1
wait_timeex 703
addex 9103 800 1150 -300 250 0 
doaction 9103 730
playsound "SFX/julian_sp4-3.wav" 0 100 1
wait_complete

fadeout 1 1
background 27
camera_pos 500
addex 9002 12  850 0 400 0 
addex 9004 7   750 0 400 0 
addex 9005 196 770 0 370 0 
addex 9006 75  630 0 430 0 
addex 9007 77  670 0 420 0 
addex 9003 11  670 0 450 0 
wait_complete
talkex 1000 93 95
wait_complete
runex 9002 1250 400 0 
runex 9004 1250 400 0 
runex 9005 1270 370 0 
runex 9006 1230 430 0 
runex 9007 1270 420 0 
runex 9003 1270 450 0 
wait_timeex 1000
fadeout 0 1
remove 9002
remove 9004
remove 9005
remove 9006
remove 9007
remove 9003
wait_time 1

background 28
camera_pos 800
addex 9002 12  850 0 400 0 
addex 9004 7   750 0 400 0 
addex 9005 196 770 0 370 0 
addex 9006 75  630 0 430 0 
addex 9007 77  670 0 420 0 
addex 9003 11  670 0 450 0 
addex 9010 76  1400 0 400 0
faceleft 9010
wait_complete
runex 9002 1050 400 0 
runex 9004 950 400 0 
runex 9005 970 370 0 
runex 9006 830 430 0 
runex 9007 870 420 0 
runex 9003 870 450 0 
fadein 0 1 

talkex 100 96 99
wait_complete
moveex 9006 1100 430 0
talkex 1000 100 103
wait_complete
talkex 1000 104 104
runex 9002 1160 430 0
wait_complete 
doaction 9002 210 
doaction 9010 8000
wait_complete 
addex 9103 800 1250 0 430 2
doaction 9103 730
playsound "SFX/julian_sp4-3.wav" 0 100 1
wait_timeex 600  
wait_complete 
runex 9002 350 400 0 
runex 9004 350 400 0 
runex 9005 370 370 0 
runex 9006 370 430 0 
runex 9007 370 420 0 
runex 9003 370 450 0 
addex 9100 800 1300 0 200 2
doaction 9100 730
playsound "SFX/julian_sp4-3.wav" 0 100 1
wait_timeex 530
addex 9101 800 1400 0 400 2
doaction 9101 730
playsound "SFX/julian_sp4-3.wav" 0 100 1
wait_timeex 530
addex 9102 800 1500 0 400 2 
doaction 9102 730
playsound "SFX/julian_sp4-3.wav" 0 100 1
wait_timeex 530
fadeout 0 1 
remove 9002
remove 9004
remove 9005
remove 9007
remove 9003
wait_complete
         
 playmusic "BGM/lfo_usa1.ogg"        
         
background 23
camera_pos 750
addex 9002 12  1850 0 400 0 
addex 9004 7   1730 0 400 0 
addex 9005 196 1770 0 370 0 
addex 9006 75  1630 0 430 0 
addex 9007 77  1670 0 420 0 
addex 9003 11  1640 0 450 0 
wait_complete
camera_shake_ex 800 100
runex 9002 1250 400 0 
runex 9004 1130 400 0 
runex 9005 1170 370 0 
runex 9006 1030 370 0 
runex 9007 1070 420 0 
runex 9003 1040 450 0 
wait_timeex 1000
addex 9100 800 1500 0 200 0
doaction 9100 730
playsound "SFX/julian_sp4-3.wav" 0 100 1
wait_timeex 530
addex 9101 800 1550 0 350 0
doaction 9101 730
playsound "SFX/julian_sp4-3.wav" 0 100 1
wait_timeex 530
addex 9102 800 1450 0 450 0 
doaction 9102 730
playsound "SFX/julian_sp4-3.wav" 0 100 1
wait_timeex 530
wait_complete
camera_pos 750
talkex 1000 105 105 
wait_complete  
faceleft 9002
talkex 1000 106 108
wait_complete 
faceleft 9005
wait_complete 
talkex 1000 109 113 
wait_complete 
fadeout 0 1
wait_time 1

end