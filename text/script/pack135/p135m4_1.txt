; this file must be in UNICODE format
begin

background 0

talkex 1000 0 0
wait_complete
playmusic "BGM/lfo_xmas.ogg"
fadeout 0 1
wait_time 1
fadein 0 1

background 20
camera_pos 300

addex 9000 71 500 0 460 0
addex 9002 5012 820 0 400 0
addex 9003 65 760 0 430 0
faceleft 9002
wait_complete
talkex 1000 1 5
wait_complete
moveex 9002 1300 400 0
runex 9003 1300 375 0
wait_timeex 0
talkex 1000 6 6
wait_complete




fadeout 0 1
background 21
wait_complete
fadein 0 1
remove 9002
remove 9003
remove 9000
wait_complete
addex 9000 5014 650 0 420 0
faceleft 9000
addex 9001 54 575 0 400 1
addex 9005 51 530 0 405 2
wait_complete
talkex 1000 7 10
wait_complete
faceleft 9001
wait_complete
doaction 9001 500
wait_complete
talkex 1000 11 11
doaction 9005 0
wait_complete
faceright 9001
talkex 1000 12 15
wait_complete
moveex 9000 1200 420 1
wait_timeex 500
camera_follow 9001
talkex 1000 16 16
wait_complete
camera_follow 9000
talkex 1000 17 17
wait_complete



fadeout 0 1
wait_timeex 500
remove 9000
background 20
fadein 0 1
wait_complete
addex 9000 71 700 0 420 0
camera_follow 9000
faceleft 9000
wait_complete
addex 9004 5081 250 0 440 0
talkex 1000 18 22
wait_complete
runex 9000 350 420 1
wait_complete
talkex 1000 23 28
wait_complete

fadeout 0 1
wait_timeex 800
background 1
camera_pos 0
fadein 0 1
addex 9000 71 320 0 450 0
talkex 1000 29 30
wait_complete
end