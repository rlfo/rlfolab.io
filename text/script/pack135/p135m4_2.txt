; this file must be in UNICODE format
begin

background 22
playmusic "BGM/lfo_xmas.ogg"

camera_pos 1000

addex 9000 71  1050 0 350 0


wait_complete

moveex 9000 1400 350 0
wait_timeex 500
talkex 1000 31 39
wait_complete
addex 9001 3285 1200 -500 550 0
addex 9002 3285 1600 -500 350 0
addex 9003 3285 1300 -500 250 0
wait_complete
faceleft 9002
talkex 1000 40 40
wait_complete
fadeout 0 1
wait_timeex 500
end