; this file must be in UNICODE format
begin

background 16
playmusic "BGM/lfo_greek3.ogg"

camera_pos 700

wait_complete

addex 9000 7   890 0 450 0
addex 9001 11  850 0 425 0
addex 9002 12  880 0 400 0
addex 9003 196 900 0 360 0
addex 9004 75  800 0 360 0
addex 9005 77  850 0 480 0
addex 9014 288 955 0 390 0   
wait_complete
moveex 9000 1300 450 0
moveex 9001 1020 425 0
moveex 9002 1180 400 0
moveex 9003 1150 360 0
moveex 9004 1250 380 0
moveex 9005 1100 480 0
moveex 9014 1355 390 0  

wait_complete
faceleft 9014
wait_timeex 1000
faceright 9014
wait_timeex 1000
faceleft 9014
talkex 1000 73 73
wait_complete
faceleft 9004
faceleft 9000
talkex 1000 74 97
wait_complete
doaction 9004 500
wait_complete
faceleft 9014
talkex 1000 98 98
wait_complete
faceleft 9003
faceleft 9005
faceleft 9002
runex 9001 970 425 0
talkex 1000 99 102
wait_complete
faceright 9003
wait_complete
talkex 1000 103 106
giveitem 9004 506
wait_complete
moveex 9003 1170 382 0
moveex 9000 1300 410 1
moveex 9002 1100 410 1
moveex 9005 1185 410 0
wait_complete
talkex 1000 107 107
wait_complete
fadein 0 1

background 17
playmusic "BGM/lfo_p4s3.ogg"
addex 9102 12  1200 0 350 0 
addex 9104 7   1100 0 425 0 
addex 9105 196 1100 0 390 0 
addex 9106 75  1380 0 405 0 
addex 9107 77  1150 0 360 0 
addex 9103 11  1200 0 405 0 
moveex 9106 1380 405 1
fadeout 0 1
wait_complete 
talkex 1000 108 109
wait_complete
fadein 0 1
fadeout 0 1

background 16
playmusic "BGM/lfo_greek3.ogg"
talkex 1000 110 116
wait_complete
wait_complete
addex 9011 11 500 0 410 0
runex 9011 850 410 0
faceleft 9005
faceleft 9003
talkex 1000 117 118
camera_pan 600
wait_complete
faceleft 9001
wait_timeex 1000
faceright 9001
wait_timeex 1000
talkex 1000 119 119
wait_complete
fadein 1 1
remove 9001
addex 9113 293 970 0 425 0
addex 9200 2108 10 0 0 0
changeholder 9200 9113
addex 9201 293 2500 0 425 0
wait_timeex 200
talkex 1000 120 135
wait_complete
remove 9113 
changeholder 9200 9201
addex 9114 363 970 0 425 0
addex 9115 363 1040 0 375 0
addex 9116 363 1040 0 475 0
addex 9117 363 900  0 375 0
addex 9118 363 900  0 475 0
playsound "SFX/BlastHit.wav" 0 100 1
addex 9210 801 1040 0 375 0
addex 9211 801 1040 0 475 0
addex 9212 801 900  0 375 0
addex 9213 801 900  0 475 0
faceleft 9118
faceleft 9117
doaction 9210 2280 
doaction 9211 2280 
doaction 9212 2280 
doaction 9213 2280 
wait_complete
talkex 1000 136 139
wait_complete
runex 9011 1180 410 0
runex 9002 600 400 0
runex 9114 1020 380 0
runex 9003 1500 425 0
runex 9000 1500 450 0
runex 9004 1500 460 0
runex 9005 1500 500 0
runex 9014 1500 390 0
runex 9115 1120 400 1
runex 9116 1120 455 1 
runex 9117 900 400 0
runex 9118 900 455 0
wait_timeex 290
remove 9011
remove 9002
addex 9500 7010 940 0 400 1
doaction 9500 8900
doaction 9114 8100
wait_timeex 750
doaction 9115 8000
doaction 9116 8000
doaction 9117 8000
doaction 9118 8000
wait_complete
fadeout 0 1
end