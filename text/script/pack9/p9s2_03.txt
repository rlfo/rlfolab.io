; this file must be in UNICODE format

begin
background 47
camera_pos 1200
playmusic "BGM/lfo_p7s3_1.ogg"
addex 9000 7001 1400 0 350 0
addex 9001 5078 1500 0 370 0
faceleft 9000
faceleft 9001

addex 9003 71 1600 0 300 0
addex 9004 7006 1550 0 400 0
addex 9005 196 1580 0 350 0

addex 9002 7004 1730 0 420 0

addex 9010 3213 1950 0 360 0
faceleft 9010

addex 9020 68 1600 0 360 1
addex 9021 68 1500 0 400 1
addex 9022 68 1650 0 420 1
addex 9023 68 1550 0 380 1
addex 9024 68 1500 0 350 1

addex 9031 3208 1750 0 300 1
faceleft 9031
faceleft 9022 
faceleft 9020

doaction 9020 500
doaction 9021 500
doaction 9022 500
doaction 9023 500
doaction 9024 500
doaction 9031 500

doaction 9000 200
doaction 9001 200
doaction 9002 200
doaction 9003 200
doaction 9004 200
doaction 9005 200
wait_timeex 800


faceright 9020
faceright 9022
doaction 9004 300
doaction 9005 300
doaction 9003 8200
faceleft 9002
doaction 9002 100
doaction 9000 0
doaction 9001 0
wait_complete

changeteam 9020 0
changeteam 9021 0
changeteam 9022 0
changeteam 9023 0
changeteam 9024 0
addex 9030 3208 1100 0 350 1
run 9030 1150 350 
doaction 9000 8400
wait_complete

addex 9030 3208 1100 0 350 1
run 9030 1200 350
faceleft 9005
faceleft 9003
doaction 9005 8100
doaction 9000 470
wait_complete

addex 9032 3208 1100 0 300 0
addex 9033 3208 1100 0 450 0
move 9032 1200 300
move 9033 1250 450
talkex 1000 19 20
wait_complete

faceright 9002
doaction 9002 8300
wait_complete

fadeout 1 0
fadein 1 1
wait_time 1
;Maybe Change BG


move 9020 800 400
move 9021 800 400
move 9022 800 400
move 9023 800 400
move 9024 800 400

move 9030 500 400
run 9031 500 400
move 9032 500 400
move 9033 500 400

camera_follow 9001
moveex 9001 1250 370 0
moveex 9000 1350 350 1
moveex 9002 1300 390 1 
moveex 9003 1150 300 0
moveex 9004 1100 400 0
moveex 9005 1220 330 0

moveex 9010 1500 350 1
talkex 1000 21 26
wait_complete
camera_pan 1000 

remove 9020
remove 9021
remove 9022
remove 9023
remove 9024

remove 9030
remove 9031
remove 9032
remove 9033

addex 9040 5079 1900 0 380 1
faceleft 9040
stopmusic
playmusic "BGM\lfo_p85s1_1.ogg"
doaction 9040 460
talkex 1000 27 27 
wait_complete

faceright 9000
faceright 9002
faceright 9010
talkex 1000 28 29
wait_complete

run 9010 500 460
move 9001 1400 370
talkex 1000 30 32
wait_complete

moveex 9040 1700 380 0
talkex 1000 33 33
wait_complete

faceleft 9040
talkex 1000 34 34
addex 9050 800 1600 0 500 0
doaction 9050 730
camera_shake 1000
wait_complete

camera_pan 1000
talkex 1000 35 38
wait_complete

talkex 1000 39 39
run 9002 1430 390
wait_complete

move 9001 1480 370
talkex 1000 40 41
wait_complete

;pickup item
addex 9041 2043 0 0 0 0
changeholder 9041 9040
talkex 1000 42 49
wait_complete

;zack move front
move 9050 1600 380
talkex 1000 50 50
wait_complete

faceleft 9001 
talkex 1000 51 54

faceright 9000
talkex 1000 55 55

fadeout 1 0
wait_complete
fadein 1 0
wait_complete

fadein 1 1
;zack transform
changeholder 9041 0 
remove 9041
talkex 1000 56 56
wait_complete
changesprite 9040 7266
doaction 9040 8050
wait_complete
stopmusic
;playmusic "BGM\lfo_p85s1_1.ogg"
faceright 9001
talkex 1000 57 60
wait_complete

faceright 9040
talkex 1000 61 61
wait_complete

run 9002 1700 370
wait_timeex 200

camera_pan 1200
character_pos 9002 1650 370
character_pos 9040 1900 380
faceleft 9040
run 9003 1450 300
run 9004 1400 400 
run 9005 1350 330
move 9001 1500 370
run 9000 1600 350
talkex 1000 62 63
wait_complete

run 9040 1950 380
wait_complete

remove 9040
talkex 1000 64 65
wait_complete
stopmusic
end