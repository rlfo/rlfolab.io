; this file must be in UNICODE format

begin
background 46
camera_pos 800
playmusic "BGM\lfo_p85s1_1.ogg"
addex 9000 7001 700 0 350 0
addex 9001 5078 680 0 370 0
addex 9002 7004 630 0 310 0
addex 9003 71 600 0 300 0
addex 9004 7006 650 0 400 0
addex 9005 196 620 0 380 0

addex 9010 3213 750 0 360 0

addex 9020 68 1600 0 360 0
addex 9021 68 1500 0 400 0
addex 9022 68 1650 0 420 0
addex 9023 68 1550 0 380 0
addex 9024 68 1500 0 350 0
faceleft 9022 
faceleft 9020

move 9000 1240 350
move 9001 1150 370
move 9002 1100 310
move 9003 1180 300
move 9004 1180 400
move 9005 1050 380

move 9010 1350 360
wait_complete

camera_pan 1000
move 9010 1450 370
faceleft 9021
faceleft 9023
faceleft 9024
talkex 1000 14 15
wait_complete

fadeout 1 0
fadein 1 0
wait_complete

fadeout 1 0
fadein 1 0
wait_complete

background 47
addex 9010 3213 1450 0 370 0

addex 9020 68 1600 0 360 0
addex 9021 68 1500 0 400 0
addex 9022 68 1650 0 420 0
addex 9023 68 1550 0 380 0
addex 9024 68 1500 0 350 0
faceleft 9020
faceleft 9021
faceleft 9022
faceleft 9023
faceleft 9024

addex 9000 7001 1240 0 350 0
addex 9001 5078 1150 0 370 0
addex 9002 7004 1100 0 310 0
addex 9003 71   1180 0 300 0
addex 9004 7006 1180 0 400 0
addex 9005 196  1050 0 380 0

fadeout 1 0
fadein 1 1
stopmusic
playmusic "BGM/lfo_p7s3_1.ogg"
camera_shake 1000
wait_time 1

faceleft 9010
doaction 9010 450
wait_complete

doaction 9020 450
doaction 9022 450
doaction 9023 450
wait_complete

doaction 9010 450
doaction 9022 450
doaction 9024 450
wait_complete

doaction 9020 500
doaction 9024 500 
doaction 9022 500

doaction 9000 200
doaction 9001 200
doaction 9002 200
doaction 9003 200
doaction 9004 200
doaction 9005 200
wait_timeex 500

doaction 9020 500
doaction 9024 500 
doaction 9022 500
wait_timeex 500

doaction 9020 500
doaction 9024 500 
doaction 9022 500
wait_timeex 500

camera_follow 9001
addex 9030 3208 300 0 400 0
addex 9031 3208 2000 0 400 0
addex 9032 3208 350 0 350 0
addex 9033 3208 2050 0 380 0

doaction 9000 0
doaction 9001 0
doaction 9002 0
doaction 9003 0
doaction 9004 0
doaction 9005 0
faceleft 9005
faceleft 9004
faceleft 9002

faceright 9020
faceright 9022

move 9030 700 350
move 9032 800 300
move 9031 1500 300
move 9033 1600 450
talkex 1000 16 18

wait_complete
stopmusic

end