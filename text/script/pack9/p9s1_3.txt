; this file must be in UNICODE format

begin
background 9
playmusic "BGM\lfo_devil2.ogg"
camera_pos 0
addex 9000 1 180 0 355 0 
addex 9001 4 120 0 330 0
addex 9002 71 100 0 375 0
addex 9003 6 30 0 340 0
addex 9004 196 0 0 380 0

wait_complete

moveex 9000 280 355 0 
moveex 9001 220 330 0
moveex 9002 200 375 0
moveex 9003 130 340 0
moveex 9004 100 380 0

wait_complete
talkex 1000 77 79

wait_complete
moveex 9003 290 300 0
wait_complete
doaction 9003 9300
wait_complete
talkex 1000 80 81
wait_complete
addex 9005 3204 900 0 300 1
moveex 9005 550 350 1
wait_timeex 500
moveex 9000 230 355 0 
moveex 9001 170 330 0
moveex 9002 150 375 0
moveex 9003 240  300 0
moveex 9004 50  380 0
wait_complete
talkex 1000 82 83
wait_complete
doaction 9005 8000
doaction 9003 130
doaction 9001 130
doaction 9002 140
doaction 9000 140
doaction 9004 140
wait_complete

talkex 1000 84 84
wait_complete



end