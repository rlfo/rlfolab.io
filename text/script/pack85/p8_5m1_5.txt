; this file must be in UNICODE format

begin

playmusic "bgm/lfo_beast_2.ogg"
background 9
camera_pos 0

addex 9000 15 250 0 320 0
addex 9001 3204 400 0 330 1
faceleft 9001

doaction 9000 8600
doaction 9001 500
wait_complete
doaction 9000 8200
doaction 9001 8000
wait_complete
doaction 9001 8100
wait_complete

doaction 9001 660
talkex 1000 28 29
wait_complete

run 9000 900 450
wait_time 5

end
