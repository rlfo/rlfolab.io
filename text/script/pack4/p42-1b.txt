; this file must be in UNICODE format
begin
background 6

background_offsetx 0

camera_pos 600

playmusic "bgm/lfo_devil5.ogg"

AddEX 9001 194 289 0 461 0
AddEX 9002 2   225 0 383 0
AddEX 9003 5   293 0 431 0
AddEX 9004 4   356 0 489 0
AddEX 9005 1   359 0 359 0
AddEX 9006 196 492 0 470 0
AddEX 9007 3   451 0 386 0
AddEX 9008 260 523 0 440 0

MoveEX 9008 1138 436 0
MoveEX 9007 1066 382 0
MoveEX 9006 1057 466 0
MoveEX 9004 971 485 0
MoveEX 9005 974 355 0
MoveEX 9003 908 427 0
MoveEX 9002 840 379 0
MoveEX 9001 804 457 0
wait_complete

AddEX 9009 189 168 0 525 0
AddEX 9010 189 211 0 348 0
AddEX 9011 190 360 0 509 0
AddEX 9012 190 389 0 361 0
AddEX 9013 189 257 0 416 0
wait_complete

TalkEX 1000 10 13
wait_complete

camera_pan 380

Faceleft 9008
Faceleft 9006
Faceleft 9004
Faceleft 9007
Faceleft 9005
Faceleft 9003
Faceleft 9002
Faceleft 9001

MoveEX 9011 657 523 0
MoveEX 9009 465 539 0
MoveEX 9013 554 430 0
MoveEX 9010 508 362 0
MoveEX 9012 686 375 0
wait_complete

TalkEX 1000 14 17
wait_complete

RunEX 9006 589 463 1
wait_complete

Faceleft 9012
Faceleft 9011
RunEX 9013 712 435 1
RunEX 9009 478 487 0
RunEX 9010 528 405 0
wait_complete

Faceleft 9006
wait_complete

TalkEX 1000 18 21
wait_complete

end