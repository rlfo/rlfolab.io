; this file must be in UNICODE format
begin
background 6

background_offsetx 750

camera_pos 0

playmusic "bgm/lfo_p4s3.ogg"

AddEX 9001 194 208 0 357 0
AddEX 9002 2   516 0 364 0
AddEX 9003 5   379 0 404 0
AddEX 9004 4   659 0 428 0
AddEX 9005 260 533 0 431 0
AddEX 9006 196 284 0 393 0
AddEX 9007 3   177 0 409 0
AddEX 9008 1   461 0 309 0

Faceleft 9005
Faceleft 9006
Faceleft 9004
Faceleft 9008

TalkEX 1000 22 24
wait_complete

end