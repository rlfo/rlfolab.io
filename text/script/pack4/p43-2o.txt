; this file must be in UNICODE format
begin
background 20

playmusic "bgm/lfo_chi3.ogg"

background_offsetx 350

camera_pos 100

AddEX 9112 2 1007 0 458 1
AddEX 9114 4 1087 0 488 1

AddEX 9312 192 1407 0 368 1
AddEX 9314 193 1487 0 448 1

runEX 9112 507 358 0
runEX 9114 597 388 1
wait_complete

DoAction 9114 910
talkex 1000 295 297
wait_complete

camera_pan 230
DoAction 9114 0
faceright 9112
faceright 9114
runEX 9312 757 368 1
runEX 9314 807 398 1
wait_complete


talkex 1000 298 301
wait_complete

end