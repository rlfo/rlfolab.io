; this file must be in UNICODE format
begin
background 34
camera_pos 0

playmusic "bgm/lfo_chi2.ogg"

camera_follow 9001


talkex 1000 122 123
wait_complete

AddEX 9001 172   150 0 400 0
AddEX 9002 176   50 0 420 0
AddEX 9003 5035  700 0 350 0
faceleft 9003


AddEX 9010 183   1500 0 400 1
AddEX 9011 3081  1600 0 380 1
AddEX 9012 3081  1600 0 430 1


talkex 1000 0 3
wait_complete

run 9001 400 400
run 9002 300 420
talkex 1000 4 7
wait_complete


run 9001 700 400
run 9002 600 420
wait_complete

faceright 9003
run 9001 1100 400
run 9002 1000 420
wait_complete

run 9010 1300 400
run 9011 1380 380
run 9012 1400 430
talkex 1000 8 9
wait_complete

Doaction 9010 8100 
Doaction 9011 8000 
Doaction 9012 8000 
wait_time 1

Doaction 9002 733
talkex 1000 10 11
wait_complete

move 9003 200 350
talkex 1000 12 13
wait_complete

camera_pan 0

AddEX 9004 5029   0 0 350 0
move 9004 120 350
talkex 1000 14 18
wait_complete


wait_time 1

end