; this file must be in UNICODE format
begin
background 20

background_offsetx 130

playmusic "bgm/lfo_intro2_2.ogg"

camera_pos 480

talkex 1000 1 1
wait_complete

AddEX 9001 1   305 0 461 0
AddEX 9002 2   153 0 457 0
AddEX 9003 3   196 0 453 0
AddEX 9004 4   242 0 461 0
AddEX 9005 5   282 0 460 0
AddEX 9009 197 167 0 464 0
AddEX 9015 196 187 0 444 0
AddEX 9010 68  193 0 465 0
AddEX 9011 194 91 0 467 0
AddEX 9012 8   264 0 473 0
AddEX 9013 54  304 0 468 0
AddEX 9014 71  401 0 457 0
AddEX 9006 6   1134 0 343 0
AddEX 9007 9   1193 0 360 0
AddEX 9008 7   1078 0 358 0
wait_complete

Faceleft 9007
Faceleft 9006
Faceleft 9008
wait_complete

MoveEX 9014 856 374 0
MoveEX 9013 760 374 0
MoveEX 9004 698 373 0
MoveEX 9001 932 360 0
MoveEX 9005 886 327 0
MoveEX 9003 640 333 0
MoveEX 9002 718 332 0
MoveEX 9012 815 334 0
RunEX 9010 626 367 0
RunEX 9009 590 366 0
RunEX 9015 570 326 0
RunEX 9011 525 367 0
wait_complete

talkex 1000 2 11
wait_complete

Fadeout 0 2
wait_time 2

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

background 21

background_offsetx 200

camera_pos 400

AddEX 9011 1   595 0 456 0
AddEX 9012 2   646 0 471 0
AddEX 9013 3   581 0 407 0
AddEX 9014 4   520 0 443 0
AddEX 9001 5   696 0 470 0
AddEX 9002 197 978 0 456 0
AddEX 9015 196 677 0 404 0
AddEX 9003 68  943 0 447 0
AddEX 9004 194 928 0 431 0
AddEX 9005 8   889 0 454 0
AddEX 9006 54  862 0 428 0
AddEX 9007 71  820 0 449 0
AddEX 9008 6   797 0 410 0
AddEX 9009 9   764 0 464 0
AddEX 9010 7   735 0 427 0   

AddEX 9016 5013 1100 0 448 0
Faceleft 9016                                          
wait_complete

talkex 1000 12 49
wait_complete

MoveEX 9014 742 493 1
wait_complete

talkex 1000 50 50
wait_complete

talkex 1000 51 64
wait_complete

RunEX 9002 720 462 1
RunEX 9003 685 443 1
wait_complete

RunEX 9002 369 463 1
RunEX 9003 334 444 1
RunEX 9012 295 466 1
wait_complete

talkex 1000 65 68
wait_complete

Faceleft 9011

RunEX 9001 229 448 0
RunEX 9004 327 454 0
RunEX 9005 309 467 0
RunEX 9006 281 451 0
RunEX 9007 250 464 0
RunEX 9008 216 453 0
RunEX 9009 184 461 0
RunEX 9010 154 450 0
RunEX 9014 113 463 1
RunEX 9015 113 463 1
RunEX 9013 10 469 0
wait_complete

talkex 1000 69 70
wait_complete

Faceright 9011

talkex 1000 71 76
wait_complete

RunEX 9011 10 469 0
talkex 1000 77 78
wait_complete

Fadeout 0 2
wait_time 2

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

background 20

background_offsetx 0

camera_pos 520

AddEX 9001 5013 1220 0 447 0
AddEX 9002 197  1165 0 436 0
AddEX 9003 68   1147 0 455 0

AddEX 9004 71   562 0 409 0
AddEX 9005 6    613 0 401 0
AddEX 9006 9    656 0 418 0
AddEX 9007 7    713 0 438 0
AddEX 9008 194   762 0 409 0
AddEX 9009 8    769 0 440 0
AddEX 9010 3    823 0 450 0
AddEX 9016 196  863 0 409 0

AddEX 9011 4     1051 0 420 0
AddEX 9012 5     1016 0 457 0
AddEX 9013 1     977 0 447 0
AddEX 9014 2     933 0 459 0
AddEX 9015 54    873 0 449 0
wait_complete
         
Faceleft 9001
Faceleft 9002
Faceleft 9003         
           
talkex 1000 79 84
wait_complete

MoveEX 9011 1051 470 0
MoveEX 9013 878 469 0

MoveEX 9010 843 450 0
MoveEX 9015 578 469 0
MoveEX 9008 899 449 0
MoveEX 9016 979 449 0
wait_complete

; 9099 - teleporter
addEX 9099 4000 970 0 440 0


talkex 1000 85 92
wait_complete

playsound "sfx/biggun.wav" 0 100 0

remove 9008
remove 9010
remove 9011
remove 9012
remove 9013
remove 9014
remove 9016

Fadeout 1 4
wait_time 4

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

background 0

playmusic "bgm/lfo_p4s1.ogg"

background_offsetx -300

camera_pos 300

remove 9001
remove 9002
remove 9003
remove 9011
remove 9012
remove 9013
remove 9014
remove 9015

wait_time 1

; 9099 - teleporter
addEX 9099 4000 720 0 420 0

playsound "sfx/biggun.wav" 0 100 0

wait_time 1


AddEX 9004 4    592 0 449 0
AddEX 9005 5    633 0 441 0
AddEX 9006 1    676 0 458 0
AddEX 9007 2    713 0 438 0
AddEX 9008 194  762 0 439 0
AddEX 9010 3    833 0 450 0
AddEX 9016 196  863 0 449 0

wait_time 1

end