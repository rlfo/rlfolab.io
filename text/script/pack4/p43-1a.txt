; this file must be in UNICODE format
begin

playmusic "bgm/lfo_greek1.ogg"

background 21

background_offsetx 800

camera_pos -400

AddEX 9001 5028 822 0 383 0
AddEX 9002 7    707 0 395 0
AddEX 9003 5013 874 0 390 0
AddEX 9004 198  823 0 368 0
AddEX 9005 260  839 0 420 0
AddEX 9006 3    640 0 383 0
AddEX 9007 2    559 0 380 0
AddEX 9008 1    485 0 345 0
AddEX 9009 196  572 0 335 0
AddEX 9010 194  627 0 339 0
AddEX 9011 4    692 0 347 0
AddEX 9012 5    763 0 349 0
AddEX 9013 197  994 0 376 0
AddEX 9014 68   974 0 364 0
AddEX 9015 8    1026 0 377 0
AddEX 9016 54   1067 0 357 0
AddEX 9017 71   1088 0 407 0
AddEX 9018 6    1148 0 368 0
AddEX 9019 9    883 0 338 0

Faceleft 9015
Faceleft 9016
Faceleft 9017
Faceleft 9018
Faceleft 9019
Faceleft 9014
Faceleft 9013
Faceleft 9004
Faceleft 9005
Faceleft 9003

Doaction 9001 600
wait_complete

camera_pos 400

fadein 0 3
wait_time 3

talkEX 1000 243 243
wait_complete

talkEX 1000 1 17
wait_complete

RunEX 9002 300 400 1
RunEx 9010 250 400 1
wait_complete

talkEX 1000 18 27
wait_complete

remove 9001
remove 9002
remove 9003
remove 9004
remove 9005
remove 9006
remove 9007
remove 9008
remove 9009
remove 9010
remove 9011
remove 9012
remove 9013
remove 9014
remove 9015
remove 9016
remove 9017
remove 9018
remove 9019

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
background 22

playmusic "bgm/lfo_heart.ogg"

background_offsetx 0
camera_pos 230     
    
AddEX 9006 3 700 0 333 0
AddEX 9007 2 159 0 330 0

Faceright 9006

fadein 0 3
wait_time 3

MoveEx 9007 570 340 0
wait_complete

talkEX 1000 28 28
wait_complete

Faceleft 9006

talkEX 1000 29 49
wait_complete

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
background 21

playmusic "bgm/lfo_greek1.ogg"

background_offsetx 800
camera_pos 400 
 
fadein 0 3
wait_time 3

AddEX 9001 5013  1154 0 358 0
AddEX 9002 7     959 0 361 0
AddEX 9003 9     899 0 352 0
AddEX 9004 198   826 0 343 0
AddEX 9005 260   757 0 335 0
AddEX 9006 3     693 0 339 0
AddEX 9007 2     625 0 335 0
AddEX 9008 1     559 0 338 0
AddEX 9009 196   493 0 342 0
AddEX 9010 194   996 0 409 0
AddEX 9011 4     935 0 401 0
AddEX 9012 5     880 0 397 0
AddEX 9013 197   794 0 386 0
AddEX 9014 68    719 0 401 0
AddEX 9015 8     648 0 390 0
AddEX 9016 54    587 0 395 0
AddEX 9017 71    522 0 404 0
AddEX 9018 6     460 0 411 0
               
Faceleft 9001              

talkEX 1000 50 51
wait_complete

MoveEx 9006 1050 370 0
MoveEx 9007 1020 405 0
MoveEx 9010 1100 395 0
Moveex 9012 1090 457 0
wait_complete

talkEX 1000 52 60
wait_complete

AddEX 9021 74  287 0 555 0
AddEX 9020 262 222 0 504 0
AddEX 9019 72  160 0 591 0
     
talkEX 1000 61 61    
MoveEx 9019 800 450 0
MoveEx 9020 870 455 0
MoveEx 9021 980 435 0
wait_complete

Faceleft 9002
Faceleft 9006
Faceleft 9007
Faceleft 9010
Faceleft 9011
Faceleft 9012

talkEX 1000 62 83
wait_complete

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
background 0

playmusic "bgm/lfo_school1.ogg"

background_offsetx 0
camera_pos 1300 

fadein 1 2
wait_time 2

AddEX 9001 1     222 0 405 0
AddEX 9002 2     304 0 374 0
AddEX 9003 3     370 0 417 0
AddEX 9004 4     400 0 386 0
AddEX 9005 5     472 0 393 0
AddEX 9006 7     530 0 427 0
AddEX 9007 72    592 0 385 0

camera_pan 0 

talkEX 1000 84 84
wait_complete

end