; this file must be in UNICODE format
begin
background 2

playmusic "bgm/lfo_greek2.ogg"

background_offsetx 950

camera_pos 100

AddEX 9001 55 247 0 412 0
AddEX 9002 4  312 0 411 0
AddEX 9003 51 252 0 377 0
AddEX 9004 2  335 0 362 0
AddEX 9005 5  186 0 406 0
AddEX 9006 1  424 0 361 0
AddEX 9007 3  378 0 393 0

talkEX 1000 27 29
wait_complete

runEX 9006 494 361 0
wait_complete

talkEX 1000 30 33
wait_complete

talkEX 1000 34 35
wait_complete

runEX 9007 430 401 0
wait_complete

talkEX 1000 36 36
wait_complete

faceleft 9006

talkEX 1000 37 41
wait_complete

runEX 9006 534 361 0
wait_complete

talkEX 1000 42 42
wait_complete

faceleft 9006

talkEX 1000 43 43
wait_complete

talkEX 1000 44 45
wait_complete

moveEX 9006 624 351 0
wait_complete

moveEX 9007 524 411 0
wait_complete

talkEX 1000 46 47
wait_complete

moveEX 9006 744 321 1
wait_complete

wait_time 1

remove 9006

playsound "sfx/gate.wav" 0 100 0

wait_time 1

talkEX 1000 48 48
wait_complete

talkEX 1000 176 180
wait_complete

Fadeout 1 1
wait_time 1

;;;;;;;;;;;;;;;;;;;
;cannot wait for davis

remove 9001
remove 9002
remove 9003
remove 9004
remove 9005
remove 9006
remove 9007

AddEX 9001 2  347 0 422 0
AddEX 9003 51 422 0 377 0
AddEX 9005 55 306 0 396 0
AddEX 9007 3  568 0 403 0
AddEX 9004 5  505 0 362 0
AddEX 9002 4  612 0 431 0

faceleft 9007
faceleft 9004
faceleft 9002

talkEX 1000 181 184
wait_complete

;;;;;;;;;;;;;;;;;;;
;lots of enemy comes out

AddEX 9011 15  1000 0 300 1
AddEX 9012 195 1000 0 350 1
AddEX 9013 199 1000 0 400 1
AddEX 9014 263 50 0 300 1
AddEX 9015 261 50 0 350 1
AddEX 9016 192 50 0 400 1

playmusic "bgm/lfo_rage.ogg"

runex 9011 800 300 1
runex 9012 750 350 1
runex 9013 700 400 1
runex 9014 250 300 0
runex 9015 290 350 0 
runex 9016 200 400 0 
wait_complete

faceleft 9001
faceleft 9003
faceleft 9005
faceright 9007
faceright 9004
faceright 9002

talkEX 1000 185 193
wait_complete

end
