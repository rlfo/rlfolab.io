; this file must be in UNICODE format
begin

background 10

background_offsetx 400

camera_pos 0

playmusic "bgm/lfo_normal.ogg"

AddEX 9001 194 642 0 441 0
AddEX 9002 196 254 0 428 0
AddEX 9003 5   385 0 455 0
AddEX 9004 4   450 0 407 0
AddEX 9005 3   469 0 477 0
AddEX 9006 2   525 0 421 0
AddEX 9007 1   574 0 460 0

AddEX 9008 199 -215 0 451 1

AddEX 9009 1 1026 0 449 0
AddEX 9010 1 1002 0 404 0

Faceleft 9001

TalkEX 1000 58 66
wait_complete

playmusic "bgm/lfo_courage.ogg"

Faceleft 9007
Faceleft 9006
Faceleft 9005
Faceleft 9004
Faceleft 9003
Faceleft 9002

camera_pan -300

wait_time 1

MoveEX 9008 76 437 0
wait_complete

camera_pan -100

TalkEX 1000 67 70
wait_complete

RunEX 9002 164 443 1
wait_complete

TalkEX 1000 71 72
wait_complete

Doaction 9008 8000
wait_complete

Doaction 9002 733
wait_complete

Doaction 9008 470
wait_complete

TalkEX 1000 73 75
wait_complete

DoAction 9008 8100
wait_complete

Doaction 9002 910
wait_complete

TalkEX 1000 76 79
wait_complete

end