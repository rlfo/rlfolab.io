; this file must be in UNICODE format
begin
background 13

background_offsetx 100

camera_pos 50

playmusic "bgm/lfo_advent.ogg"

AddEX 9001 194 43 0 455 0
AddEX 9002 196 110 0 374 0
AddEX 9003 5   152 0 415 0
AddEX 9004 4   208 0 435 0
AddEX 9005 3   240 0 391 0
AddEX 9006 2   283 0 459 0
AddEX 9007 260 336 0 398 0
AddEX 9008 1   57 0 387 0

AddEX 9009 198 630 0 350 0

Faceleft 9009

Doaction 9009 8099
wait_complete

RunEX 9001 143 455 0
RunEX 9002 210 374 0
RunEX 9003 252 415 0
RunEX 9004 308 435 0
RunEX 9005 340 391 0
RunEX 9006 383 459 0
RunEX 9007 436 398 0
RunEX 9008 157 387 0
wait_complete



TalkEX 1000 23 24
wait_complete

Doaction 9009 740
wait_complete

TalkEX 1000 25 56
wait_complete

MoveEX 9006 10 474 1
MoveEX 9005 10 406 1
MoveEX 9004 10 450 1
MoveEX 9003 10 430 1
MoveEX 9002 10 389 1
MoveEX 9008 10 402 1
MoveEX 9001 10 470 1
wait_complete

TalkEX 1000 57 57
wait_complete

end

