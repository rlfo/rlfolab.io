; this file must be in UNICODE format
begin
background 10

background_offsetx 0

playmusic "bgm/lfo_advent.ogg"

camera_pos 0
AddEX 9001 194 179 0 408 0
AddEX 9003 5   375 0 375 0
AddEX 9004 4   447 0 386 0
AddEX 9005 3   247 0 450 0
AddEX 9006 196 113 0 409 2
AddEX 9002 2   344 0 420 0
AddEX 9007 1   257 0 381 0

AddEX 9008 199 506 0 450 1

AddEX 9009 260 960 0 408 0
AddEX 9010 198 1083 0 456 0

Faceleft 9008

talkex 1000 80 83
wait_complete

DoAction 9008 8200
DoAction 9005 210
talkex 1000 84 87
wait_complete

RunEX 9002 392 449 0
wait_complete

Doaction 9002 8200
Doaction 9008 200 
runEX 9005 200 360 0
wait_complete

Doaction 9002 470
wait_complete

wait_time 1

AddEX 9901 817 520 -50 456 0
Doaction 9901 201 
wait_complete

Doaction 9008 740
wait_complete

wait_time 1
talkex 1000 88 91
wait_complete

Doaction 9008 100
wait_complete

;Doaction 9002 200 
Doaction 9008 8300 
wait_complete

Doaction 9002 0 
talkex 1000 92 92
wait_complete

RunEX 9010 755 465 1
RunEX 9009 722 417 1
wait_complete

talkex 1000 93 96
wait_complete


RunEX 9002 204 440 0
wait_complete

;Doaction 9008 8000
Doaction 9002 8100
wait_complete


faceleft 9002
RunEX 9008 600 451 0
wait_complete

DoAction 9008 0
wait_complete

Doaction 9010 510 
wait_complete

Doaction 9010 8300 
wait_complete

talkex 1000 97 101
wait_complete

RunEX 9008 20 451 1
wait_complete

remove 9008

RunEX 9002 400 451 1
wait_complete

talkex 1000 102 102
wait_complete

Faceleft 9002

talkex 1000 103 111
wait_complete

Giveitem 9001 405

talkex 1000 112 136
wait_complete

RunEX 9002 143 405 1
wait_complete

talkex 1000 137 137
Doaction 9002 500
wait_complete

Faceright 9002

talkex 1000 138 156
wait_complete

runex 9001 859 408 0
runex 9003 855 405 0
runex 9004 857 406 0
runex 9005 857 400 0
runex 9006 853 409 0
runex 9002 854 400 0
runex 9007 857 401 0
runex 9008 856 400 0
runex 9009 850 408 0
wait_complete

talkex 1000 157 159
wait_complete

Faceleft 9010

runex 9010 853 406 0
wait_complete

Fadeout 1 5
wait_time 5

end