; this file must be in UNICODE format
begin

background 16
camera_pos 400

playmusic "BGM\lfo_p6s1_2.ogg"

;9001 迪倫,9003 卓男,9004 龍翼EX, 9030 奧龍, 9021 黑羅剎,9023 蓮娜, 9025 烏邪,9028 魔姥, 9032 巫神,9020 夜剎
;9050~9051 巨靈守神, 9055~9057 機械人

addex 9001 191 1155 0 470 0
addex 9003 195 1180 0 390 0
addex 9004 15 1225 0 440 0
addex 9021 5055 1360 0 420 0
addex 9030 192 1080 0 480 0
addex 9032 5048 1400 0 460 0
addex 9020 5047 1310 0 470 0
addex 9028 5050 1440 0 400 0
addex 9023 193 1115 0 430 0
addex 9025 5052 1270 0 385 0

faceleft 9001
faceleft 9003
faceleft 9004
faceleft 9021
faceleft 9030
faceleft 9032
faceleft 9028
faceleft 9020
faceleft 9025
faceleft 9023


moveex 9001 855 470 1
moveex 9003 880 390 1
moveex 9004 925 440 1
moveex 9021 1060 420 1
moveex 9030 780 480 1
moveex 9032 1100 460 1
moveex 9020 1010 470 1
moveex 9028 1140 400 1
moveex 9023 815 430 1
moveex 9025 970 385 1

wait_complete

addex 9050 3155 150 0 430 1
addex 9051 3155 200 0 530 1
addex 9055 3156 350 0 420 1
addex 9056 3141 300 0 480 1

moveex 9050 470 430 0
moveex 9051 520 530 0
moveex 9056 620 480 0
wait_timeex 1000
moveex 9055 670 420 0

wait_complete
talkex 1000 60 61
wait_complete

end