; this file must be in UNICODE format
begin

background 9
camera_pos 680

playmusic "BGM\lfo_p6s1_2.ogg"

;9001 迪倫,9003 卓男,9004 龍翼EX, 9030 奧龍, 9021 黑羅剎, 9010~9011 鳳凰,9013 uranus_crow,9040~9045 小動物,9023 蓮娜, 9025 烏邪
;9028 魔姥, 9032 巫神 ,9020 夜剎

addex 9001 191 1150 0 320 0
addex 9003 195 1110 0 260 0
addex 9004 15 1090 0 360 0
addex 9021 5055 1350 0 330 0
addex 9030 192 1430 0 350 0
addex 9032 5048 1365 0 270 0
addex 9028 5050 1330 0 370 0
addex 9020 5047 1250 0 335 0
addex 9025 5052 1290 0 275 0
addex 9023 193 1460 0 280 0


faceleft 9001
faceleft 9003
faceleft 9004
faceleft 9021
faceleft 9030
faceleft 9032
faceleft 9028
faceleft 9020
faceleft 9025
faceleft 9023

moveex 9001 950 320 1
moveex 9003 910 260 1
moveex 9004 890 360 1
moveex 9021 1150 330 1
moveex 9030 1230 350 1
moveex 9032 1165 270 1
moveex 9028 1130 370 1
moveex 9020 1050 335 1
moveex 9025 1090 275 1
moveex 9023 1260 280 1

wait_complete

addex 9010 3150 600 0 270 1
addex 9011 3150 530 0 335 1
addex 9013 3145 1550 0 300 1
addex 9040 3064 1600 0 250 1
addex 9041 3064 1650 0 320 1
addex 9042 3157 1525 0 330 1
addex 9043 3157 1600 0 360 1

moveex 9010 800 270 0
moveex 9011 730 335 0
moveex 9013 1350 300 1
moveex 9040 1400 250 1
moveex 9041 1450 320 1
moveex 9042 1325 330 1
moveex 9043 1400 360 1

wait_complete
wait_timeex 300
faceright 9030
faceright 9021
faceright 9032
faceright 9028
faceright 9020
faceright 9025
faceright 9023

wait_complete

talkex 1000 39 43
wait_complete


end