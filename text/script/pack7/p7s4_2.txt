; this file must be in UNICODE format
;9000 davis_r(7001),9001 dannis_r(7004),9002 anya(261),9003 jessie(71)
;9020 bloodmad(3203)

begin

background 3
playmusic "BGM\lfo_p5s6_2.ogg"
camera_pan 350                     

addex 9000 7001 300 0 420 0
addex 9001 7004 380 0 410 0
addex 9002 261 230 0 400 0
addex 9003 71 150 0 405 0

runex 9000 620 420 0
runex 9001 700 410 0
runex 9002 550 400 0
runex 9003 470 405 0

wait_complete

talkex 1000 23 24
wait_complete

addex 9020 3203 1000 0 300 1
faceleft 9020
moveex 9020 1000 415 1

wait_complete


talkex 1000 25 28
wait_complete

faceleft 9000
talkex 1000 29 30
wait_complete

faceright 9000
talkex 1000 31 37
wait_complete

moveex 9000 820 410 0
wait_complete
moveex 9001 620 415 0
wait_complete
talkex 1000 38 40

wait_complete




end