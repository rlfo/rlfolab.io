;this file must be in UNICODE format
;9000 davis_r(7001),9001 dannis_r(7004),9002 anya(261),9003 jessie(71)
;9004 woody_r(7005),9005 deep_r(7008),9006 henry_r(7006),9007 rudolf_r(7009)
;9008 milo_r (7054),9009 vector_r (7051),9010 ricky (55),9011 cutie_r (7003)
;9012 firen(11),9013 freeze(12),9014 gaia(263),9015 uranus(265),9016 john(7007)
;9017~9019 knight(56),9100 firzen_r(7010)
;
;9020 bloodmad(3203),9021 nina(193),9022 owen_r(7192),9023 dilun(191)
;9035~9045 omegac(187),9025 npc_Hector(5055),9031 迪諾夫(5027)
;9026~9030 5亂魔人(5045,5044,5046,5049,5051)
;9050 bomb(800->2730),9056 fire(819->1600),9057 box(5604)
;9060 heart(5023)






begin
                    
background 15
playmusic "BGM\lfo_p7s3_1.ogg"
background_offsetx -800

camera_pos 1700

addex 9000 7002 1900 0 375 1
addex 9001 7001 1850 0 385 1

;addex 9901 3170 1950 0 380 2
addex 9902 3170 2100 0 380 2

;doaction 9901 8100
doaction 9902 8100

addex 9050 5076 2300 0 380 0
faceleft 9050
doaction 9050 9000

wait_complete

doaction 9000 8750
doaction 9001 8300
doaction 9050 8100
camera_shake 1700

wait_timeex 500

doaction 9000 9000
doaction 9001 9500
doaction 9050 9000

wait_complete

addex 9950 800 2000 -50 380 0
doaction 9950 2730
addex 9951 800 2500 0 300 0
doaction 9951 2730
addex 9952 800 2400 0 410 0
doaction 9952 2730
addex 9953 800 2100 -100 380 0
doaction 9953 2730
addex 9954 800 2200 100 300 0
doaction 9954 2730

wait_complete
wait_time 1

addex 9955 800 2150 -50 380 0
doaction 9955 2730
addex 9956 800 2000 -100 300 0
doaction 9956 2730
addex 9957 800 2050 -200 410 0
doaction 9957 2730
addex 9958 800 2500 -100 380 0
doaction 9958 2730
addex 9959 800 2300 -100 300 0
doaction 9959 2730

wait_complete


wait_timeex 500
camera_pos 1700
wait_complete
talkex 1000 117 117

wait_complete
wait_time 1

doaction 9000 0
doaction 9001 0
changesprite 9050 5077
doaction 9050 9300
wait_complete
talkex 1000 118 120

wait_complete

runex 9000 2100 375 0
wait_complete

changeteam 9001 0

addex 9100 3205 2400 0 380 0
faceleft 9100
doaction 9100 9734
wait_Complete
doaction 9100 500

;doaction 9050 8200
doaction 9000 200
wait_complete

doaction 9100 734
wait_complete

remove 9100

runex 9050 2600 380 0
wait_timeex 500
doaction 9050 0
talkex 1000 121 121
wait_complete
runex 9000 2600 375 0
wait_complete
moveex 9001 2000 385 0
talkex 1000 122 122
wait_complete

addex 9002 7004 1400 0 360 0
doaction 9002 9000 
wait_complete
talkex 1000 123 124
wait_complete

runex 9002 2790 360 0
runex 9001 2790 385 0

background_offsetx -1200

addex 9003 7008 1720 0 340 0
addex 9004 7006 1800 0 375 0
addex 9005 7009 1730 0 400 0
addex 9006 7054 1400 0 355 0
addex 9007 55 1300 0 370 0
addex 9008 7051 1350 0 405 0

faceleft 9003
faceleft 9004
faceleft 9005

addex 9009 7005 1580 0 375 0
faceleft 9009
doaction 9009 733
addex 9010 7007 1550 0 370 0
doaction 9010 9300

wait_timeex 500

camera_pan 1150
wait_complete
talkex 1000 125 126
wait_complete

end
