; this file must be in UNICODE format
begin
background 10
playmusic "bgm/lfo_advent.ogg"

background_offsetx -630

camera_pos 630

; 9001 davis  9002 stinky 9003 cutie
; 9004 dennis 9005 woody  9112 kiki
; 9113 jace   9068 bochai
addEX 9005 5 1260 0 462 1
addEX 9002 2 1120 0 400 1
addEX 9113 194 990 0 442 1
addEX 9001 1 1000 0 404 1
addEX 9068 68 840 0 445 1
addEX 9003 3 820 0 398 1
addEX 9004 4 790 0 422 1
addEX 9112 197 860 0 413 1    
; 9014-Fox
addEX 9014 14 1210 0 435 1
; 9196-paco
addEX 9196 196 1350 0 414 1
; 9092-swat team A
;addEX 9200 3092 990 0 485 1
;addEX 9201 3092 1070 0 474 1
;addEX 9202 3092 1130 0 483 1
; 9526-president
;addEX 9526 5025 930 0 400 1 

Faceleft 9005
Faceleft 9002
Faceleft 9113
Faceleft 9001
Faceleft 9068
Faceleft 9003
Faceleft 9004
Faceleft 9112
Faceleft 9014
Faceleft 9196
Faceleft 9200
Faceleft 9201
Faceleft 9202
Faceleft 9526

playsound "sfx/gate.wav" 0 100 0

talkEX 1000 142 142
wait_complete

talkEX 1000 302 302
wait_complete

talkEX 1000 144 144
wait_complete

end