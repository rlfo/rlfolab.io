; this file must be in UNICODE format
begin
         
background 10
playmusic "bgm/lfo_advent.ogg"

background_offsetx -250

camera_pos 630

; 9001 davis  9002 stinky 9003 cutie
; 9004 dennis 9005 woody  9112 kiki
; 9113 jace   9068 bochai
addEX 9005 5 1360 0 562 1
addEX 9002 2 1280 0 500 1
addEX 9113 194 1290 0 542 1
addEX 9001 1 1260 0 504 1
addEX 9068 68 1350 0 545 1
addEX 9003 3 1320 0 528 1
addEX 9004 4 1430 0 542 1
addEX 9112 197 1380 0 513 1    
; 9196-paco
addEX 9196 196 1450 0 514 1
; 9095-fake magnet devil
addEX 9095 3096 820 0 430 0

Faceleft 9005
Faceleft 9002
Faceleft 9113
Faceleft 9001
Faceleft 9068
Faceleft 9003
Faceleft 9004
Faceleft 9112
Faceleft 9014
Faceleft 9196
Faceleft 9200
Faceleft 9201
Faceleft 9202
Faceleft 9526

MoveEX 9005 1260 512 1
MoveEX 9002 1220 400 1
MoveEX 9113 1200 532 1
MoveEX 9001 1100 444 1
MoveEX 9068 1150 495 1
MoveEX 9003 1120 528 1
MoveEX 9004 1330 512 1
MoveEX 9112 1180 463 1  
MoveEX 9014 1280 405 1
MoveEX 9196 1350 464 1
wait_complete

talkEX 1000 316 317
wait_complete

end