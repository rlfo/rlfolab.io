; this file must be in UNICODE format
begin
background 16
playmusic "bgm/lfo_rage.ogg"

background_offsetx -230

camera_pos 970

; 9001 davis  9002 stinky 9003 cutie
; 9004 dennis 9005 woody  9112 kiki
; 9113 jace   9068 bochai
addEX 9005 5 1310 0 285 1
addEX 9002 2 1230 0 280 1
addEX 9113 194 1200 0 388 1
addEX 9001 1 1260 0 315 1
addEX 9068 68 1150 0 388 1
addEX 9003 3 1160 0 283 1
addEX 9004 4 1070 0 262 1
addEX 9112 197 1120 0 398 1
; 9014-Fox
addEX 9014 14 1180 0 340 1
; 9196-paco
addEX 9196 196 1340 0 338 0
; 9092-swat team A
addEX 9200 3092 950 0 410 1
addEX 9201 3092 1020 0 340 1
addEX 9202 3092 980 0 370 1
; 9016-louisEX
addEX 9016 15 1510 0 350 1
; 9526-president
addEX 9526 5025 1590 0 280 1

Faceleft 9016
Faceleft 9526

talkEX 1000 155 156
wait_complete

RunEX 9196 1390 348 0
wait_complete

talkEX 1000 157 158
wait_complete

RunEX 9016 1510 310 1
wait_complete

Doaction 9196 8000
wait_complete

talkEX 1000 159 159
wait_complete

RunEX 9016 1510 320 1
wait_complete

Doaction 9016 8200
wait_complete

wait_time 1

talkEX 1000 160 160
MoveEX 9196 1050 302 0
wait_complete

RunEX 9196 1100 318 0
wait_complete

talkEX 1000 161 161
wait_complete

RunEX 9005 1390 285 0
wait_complete

talkEX 1000 162 166
wait_complete

end