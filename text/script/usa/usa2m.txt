; this file must be in UNICODE format
begin
background 22
playmusic "bgm/lfo_bliss.ogg"

background_offsetx -550

camera_pos 617

; 9001 davis  9002 stinky 9003 cutie
; 9004 dennis 9005 woody  9112 kiki
; 9113 jace   9068 bochai
addEX 9005 5 960 0 455 1
addEX 9002 2 890 0 440 1
addEX 9113 194 810 0 464 1
addEX 9001 1 930 0 415 1
addEX 9068 68 740 0 445 1
addEX 9003 3 800 0 418 1
addEX 9004 4 690 0 452 1
addEX 9112 197 760 0 443 1    
; 9014-Fox
addEX 9014 14 1230 0 470 0
; 9196-paco
addEX 9196 196 1180 0 440 0
; 9092-swat team A
addEX 9200 3092 1000 0 460 0
addEX 9201 3092 1070 0 470 0
addEX 9202 3092 1030 0 480 0
; 9526-president
addEX 9526 5025 1130 0 450 0

talkEX 1000 207 207
wait_complete

camera_shake 650
wait_time 1
camera_pos 600

talkEX 1000 208 216
wait_complete

Fadeout 1 5
wait_time 1

end