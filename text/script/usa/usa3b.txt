; this file must be in UNICODE format
begin
background 32
playmusic "bgm/lfo_advent.ogg"

background_offsetx 480

camera_pos 570


; 9001 davis  9002 stinky 9003 cutie
; 9004 dennis 9005 woody  9112 kiki
; 9113 jace   9068 bochai
addEX 9005 5 860 0 462 1
addEX 9002 2 900 0 450 1
addEX 9113 194 700 0 442 1
addEX 9001 1 980 0 464 1
addEX 9068 68 810 0 445 1
addEX 9003 3 740 0 398 1
addEX 9004 4 810 0 382 1
addEX 9112 197 800 0 413 1    
; 9014-Fox
addEX 9014 14 1030 0 435 0
; 9196-paco
addEX 9196 196 1040 0 414 0
; 9092-swat team A
addEX 9200 3092 890 0 365 0
addEX 9201 3092 970 0 374 0
addEX 9202 3092 930 0 385 0
; 9526-president
;addEX 9526 5025 830 0 400 0 

MoveEX 9196 1170 384 0
wait_complete

talkEX 1000 16 28
wait_complete

MoveEX 9196 1070 408 0
wait_complete

MoveEX 9014 1140 434 0
wait_complete

MoveEX 9014 1140 384 0
wait_complete

talkEX 1000 29 34
wait_complete

MoveEX 9014 1100 414 0
wait_complete

MoveEX 9196 1170 378 0
wait_complete

playsound "sfx/lighthit.wav" 0 100 0
Doaction 9196 500
wait_complete

wait_time 1

playmusic "bgm/lfo_courage.ogg"

talkEX 1000 35 35
wait_complete

talkEX 1000 36 36
wait_complete

end