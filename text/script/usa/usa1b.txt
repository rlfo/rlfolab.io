; this file must be in UNICODE format
begin
background 0
playmusic "bgm/lfo_advent.ogg"

background_offsetx -160

camera_pos 150

; 9001 davis  9002 stinky 9003 cutie
; 9004 dennis 9005 woody  9112 kiki
; 9113 jace   9068 bochai
addEX 9001 1 420 0 405 1               
addEX 9005 5 490 0 400 0
addEX 9068 68 610 0 400 1
addEX 9113 194 510 0 445 1
addEX 9002 2 650 0 455 1
addEX 9003 3 560 0 408 1
addEX 9004 4 730 0 420 1
addEX 9112 197 690 0 400 1    

; 9087 9187 9287 indian
addEX 9087 3087 1930 0 520 1
addEX 9187 3087 1960 0 570 1
addEX 9287 3087 1980 0 550 1
      
talkEX 1000 32 39
wait_complete

talkEX 1000 40 40
MoveEX 9112 1000 450 0
MoveEX 9068 1030 470 0
wait_complete

talkEX 1000 41 41
RunEX 9112 1600 450 0
RunEX 9068 1630 470 0
wait_complete

talkEX 1000 42 45
wait_complete

Faceleft 9005
Faceleft 9113
Faceleft 9002
Faceleft 9003
Faceleft 9004

talkEX 1000 46 47
wait_complete

playmusic "bgm/lfo_delight.ogg"

talkEX 1000 48 50
wait_complete

MoveEX 9001 430 470 0
wait_complete

talkEX 1000 51 51
MoveEX 9001 1000 470 0
wait_complete

Faceright 9005
Faceright 9113
Faceright 9002
Faceright 9003
Faceright 9004


talkEX 1000 52 52
MoveEX 9001 1230 390 1
wait_complete

talkEX 1000 53 57
wait_complete

talkEX 1000 58 70
wait_complete

talkEX 1000 71 71
wait_complete

playmusic "bgm/lfo_rage.ogg"

camera_pan 1000

MoveEX 9087 1730 455 1
MoveEX 9187 1760 480 1
MoveEX 9287 1780 500 1
wait_complete

talkEX 1000 72 76
wait_complete

talkEX 1000 77 77
MoveEX 9087 1520 452 0
MoveEX 9187 1660 455 1
MoveEX 9287 1620 428 1
wait_complete

camera_pan 450

talkEX 1000 78 85
wait_complete

Faceright 9001

Faceleft 9087
Faceleft 9187
Faceleft 9287
Faceleft 9112
Faceleft 9068

camera_pan 1000

RunEX 9001 1390 435 0               
RunEX 9005 1050 460 0
RunEX 9113 1110 465 0
RunEX 9002 1250 455 0
RunEX 9003 1160 488 0
RunEX 9004 1330 460 0
wait_complete

talkEX 1000 86 87
wait_complete

RunEX 9112 2000 480 0
RunEX 9068 2000 500 0
RunEX 9087 2000 482 0
RunEX 9187 2000 488 0
RunEX 9287 2000 455 0
wait_complete

remove 9112
remove 9068
remove 9087
remove 9187
remove 9287

RunEX 9001 1690 435 0
RunEX 9005 1310 460 0
RunEX 9113 1410 465 0
RunEX 9002 1550 455 0
RunEX 9003 1460 488 0
RunEX 9004 1630 460 0
wait_complete

talkEX 1000 88 92
wait_complete

camera_pan 1150

RunEX 9005 1520 508 0
wait_complete

RunEX 9005 1720 438 1
wait_complete

Doaction 9005 500
wait_complete

playmusic "bgm/lfo_sorrow.ogg"

talkEX 1000 93 110
wait_complete

RunEX 9001 2190 495 0
RunEX 9005 2110 490 0
RunEX 9113 2110 495 0
RunEX 9002 2150 495 0
RunEX 9003 2160 498 0
RunEX 9004 2130 490 0
wait_complete

end