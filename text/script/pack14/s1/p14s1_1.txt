; this file must be in UNICODE format
begin


background 0

talkex 1000 0 0
wait_complete


background 21

playmusic "BGM/lfo_greek3.ogg"



addex 9900 8808 700 0 386 0
addex 9901 365 615 0 370 1
addex 9902 366 660 -25 385 1

doaction 9901 700
doaction 9902 700

addex 9903 367 1100 0 386 0
faceleft 9903

camera_pos 450
talkex 1000 1 1
wait_complete

camera_pan 750

addex 9950 830 1250 0 376 0
doaction 9950 2201

addex 9904 8806 1250 -200 377 0
faceleft 9904
doaction 9904 8250
wait_timeex 1300
doaction 9904 8260
wait_timeex 1000

doaction 9904 8170
talkex 1000 2 2
wait_complete
camera_follow 9900

doaction 9904 8160
talkex 1000 3 3
wait_complete



wait_timeex 150
fadeout 0 1
wait_timeex 200
background 0
playmusic "BGM/lfo_heart.ogg"
wait_complete

remove 9900
remove 9901
remove 9902
remove 9903
remove 9904

background 22

wait_timeex 150
fadein 1 1
wait_complete

camera_pos 330

addex 9905 7267 1300 0 400 0
addex 9906 7266 1400 0 380 0

moveEX 9905 900 400 1 
moveEX 9906 1000 380 1 

talkex 1000 4 5
wait_complete

talkex 1000 6 6
wait_complete

talkex 1000 7 25
wait_complete

playmusic "BGM/lfo_p5s6_2.ogg"
talkex 1000 26 41
wait_complete

faceright 9905
talkex 1000 42 43
wait_complete

faceleft 9905
talkex 1000 44 55
wait_complete

stopmusic

moveEX 9905 1100 400 1 
moveEX 9906 900 380 1 
wait_complete

addex 9950 847 900 -35 385 0
doaction 9950 74

addex 9970 7077 900 -25 385 0
doaction 9970 8720

fadein 1 1
wait_complete

addex 9950 847 900 -35 385 0
doaction 9950 74

addex 9970 7077 900 -25 385 0
doaction 9970 8720

fadein 1 1
wait_complete

addex 9950 847 900 -35 385 0
doaction 9950 74

addex 9970 7077 900 -25 385 0
doaction 9970 8720

fadeout 1 1

wait_complete






background 25
camera_pos 330

addex 9000 7267 1100 0 400 0
addex 9001 7266 900 0 380 0
faceleft 9000
faceleft 9001


playmusic "BGM/lfo_china1.ogg"
camera_shake_ex 550 400
doaction 9001 8550




talkex 1000 56 56
wait_complete

wait_timeex 1300

doaction 9001 8555
talkex 1000 57 59
wait_complete

talkex 1000 60 60
wait_complete
camera_pos 330
doaction 9001 0
wait_complete
talkex 1000 61 61
wait_complete


background 22
camera_pos 330


talkex 1000 62 64
wait_complete

camera_follow 9907
addex 9907 3197 1500 0 400 0
moveEX 9907 1250 400 1 


faceright 9905
faceright 9906
wait_timeex 1000

moveEX 9905 1000 400 0 
faceright 9905

talkex 1000 65 66
wait_complete

camera_pan 330
talkex 1000 67 67
wait_complete




addex 9908 5082 1200 0 340 0
addex 9909 5082 1180 0 360 0
addex 9910 5082 1160 0 380 0
addex 9911 5082 1140 0 400 0
addex 9912 5082 1160 0 420 0
addex 9913 5082 1180 0 440 0
addex 9914 5082 1200 0 460 0

doaction 9909 8100
doaction 9910 8200
doaction 9911 8300
doaction 9912 8400
doaction 9913 8500
doaction 9914 8600

camera_follow 9907
talkex 1000 68 68
wait_complete

faceleft 9905
faceleft 9906

camera_pan 330
talkex 1000 69 71
wait_complete

faceright 9906
talkex 1000 72 72
wait_complete

camera_follow 9907
faceright 9905
talkex 1000 73 73
wait_complete

moveEX 9905 1050 400 0 
moveEX 9906 1000 380 0 
wait_timeex 1000


stopmusic
talkex 1000 74 74
wait_complete



fadein 1 1
fadeout 1 1
doaction 9907 8100
wait_timeex 600
doaction 9907 8100
wait_timeex 600
fadein 1 1
fadeout 1 1
doaction 9907 8100
wait_timeex 600
doaction 9907 8100

camera_shake_ex 1250 300

fadein 1 1
fadeout 1 1
remove 9905
remove 9906

playmusic "BGM/lfo_china3.ogg"
addex 9950 848 1025 0 300 0
doaction 9950 940

doaction 9907 8100
wait_timeex 600


remove 9908
remove 9909
remove 9910
remove 9911
remove 9912
remove 9913
remove 9914

doaction 9907 8100

fadein 1 1
fadeout 1 1


addex 9915 7077 500 0 340 0
addex 9916 7077 600 0 400 0
addex 9917 7077 700 0 310 0
addex 9918 7077 800 0 360 0
addex 9919 7077 1200 0 200 0
addex 9920 7077 1300 0 400 0
addex 9921 7077 1400 0 320 0
addex 9922 7077 1500 0 450 0

faceleft 9919
faceleft 9920
faceleft 9921
faceleft 9922

doaction 9915 8600
doaction 9916 8600
doaction 9917 8600
doaction 9918 8600
doaction 9919 8600
doaction 9920 8600
doaction 9921 8600
doaction 9922 8600

wait_timeex 600
doaction 9907 8100

camera_follow 9923
addex 9923 8808 900 -400 400 0
doaction 9923 8250
faceright 9923

wait_timeex 4000
doaction 9923 0
talkex 1000 75 75
wait_complete

talkex 1000 76 76
wait_complete


talkex 1000 77 77
wait_complete

doaction 9923 8060
faceright 9923

talkex 1000 78 81
wait_complete



fadeout 0 1
background 0
playmusic "BGM/lfo_beast_2.ogg"
wait_timeex 2000
wait_complete


background 21
wait_timeex 150
fadein 1 1


addex 9900 8808 700 0 386 0
addex 9901 365 615 0 370 1
addex 9902 366 660 0 385 1
doaction 9901 700
doaction 9902 700
addex 9903 367 1380 0 409 1
addex 9904 8806 1520 0 398 0
faceleft 9904

doaction 9904 8170
faceleft 9903
camera_pos 250
talkex 1000 82 83

wait_complete


camera_pan 1000
talkex 1000 84 84
wait_complete


faceright 9903
doaction 9903 8150
talkex 1000 85 89
wait_complete


camera_pan 300
addex 9924 8800 300 0 420 0
runex 9924 780 400 0
moveEX 9903 1000 409 1 
talkex 1000 90 95
wait_complete

camera_pan 450
doaction 9903 8170
wait_timeex 400
doaction 9924 520

playmusic "BGM/lfo_china1.ogg"

talkex 1000 96 98
wait_complete


doaction 9903 400
wait_timeex 200
remove 9924
remove 9903
addex 9927 367 1100 0 409 0
addex 9925 8800 1150 0 410 0
addex 9926 848 1060 0 370 0
faceleft 9925
faceleft 9927
doaction 9925 8120
doaction 9926 860
doaction 9927 8150






playmusic "BGM/lfo_p7s3_1.ogg"
talkex 1000 99 99
wait_complete
addex 9940 836 1125 0 411 0
doaction 9940 1610
wait_timeex 1000


talkex 1000 100 102
wait_complete



fadein 0 1
fadeout 1 0
wait_complete

fadein 0 1
fadeout 1 0
wait_complete



addex 9950 836 1050 0 300 0
doaction 9950 1751
wait_timeex 500

addex 9951 836 1250 0 440 0
doaction 9951 1751
wait_timeex 500

addex 9952 836 1450 0 250 0
doaction 9952 1751
wait_complete







background 2
camera_pos 450
wait_timeex 150
fadein 1 1
fadeout 1 1


addex 9900 8808 700 0 386 0
addex 9901 365 615 0 370 1
addex 9902 366 660 0 385 1
doaction 9901 700
doaction 9902 700

addex 9925 8800 1150 0 410 0
faceleft 9925
doaction 9925 8120

addex 9952 836 1130 0 410 0
doaction 9952 1610

addex 9927 367 1100 0 409 0
faceleft 9927
doaction 9927 8150

wait_complete

remove 9927
addex 9960 829 1100 0 409 0
doaction 9960 190

addex 9961 850 1100 0 409 0
faceleft 9961
doaction 9961 190


wait_timeex 900
remove 9961
remove 836
wait_timeex 500
doaction 9925 0
remove 9952

camera_pan 600
wait_timeex 900

addex 9970 850 1000 0 410 0
addex 9943 829 1000 0 410 0
doaction 9970 190
doaction 9943 190
wait_timeex 500

addex 9971 850 1250 0 380 0
addex 9944 829 1250 0 380 0
faceleft 9971
doaction 9971 190
doaction 9944 190
wait_timeex 500

addex 9945 829 1250 0 450 0
addex 9972 850 1250 0 450 0
faceleft 9972
doaction 9972 190
doaction 9945 190
wait_timeex 500

wait_timeex 500
remove 9970
addex 9940 7193 1000 0 410 0

wait_timeex 500
remove 9971
addex 9941 7277 1250 0 380 0
faceleft 9941

wait_timeex 500
remove 9972
addex 9942 7273 1250 0 450 0
faceleft 9942


talkex 1000 103 103
wait_complete






end