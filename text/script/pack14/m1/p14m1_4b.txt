; this file must be in UNICODE format
begin

background 5
playmusic "BGM/lfo_devil3.ogg"
camera_pos 300

addex 9001 267 700 0 435 1  
addex 9002 195 650 0 450 1
wait_complete
addex 9003 17  1000 0 440 0
faceleft 9003
addex 9010 3008 420 0 350 0
addex 9011 3008 470 0 440 0
addex 9012 3008 455 0 625 0
addex 9013 3008 300 0 450 0
addex 9014 3008 440 0 430 0
addex 9020 3008 370 0 320 0
addex 9021 3008 350 0 425 0
addex 9022 3008 405 0 455 0
addex 9023 3008 500 0 650 0
addex 9024 3008 550 0 410 0
addex 9015 3051 1025 0 555 0
addex 9016 3051 935 0 505 0
addex 9017 3051 925 0 405 0
addex 9018 3051 905 0 455 0
addex 9019 3051 1030 0 415 0
faceleft 9015
faceleft 9016
faceleft 9017
faceleft 9018
faceleft 9019
wait_complete
runex 9001 500 450 1
wait_complete
doaction 9001 500
wait_complete
faceleft 9002
talkex 1000 56 56
wait_complete
runex 9001 200 435 1
runex 9002 200 450 1
wait_timeex 1000
camera_follow 9003
talkex 1000 57 57
wait_complete
fadeout 0 1
wait_timeex 500

background 57
playmusic "BGM/lfo_devil1.ogg"
camera_pos 1200

addex 9001 267 1150 0 430 0  
addex 9002 195 1150 0 450 0
wait_complete
runex 9001 1730 430 1
runex 9002 1670 450 0
faceleft 9003
wait_complete
talkex 1000 58 58
wait_complete
addex 9020 3008 400 0 300 0
addex 9021 3008 440 0 350 0
addex 9003 17   350 0 400 0
addex 9023 3008 350 0 450 0
addex 9024 3008 440 0 500 0
addex 9025 3008 500 0 400 0
wait_complete    
playmusic "BGM/lfo_devil2.ogg"
camera_pan 300
runex 9020 1200 300 1
runex 9021 1240 350 1
runex 9003 1250 400 1
runex 9023 1250 450 1
runex 9024 1240 500 1
runex 9025 1240 400 1
wait_timeex 1100

camera_follow 9001

talkex 1000 59 61  
wait_complete      
fadeout 0 1
wait_timeex 700
end