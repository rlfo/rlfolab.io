; this file must be in UNICODE format
begin

background 30
playmusic "BGM/lfo_devil1.ogg"
camera_pos 300

addex 9001 267 700 0 430 0  
wait_complete
faceleft 9001
wait_timeex 1000
faceright 9001
wait_timeex 1000
faceleft 9001
talkex 1000 39 40  
wait_complete
moveex 9001 1700 430 0
wait_timeex 1000
fadeout 0 1        
wait_timeex 700

end