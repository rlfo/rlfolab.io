; this file must be in UNICODE format
begin

background 18
playmusic "BGM/lfo_devil6.ogg"

wait_complete
camera_pos 500

addex 9001 267 800 0 400 0  
addex 9004 18  1150 0 400 1 

addex 9002 195 200 0 300 0 
addex 9005 5037 100 0 300 0
 
addex 9003 17  1600 0 300 0 
addex 9006 5015 1700 0 300 0  

faceleft 9004
faceleft 9003
faceleft 9006

talkex 1000 159 160
wait_complete

doaction 9004 8720
doaction 9001 8500
wait_timeex 1400

doaction 9001 8100
wait_complete

remove 9004
wait_complete

addex 9004 18  600 0 400 1 

camera_follow 9004
talkex 1000 161 161
wait_complete
talkex 1000 162 162
wait_complete
runex 9001 1050 400 1
runex 9004 750 400 0
wait_complete


doaction 9001 8100
doaction 9004 8000
wait_timeex 500
talkex 1000 163 164

camera_shake_ex 430 100
doaction 9001 10000
doaction 9004 10000

wait_timeex 2500

fadein 0 1
fadeout 0 1

background 0
stopmusic
wait_complete
fadein 0 1
fadeout 0 1
talkex 1000 165 165
wait_complete
fadein 0 1
fadeout 0 1
talkex 1000 166 166
wait_complete
fadein 0 1
fadeout 0 1
talkex 1000 167 167
wait_complete
fadein 0 1
fadeout 0 1
talkex 1000 168 168
wait_complete

background 10
playmusic "BGM/lfo_china3.ogg"
camera_shake_ex 400 50
talkex 1000 169 169
addex 9010 849 800 0 400 0 
doaction 9010 1050
wait_complete
fadein 1 1
fadeout 1 1
camera_shake_ex 600 50
talkex 1000 170 170
wait_complete

fadein 1 1
fadeout 1 1
camera_shake_ex 400 50
talkex 1000 171 171
wait_complete

fadein 1 1
fadeout 1 1
camera_shake_ex 600 50
talkex 1000 172 172
wait_complete

fadein 1 1
fadeout 1 1
camera_shake_ex 500 50
talkex 1000 173 173
wait_complete
fadein 1 1
fadeout 1 1
talkex 1000 174 174
wait_complete

remove 9010
fadein 1 2
fadeout 1 2





stopmusic
showpicture "special/p14m2cg.png"
wait_timeex 2000
talkex 1000 175 177
wait_complete
hidepicture

fadein 1 2
fadeout 1 2

background 18
playmusic "BGM/lfo_p5s3_4.ogg"
camera_shake_ex 100 50


addex 9001 267 200 0 350 0  
addex 9004 18  1150 0 400 1 

addex 9002 195 200 0 300 0 
addex 9005 5037 100 0 300 0

faceleft 9004

addex 9010 849 800 0 400 0 
doaction 9010 1050

doaction 9001 10400
talkex 1000 178 179
wait_complete

camera_shake_ex 750 50
fadein 1 1
fadeout 1 1
talkex 1000 180 182
wait_complete


camera_shake_ex 100 50
wait_timeex 1000
camera_follow 9001
remove 9001
remove 9010
fadein 1 1
fadeout 1 1
playmusic "BGM/lfo_p8s3_1.ogg"
addex 9001 267 200 0 350 0 
doaction 9001 700

wait_complete


talkex 1000 183 184
runex 9002 300 350 1
runex 9005 100 350 0
wait_complete


runex 9004 350 400 1
wait_complete

addex 9003 17  800 0 450 0 
addex 9006 5015 800 0 350 0  

talkex 1000 185 186
moveex 9003 480 450 1
moveex 9006 480 350 1
wait_complete


talkex 1000 187 187
wait_complete

faceright 9004
talkex 1000 188 189
wait_complete


fadein 0 1
fadeout 0 1

background 0
talkex 1000 190 192
wait_complete

background 17
playmusic "BGM/lfo_theme2.ogg"
talkex 1000 193 198
camera_follow 9004
addex 9004 18  1005 0 240 1 

addex 9003 17  1110 0 290 0 
doaction 9004 0
faceleft 9003
wait_complete

fadein 0 2
fadeout 0 2

background 0
fadein 0 1
fadeout 0 1
talkex 1000 199 199
wait_complete

fadein 0 1
fadeout 0 1
talkex 1000 200 200
wait_complete

fadein 0 1
fadeout 0 1
talkex 1000 201 201
wait_complete



fadeout 0 1
end