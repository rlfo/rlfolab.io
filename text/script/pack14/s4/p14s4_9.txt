; this file must be in UNICODE format
begin

background 21
playmusic "BGM/lfo_p7s3_1.ogg"

camera_pos 1000
addex 9902 3044 1300 0 400 0
faceleft 9902


addex 9901 7002 1500 0 400 0
faceleft 9901
wait_complete
runex 9901 1400 401 1
wait_timeex 500
doaction 9901 500
playsound "SFX/freeze_ice-hit.wav" 0 100 1
wait_timeex 500
doaction 9901 500
playsound "SFX/freeze_ice-hit.wav" 0 100 1
wait_timeex 500
doaction 9901 500
playsound "SFX/freeze_ice-hit.wav" 0 100 1
wait_timeex 500
doaction 9902 733
playsound "SFX/freeze_ice-break.wav" 0 100 1
wait_timeex 1000


talkex 1000 15 15
wait_complete


wait_timeex 500
fadeout 0 1
wait_timeex 200
background 0
wait_complete


end