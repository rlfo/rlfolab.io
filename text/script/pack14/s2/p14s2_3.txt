; this file must be in UNICODE format
begin


background 6
camera_pos 700
playmusic "BGM\lfo_devil1.ogg"
addex 9900 268 1100 0 411 0
addex 9901 278 850 0 411 0

addex 9902 86 1250 0 411 1
faceleft 9902

doaction 9900 8370
wait_complete

talkex 1000 80 80
wait_complete

doaction 9901 8700
talkex 1000 81 81
wait_complete

moveex 9900 1100 411 0
runex 9901 1150 411 0
talkex 1000 82 87
wait_complete

moveex 9900 1450 411 0
moveex 9901 1400 411 0

doaction 9902 0
wait_complete

faceleft 9901
faceright 9902

talkex 1000 88 88
wait_complete


moveex 9900 1600 411 0
moveex 9901 1600 411 0
wait_complete

talkex 1000 89 89
wait_complete
end