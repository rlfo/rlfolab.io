; this file must be in UNICODE format
begin

background 12
camera_pos 150

playmusic "BGM\lfo_china1.ogg"

addex 9900 268 500 0 411 0
addex 9901 278 300 0 380 0

addex 9903 3270 600 0 411 1
addex 9902 2 800 0 380 0
doaction 9902 910
faceleft 9902
faceleft 9903

doaction 9900 8300
wait_complete
wait_timeex 500
talkex 1000 104 104
wait_complete


talkex 1000 105 105
wait_complete


runex 9901 600 380 0
talkex 1000 106 106
wait_complete

camera_pan 400
runex 9903 850 385 1
wait_timeex 500
wait_complete
talkex 1000 107 110
wait_complete





addex 9904 362 1400 0 380 0
moveex 9904 1070 380 1
faceright 9903
playmusic "BGM\lfo_p5s6_2.ogg"
talkex 1000 111 111
wait_complete

talkex 1000 112 120
wait_complete


doaction 9904 9060
wait_timeex 1000
remove 9903
playmusic "BGM\lfo_beast_2.ogg"
addex 9905 3286 850 0 385 1

faceleft 9905
doaction 9904 0


talkex 1000 121 124
wait_complete
end