; this file must be in UNICODE format
begin
background 50

playmusic "bgm/lfo_icefire3.ogg"

background_offsetx -200

camera_pos 600

addex 9001 12 1020 0 340 1

faceleft 9001
talkex 1000 44 45
wait_complete

doaction 9001 210
wait_complete

talkex 1000 46 46
doaction 9001 8550

playsound "sfx/freeze_ice-break.wav" 0 100 1

fadein 1 2

;;;;;;;;;;;;;;;;;;;;
background 23

background_offsetx -200

camera_pos 600

addex 9001 12 1020 0 340 1

faceleft 9001

doaction 9001 910
wait_complete

wait_time 1

doaction 9001 0
wait_complete

;giveitem 9001 180

talkex 1000 47 48
wait_complete

fadein 0 1

;;;;;;;;;;;;;;;;;;;;
background 44

playmusic "bgm/lfo_courage.ogg"

background_offsetx -270

camera_pos 1000

addex 9001 11 1220 0 345 1
addex 9003 199 1805 0 340 0

faceleft 9001
faceleft 9003

moveex 9003 1505 360 1
wait_complete

faceright 9001

talkex 1000 49 52
wait_complete

doaction 9003 8100
wait_complete

talkex 1000 53 53
wait_complete

runex 9003 1305 340 1
wait_complete

doaction 9001 210
wait_complete

doaction 9003 1500
wait_complete

doaction 9001 0
wait_complete

talkex 1000 54 57
wait_complete

;doaction 9003 420
wait_complete

camera_pan 1100

;doaction 9003 8700
doaction 9003 1550
doaction 9001 8000
wait_complete

AddEX 9901 817 1480 -50 363 0
Doaction 9901 201 

Doaction 9003 740
wait_complete

doaction 9003 420
wait_complete

talkex 1000 58 59
wait_complete

doaction 9003 470
wait_complete

doaction 9003 8700
doaction 9001 8000
wait_complete

runex 9003 1400 355 0
runex 9001 1555 353 1
wait_complete

talkex 1000 60 63
wait_complete

end

