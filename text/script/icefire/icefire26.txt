; this file must be in UNICODE format
begin
background 26

;background_offsetx -1000

playmusic "bgm/lfo_normal.ogg"

;============== firen starts to train heros ==================
camera_pos 1100

add 9004 3333 1370 0 350
add 9011 11 1750 0 340
add 9012 12 1700 0 370
add 9037 3037 1600 0 280
DoAction 9037 0

faceleft 9011
faceleft 9012
                                  
move 9012 1560 295
wait_complete

add 9137 3037 1600 0 280
DoAction 9137 10
faceleft 9002
faceleft 9003
faceleft 9004
wait_complete

;============== firen free those dongs ==================
add 9068 68 1650 0 250
add 9168 68 1660 0 260
add 9268 68 1570 0 265
add 9368 68 1580 0 275
add 9468 68 1590 0 280
add 9568 68 1400 0 296
add 9668 68 1410 0 300
move 9068 1250 330
move 9168 1300 350
move 9268 1350 400
;move 9368 1250 410
moveEX 9468 1440 340 0
moveEX 9568 1500 370 0
moveEX 9668 1470 390 0
wait_complete

move 9068 1050 300
move 9168 1050 320
move 9268 1050 350
move 9368 1050 370
move 9468 1050 400
move 9568 1050 440
move 9668 1050 440
faceright 9002
faceright 9003
faceright 9004

talk 752
wait_complete
end


