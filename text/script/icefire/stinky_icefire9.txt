; this file must be in UNICODE format
begin
background 19

background_offsetx -200
;background_offsetx -180

playmusic "bgm/lfo_courage.ogg"

;============== arrive the monsters' cave ==================
camera_pos 350
add 9002 2 1050 0 350

add 9001 1 90 0 320

run 9001 990 330
wait_complete

add 9005 5 400 0 330
add 9004 4 350 0 310
add 9003 3 100 0 370

talkEX 684 0 1
run 9004 950 380
run 9005 910 350
run 9003 910 400
wait_complete
   
add 9301 3031 1400 0 340
add 9302 3031 1430 0 360
add 9303 3031 1480 0 370
add 9304 3031 1500 0 340
add 9305 3031 1550 0 390
                                                 
faceleft 9301
faceleft 9302
faceleft 9303
faceleft 9304
faceleft 9305

camera_pan 840
talkEX 684 2 5
wait_complete

run 9301 1170 340
run 9302 1230 350
run 9303 1320 360
run 9304 1270 370
run 9305 1220 380
wait_complete
fadeout 0 1
end


