; this file must be in UNICODE format
begin
background 26

background_offsetx 2000                                                 
playmusic "bgm/lfo_advent.ogg"

;============== heros tries to communciate with earth ==================
camera_pos 850

add 9011 12 1600 0 250
add 9004 1 1820 0 250
add 9003 5 1870 0 300

moveEX 9011 1000 330 0
move 9004 1220 330
move 9003 1270 380
wait_complete

talk 729
wait_complete

move 9011 700 430 
move 9004 700 430
move 9003 700 480
wait_complete

fadein 0 5
stopmusic
background 5
background_offsetx 100        
camera_pos 1000
wait_complete

playmusic "bgm/lfo_sorrow.ogg"
                 
add 9011 12 800 0 250
add 9004 1 620 0 250
add 9003 5 570 0 300
camera_follow 9011


moveEX 9011 1600 250 1
move 9004 1420 250
move 9003 1370 300
wait_complete

talkEX 714 0 12
wait_complete
camera_pan 1000
move 9004 1210 280
move 9003 1200 350
wait_complete

talkEX 714 13 18
wait_complete

;============== trained in a month ==================
fadein 0 3 
stopmusic
remove 9004
remove 9002
remove 9003
remove 9011
wait_complete

talkEX 714 22 22
wait_complete

fadein 0 3
add 9004 1 1420 0 250
add 9003 5 1370 0 300
add 9011 12 1600 0 250
faceleft 9011
playmusic "bgm/lfo_normal.ogg"
wait_complete

talkEX 714 19 21
wait_complete

move 9011 1300 350
wait_complete

move 9011 920 350
move 9003 920 350
move 9004 920 350
wait_complete
end


