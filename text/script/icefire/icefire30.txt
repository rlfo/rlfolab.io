; this file must be in UNICODE format
begin
background 30

;background_offsetx -1000

playmusic "bgm/lfo_sorrow.ogg"

;============== heros found the rock ==================

camera_pos 900
add 9004 3333 1400 0 400

moveEX 9004 1180 350 0
wait_complete

add 9501 205 1180 0 350
changeholder 9501 9004

talk 762 
wait_complete

move 9004 1800 360 
wait_complete



background 29
background_offsetx -100

;============== heros enters the rocks cave ==================
camera_pos 150
add 9033 3333 150 0 380 

add 9011 11 700 0 350 
add 9012 12 650 0 370
faceleft 9011
faceleft 9012

add 9501 205 350 0 350
changeholder 9501 9033

;move 9033 500 380
move 9033 600 370
wait_complete
changeholder 9501 9012
wait_complete

moveEX 9033 500 380 0
wait_complete


talk 755
wait_complete

move 9011 1000 350
move 9012 1000 370
move 9033 1000 380
wait_complete

end


