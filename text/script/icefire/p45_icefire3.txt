; this file must be in UNICODE format
begin
background 23

playmusic "bgm/lfo_courage.ogg"

background_offsetx -200

camera_pos 600

addex 9001 12 1000 0 335 1
addex 9003 199 1080 0 330 0

Faceleft 9002
Faceleft 9003

doaction 9001 8000
doaction 9003 430
wait_complete

Faceleft 9001

talkex 1000 37 37
wait_complete

doaction 9001 8200
wait_complete

faceright 9003
doaction 9003 210
wait_complete

doaction 9001 1500
wait_complete

AddEX 9901 817 850 -50 333 0
Doaction 9901 201 

talkex 1000 38 39
wait_complete

talkex 1000 40 42
wait_complete

doaction 9003 8100
wait_complete

doaction 9003 1500
wait_complete

doaction 9001 210
wait_complete

fadeout 1 1
wait_time 1

remove 9001
remove 9002
remove 9003
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

background 50

background_offsetx -200

camera_pos 600

addex 9001 12 1020 0 335 1
addex 9003 199 835 0 330 0

faceleft 9002
faceleft 9001

doaction 9001 210
wait_complete

talkex 1000 43 43
wait_complete

runex 9003 550 330 1
wait_complete

end
