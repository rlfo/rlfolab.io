; this file must be in UNICODE format
begin
background 26

background_offsetx -500

playmusic "bgm/lfo_humor.ogg"

;============== Beaten Ninja  ==================
camera_pos 1500
;camera_follow 9004
add 9004 4 2200 0 510
add 9003 3 2300 0 530
add 9002 2 2250 0 550
add 9319 3019 2490 0 530
faceleft 9319
wait_complete

run 9004 1400 510
run 9003 1400 530 
run 9002 1400 550  
run 9319 1300 530 
wait_complete

character_pos 9004 1400 450
character_pos 9003 1300 460 
character_pos 9002 1350 470  
character_pos 9319 1100 460 
wait_complete

run 9004 2400 450
run 9003 2400 460 
run 9002 2400 470  
run 9319 2490 460 
talkEX 704 0 0
wait_complete

character_pos 9004 2200 400
character_pos 9003 2300 410 
character_pos 9002 2250 420 
character_pos 9319 2490 410 
wait_complete

run 9004 1300 400
run 9003 1300 410 
run 9002 1300 420  
run 9319 1300 410 
talkEX 704 1 1
wait_complete

character_pos 9004 1200 350
character_pos 9003 1100 360 
character_pos 9002 1150 370  
character_pos 9319 900 360 
wait_complete

run 9004 2400 350
run 9003 2400 360 
run 9002 2400 370  
run 9319 2490 360 
talkEX 704 2 2
wait_complete

character_pos 9004 2300 330
character_pos 9003 2400 340 
character_pos 9002 2350 350 
character_pos 9319 1400 340 
wait_complete

run 9004 1950 330 
run 9003 2100 340 
run 9002 2000 350  
run 9319 1750 340 
wait_complete

run 9319 1900 340 
wait_complete
DoAction 9002 210
DoAction 9003 100
DoAction 9004 470
DoAction 9319 500
wait_complete

run 9002 1950 350
run 9004 2050 330
faceright 9003
faceleft 9319 
DoAction 9319 500
DoAction 9003 210
wait_complete

faceright 9319
DoAction 9002 210 	
DoAction 9004 210 	
DoAction 9319 500
wait_complete
DoAction 9319 500
wait_complete

Playsound "SFX/johnnyconfirm.wav" 0 100 0
DoAction 9002 0
DoAction 9003 0
DoAction 9004 0
faceleft 9002
faceleft 9004
faceright 9003
add 9037 3037 1920 0 330
DoAction 9037 30
remove 9319
add 9011 11 1920 0 330
wait_complete

playmusic "bgm/lfo_heart.ogg"
talkEX 704 3 9 
wait_complete

faceleft 9011
talkEX 704 10 10 
wait_complete

faceright 9011
talkEX 704 11 16 
wait_complete

faceleft 9011
talkEX 704 17 18 
wait_complete

move 9011 1750 330
wait_complete
move 9004 1400 330 
move 9003 1400 340 
move 9002 1400 350
move 9011 1400 330
wait_complete
end


