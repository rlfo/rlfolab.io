; this file must be in UNICODE format
begin
background 11
playmusic "bgm/lfo_heart.ogg"

;============== going back to right size ==================
background_offsetx 1000


add 9002 4 450 0 400
add 9001 5 550 0 500


camera_pos 50

moveEX 9002 300 340 0
moveEX 9001 400 400 1
wait_complete

talk 753
wait_complete

move 9002 450 400
move 9001 520 470
wait_complete
remove 9001

move 9002 520 470
wait_complete

end


