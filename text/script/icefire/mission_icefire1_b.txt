; this file must be in UNICODE format
begin
background 3

playmusic "bgm/lfo_humor.ogg"
; victor = 9051  yourself = 9000

camera_pos 350

add 9000 3333 750 0 430

add 9010 3039 430 0 445
add 9011 3039 480 0 415
add 9012 3039 1020 0 465
add 9013 3039 1060 0 405

DoAction 9010 733
DoAction 9011 733
DoAction 9012 733
DoAction 9013 733

faceleft 9001

faceleft 9012
faceleft 9013

faceleft 9020

add 9020 11 1200 0 430
move 9020 1000 430
wait_complete

faceright 9001
talkEX 1000 29 29
wait_complete

move 9020 870 430
wait_complete

Fadeout 0 1
end