; this file must be in UNICODE format
begin
background 42 

;background_offsetx -1500
background_offsetx 250

playmusic "bgm/lfo_advent.ogg"

;============== heros arrive the cave entrance ==================
camera_pos -450
add 9012 12 300 0 250
add 9005 5  650 0 230
add 9001 1  600 0 300

moveEX 9012 10 250 0
move 9005   250 230 
move 9001   200 300 

wait_complete
talk 722 
wait_complete
move 9001 0 330 
wait_complete
faceleft 9012
move 9001 -200 330 
wait_complete

end


