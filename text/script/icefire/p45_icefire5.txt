; this file must be in UNICODE format
begin
background 44

playmusic "bgm/lfo_rage.ogg"

background_offsetx -270

camera_pos 1000

addex 9001 11 1520 0 342 1
addex 9003 199 1385 0 340 0

addex 9004 13 1885 0 340 1

faceleft 9004

faceleft 9001

doaction 9003 510
wait_complete

doaction 9003 8000
wait_complete

talkex 1000 64 68
wait_complete

Runex 9003 1685 340 0

doaction 9004 8000
wait_complete

runex 9004 1650 341 1
wait_complete

Doaction 9003 210
wait_complete

wait_time 1

runex 9004 1700 341 1
wait_complete

camera_pan 800

AddEX 9901 817 1250 -50 363 0
Doaction 9901 201 

remove 9001
remove 9004

wait_time 1

Doaction 9003 740
wait_complete

runex 9003 1550 341 0
wait_complete

camera_pan 1100

talkex 1000 69 70
wait_complete

fadeout 0 2
wait_time 2


end
