; this file must be in UNICODE format
begin

background 10
playmusic "BGM/lfo_p6s3_1.ogg"

camera_pos 300
wait_timeex 100
camera_shake_ex 400 100

addex 9002 5069 1138 0 290 0
addex 9003 3197 1190 0 360 0
addex 9004 7004 1300 0 362 0
addex 9005 7003 1320 0 310 0
addex 9006 5078 1470 0 370 0
addex 9101 284 1410 0 350 0
addex 9102 7269 1520 0 340 0
addex 9103 7266 1360 0 360 0
addex 9200 8800 900 0 360 0
doaction 9101 8500
doaction 9103 9110
doaction 9200 8040
playsound "sfx/julian_sp3.wav" 0 100 0
faceleft 9001
faceleft 9002
faceleft 9003
faceleft 9004
faceleft 9005
faceleft 9006
faceleft 9101
faceleft 9102
faceleft 9103
faceleft 9200
addex 9301 291 270 0 345 1
addex 9302 290 590 0 360 1
addex 9303 293 540 0 365 1
addex 9304 294 650 0 350 1
doaction 9301 8400
wait_complete

wait_time 1

doaction 9200 8000
wait_complete

wait_timeex 500

move 9302 205 360
move 9303 160 365
move 9304 119 350
wait_complete

camera_shake_ex 100 100
doaction 9200 100
wait_complete

background 11

addex 9002 5069 1138 0 290 0
addex 9003 3197 1190 0 360 0
addex 9004 7004 1300 0 362 0
addex 9005 7003 1320 0 310 0
addex 9006 5078 1470 0 370 0
addex 9101 284 1410 0 350 0
addex 9102 7269 1520 0 340 0
addex 9103 7266 1360 0 360 0
addex 9200 8800 528 0 360 0
doaction 9101 8500
doaction 9103 9110
faceleft 9001
faceleft 9002
faceleft 9003
faceleft 9004
faceleft 9005
faceleft 9006
faceleft 9101
faceleft 9102
faceleft 9103
faceleft 9200
addex 9301 291 270 0 345 1
addex 9302 290 205 0 360 1
addex 9303 293 160 0 365 1
addex 9304 294 118 0 350 1
doaction 9301 8400
wait_complete

wait_timeex 500

playsound "sfx/Bomb.wav" 0 100 0
addex 9901 800 205 -100 361 1
doaction 9901 730
wait_timeex 200
playsound "sfx/Bomb.wav" 0 100 0
addex 9902 800 205 -100 361 1
doaction 9902 730
wait_timeex 100
playsound "sfx/Bomb.wav" 0 100 0
addex 9903 800 205 -100 361 1
doaction 9903 730
wait_timeex 100
playsound "sfx/Bomb.wav" 0 100 0
addex 9904 800 205 -100 361 1
doaction 9904 730
wait_timeex 200
doaction 9302 8410
playsound "sfx/Bomb.wav" 0 100 0
addex 9905 800 160 -100 366 1
doaction 9905 730
wait_timeex 200
playsound "sfx/Bomb.wav" 0 100 0
addex 9906 800 160 -100 366 1
doaction 9906 730
wait_timeex 100
playsound "sfx/Bomb.wav" 0 100 0
addex 9907 800 160 -100 366 1
doaction 9907 730
wait_timeex 100
playsound "sfx/Bomb.wav" 0 100 0
addex 9908 800 160 -100 366 1
doaction 9908 730
wait_timeex 200
doaction 9303 8350
doaction 9304 8320

playsound "sfx/Bomb.wav" 0 100 0
addex 9909 800 118 -100 351 1
doaction 9909 730
wait_timeex 100

playsound "sfx/Bomb.wav" 0 100 0
addex 9910 800 180 -100 350 1
doaction 9910 730
wait_timeex 200
playsound "sfx/Bomb.wav" 0 100 0
addex 9911 800 180 -100 350 1
doaction 9911 730
wait_timeex 100
playsound "sfx/Bomb.wav" 0 100 0
addex 9912 800 180 -100 350 1
doaction 9912 730
wait_timeex 100
playsound "sfx/Bomb.wav" 0 100 0
addex 9913 800 180 -100 350 1
doaction 9913 730
wait_complete

wait_timeex 500

addex 9911 836 225 500 361 0
addex 9912 836 180 500 366 0
addex 9913 836 128 500 351 0
doaction 9911 1610
doaction 9912 1610
doaction 9913 1610
wait_complete

wait_time 1
addex 9914 836 300 500 346 0
doaction 9914 1610
wait_complete

wait_timeex 500
camera_shake_ex 950 100
wait_time 1

talkex 1000 143 143
wait_complete

wait_timeex 500
camera_shake_ex 100 100
wait_time 1

talkex 1000 144 145
wait_complete

wait_timeex 500
camera_shake_ex 950 100
wait_time 1

talkex 1000 146 146
wait_complete

wait_timeex 500
camera_shake_ex 100 100
wait_time 1

talkex 1000 147 147
wait_complete

wait_time 3

remove 9911
remove 9912
remove 9913
remove 9914
wait_complete

wait_time 1

doaction 9301 0
doaction 9302 0
doaction 9303 0
doaction 9304 0
wait_complete

addex 9801 819 270 0 344 1
doaction 9801 1020
wait_complete

wait_time 1

addex 9802 819 205 0 359 1
addex 9803 819 160 0 364 1
addex 9804 819 118 0 349 1
doaction 9802 1020
doaction 9803 1020
doaction 9804 1020
wait_complete


wait_time 1

talkex 1000 148 149
wait_complete

wait_timeex 500
camera_shake_ex 950 100
wait_time 1

talkex 1000 150 153
wait_complete

wait_timeex 500
camera_shake_ex 100 100
wait_time 1

talkex 1000 154 156
wait_complete

wait_timeex 500

addex 9811 819 270 0 344 1
addex 9812 819 205 0 359 1
addex 9813 819 160 0 364 1
addex 9814 819 118 0 349 1
doaction 9811 1020
doaction 9812 1020
doaction 9813 1020
doaction 9814 1020
wait_complete

wait_time 1

talkex 1000 157 157
wait_complete

wait_time 1

doaction 9200 8040
playsound "sfx/firzen2.wav" 0 100 0
wait_timeex 300
camera_shake_ex 100 100
playsound "sfx/julian_sp3.wav" 0 100 0
wait_complete

wait_time 1

talkex 1000 158 160
wait_complete

doaction 9302 8420
doaction 9303 8360
doaction 9304 8390
addex 9821 819 270 0 344 1
addex 9822 819 205 0 359 1
addex 9823 819 160 0 364 1
addex 9824 819 118 0 349 1
doaction 9821 1020
doaction 9822 1020
doaction 9823 1020
doaction 9824 1020
wait_complete

wait_time 1

doaction 9301 10000
doaction 9200 8000
wait_timeex 500
fadeout 1 3
wait_time 3

camera_pos 900

talkex 1000 161 163
move 9301 370 345
move 9302 305 360
move 9303 260 365
move 9304 218 350
move 9200 538 360
wait_complete

faceleft 9200
doaction 9200 8110
wait_complete

camera_pan 0

addex 9950 836 540 0 361 0
addex 9951 836 560 0 361 0
doaction 9950 1610
doaction 9951 1610
wait_complete

wait_timeex 1500

playmusic "BGM/lfo_p6s2_2.ogg"
remove 9200
playsound "sfx/freeze_sp3-2.wav" 0 100 0
addex 9201 7001 523 0 360 0
addex 9202 7005 603 0 360 0
faceleft 9201
faceleft 9202
doaction 9201 9000
doaction 9202 9010
remove 9950
remove 9951
wait_complete

talkex 1000 164 165
wait_complete

camera_pan 900
wait_time 1

talkex 1000 166 166
wait_complete

camera_pan 0
wait_time 1

talkex 1000 167 169
wait_complete

camera_pan 900
wait_time 1

talkex 1000 170 170
wait_complete

camera_pan 0
wait_time 1

run 9301 460 360
run 9303 380 365
doaction 9302 520
wait_complete

faceleft 9302
doaction 9301 500
doaction 9302 500
wait_timeex 500
fadein 1 1
remove 9201
remove 9202
wait_timeex 200
addex 9921 836 523 500 361 0
addex 9922 836 603 500 361 0
doaction 9921 1610
doaction 9922 1610
wait_complete

wait_timeex 500

stopmusic

camera_shake_ex 100 100
addex 9701 819 622 0 359 0
addex 9702 819 460 0 359 0
addex 9703 819 380 0 359 0
playsound "sfx/thunder_sp2-2.wav" 0 100 0
doaction 9301 8410
doaction 9302 8410
doaction 9303 8350
doaction 9701 11000
doaction 9702 11000
doaction 9703 11000
wait_complete

remove 9921
remove 9922
wait_complete

talkex 1000 171 172
wait_complete

fadein 1 1
playmusic "BGM/lfo_omega.ogg"
addex 9201 8800 170 0 351 0
doaction 9201 8120
playsound "sfx/pick.wav" 0 100 0
doaction 9304 8320
wait_complete

faceleft 9301
talkex 1000 173 173
wait_complete

wait_timeex 500

talkex 1000 174 180
wait_complete

wait_timeex 500
camera_shake_ex 950 100
wait_timeex 500

talkex 1000 181 183
wait_complete

camera_shake_ex 100 100
talkex 1000 184 185
wait_complete

camera_shake_ex 950 100
talkex 1000 186 187
wait_complete

camera_shake_ex 100 100
talkex 1000 188 193
wait_complete

playsound "sfx/Bark.wav" 0 100 0
wait_timeex 500
changeteam 9201 1
playsound "sfx/Bomb.wav" 0 100 0
addex 9905 800 218 -100 350 1
doaction 9905 730
wait_complete

fadein 1 2
doaction 9201 0
doaction 9304 8395
wait_complete
addex 9923 836 240 500 360 0
doaction 9923 1610
wait_complete

wait_time 1
changeteam 9301 0
remove 9702
wait_complete

run 9301 250 360
wait_timeex 200
doaction 9201 8000
wait_complete
playsound "sfx/ar.wav" 0 100 0

talkex 1000 194 195
wait_complete

fadein 1 2
playsound "sfx/julian_sp1.wav" 0 100 0
remove 9301
remove 9304
remove 9923
wait_complete

talkex 1000 196 196
wait_complete

doaction 9201 8130
playsound "sfx/julian_sp3.wav" 0 100 0
wait_timeex 500
camera_shake_ex 100 100
wait_time 1

changeteam 9302 1
changeteam 9303 1

playsound "sfx/Bark.wav" 0 100 0
fadeout 1 1
playsound "sfx/Bomb.wav" 0 100 0
addex 9601 800 300 0 390 0
doaction 9601 730
playsound "sfx/Bomb.wav" 0 100 0
addex 9602 800 400 -100 391 0
doaction 9602 730
playsound "sfx/Bomb.wav" 0 100 0
addex 9603 800 750 0 330 0
doaction 9603 730
playsound "sfx/Bomb.wav" 0 100 0
addex 9604 800 550 -100 350 0
doaction 9604 730
wait_timeex 300

playsound "sfx/Bomb.wav" 0 100 0
addex 9605 800 510 -100 392 0
doaction 9605 730
playsound "sfx/Bomb.wav" 0 100 0
addex 9606 800 350 0 320 0
doaction 9606 730
wait_timeex 300
playsound "sfx/Bomb.wav" 0 100 0
addex 9607 800 600 -100 340 0
doaction 9607 730
playsound "sfx/Bomb.wav" 0 100 0
addex 9608 800 950 0 398 0
doaction 9608 730
wait_timeex 300

playsound "sfx/julian_sp2.wav" 0 100 0
remove 9302
remove 9303
remove 9701
remove 9703

playsound "sfx/Bomb.wav" 0 100 0
addex 9609 800 400 0 390 0
doaction 9609 730
playsound "sfx/Bomb.wav" 0 100 0
addex 9610 800 510 -100 391 0
doaction 9610 730
playsound "sfx/Bomb.wav" 0 100 0
addex 9611 800 620 0 330 0
doaction 9611 730
playsound "sfx/Bomb.wav" 0 100 0
addex 9612 800 700 -100 350 0
doaction 9612 730
wait_timeex 300

playsound "sfx/Bomb.wav" 0 100 0
addex 9613 800 440 -100 392 0
doaction 9613 730
playsound "sfx/Bomb.wav" 0 100 0
addex 9614 800 550 0 320 0
doaction 9614 730
wait_timeex 300
playsound "sfx/Bomb.wav" 0 100 0
addex 9615 800 650 -100 340 0
doaction 9615 730
playsound "sfx/Bomb.wav" 0 100 0
addex 9616 800 680 0 398 0
doaction 9616 730
wait_timeex 300

camera_pos 0
doaction 9201 0
wait_complete
wait_time 1

doaction 9201 900
wait_complete

wait_timeex 1500

fadein 0 1
showpicture "ver11_s1_picture/ver11_s1_5.png"
wait_timeex 500
talkex 1000 197 197
wait_complete

wait_timeex 500

fadeout 0 1
wait_time 1

hidepicture

end