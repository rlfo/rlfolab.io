; this file must be in UNICODE format
begin

background 10
playmusic "BGM/lfo_stadium1.ogg"

camera_pos 0

addex 9000 5001 0 0 400 0
addex 9001 2 0 0 401 0
addex 9002 3117 1000 0 399 1
addex 9003 3117 900 0 370 1
faceleft 9003
addex 9004 3117 1200 0 420 1
addex 9005 3117 1200 0 380 1
faceleft 9005
addex 9006 3117 1100 0 360 1
addex 9007 3117 950 0 410 1
faceleft 9007

runex 9000 400 400 0
runex 9001 500 401 0
wait_complete

talkex 1000 75 76
wait_complete

faceleft 9001
talkex 1000 77 78
wait_complete

faceright 9001
camera_follow 9001
runex 9000 1000 400 0
runex 9001 1600 401 0

wait_time 2

playsound "SFX/hit1.wav" 0 100 1
wait_complete

runex 9000 1350 400 0
wait_complete

camera_pan 1000
doaction 9002 8100
faceright 9002
playsound "SFX/pig.wav" 0 100 1
talkex 1000 79 79
wait_complete

faceleft 9001
talkex 1000 80 80
wait_complete

faceleft 9000
talkex 1000 81 81
wait_complete

playsound "SFX/pig.wav" 0 100 1
talkex 1000 82 86
wait_complete

playsound "SFX/pig.wav" 0 100 1
runex 9001 1300 401 1

fadeout 0 1
wait_time 1

end