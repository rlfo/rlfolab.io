; this file must be in UNICODE format
begin

background 0

camera_pos 750

background 2
playmusic "BGM/lfo_lobby1_2.ogg"

addex 2 2 1210 0 400 0
addex 3 3 1090 0 400 0

faceleft 2
wait_complete

talkex 1000 10 10
wait_complete
doaction 2 920
wait_time 1
talkex 1000 11 11
wait_complete
doaction 2 0
talkex 1000 12 13
wait_complete

doaction 2 500
wait_complete
doaction 2 500
wait_complete
doaction 2 500
wait_complete

doaction 2 570
wait_complete
doaction 2 570
wait_complete
doaction 2 570
wait_complete

doaction 2 450
wait_time 1
doaction 2 450
wait_time 1
doaction 2 450
wait_time 1
talkex 1000 14 16
wait_complete

end