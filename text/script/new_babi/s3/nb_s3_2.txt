; this file must be in UNICODE format
begin

background 0

camera_pos 10

background 2
playmusic "BGM/lfo_lobby1_2.ogg"

addex 2 2 0 0 497 0
addex 3 3 0 0 497 0

wait_complete
fadein 0 2
wait_complete
move 2 167 511
move 3 200 497
wait_complete
talkex 1000 7 15
wait_complete
run 2 359 511
wait_timeex 230
doaction 2 450
wait_timeex 800
run 2 900 497
wait_complete
talkex 1000 16 17
wait_complete

end