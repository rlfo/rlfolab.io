; this file must be in UNICODE format
begin

background 0

camera_pos 620

background 8
playmusic "BGM/lfo_lobby1_2.ogg"

addex 2 2 1000 0 400 0
addex 3 3 1100 0 400 0
addex 4 3164 1500 0 380 0

faceleft 3
wait_complete
fadein 0 2
wait_complete
doaction 3 900
doaction 2 900
talkex 1000 31 31
wait_complete
camera_pan 850
talkex 1000 32 32
wait_complete

end