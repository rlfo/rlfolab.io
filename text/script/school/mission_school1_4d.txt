; this file must be in UNICODE format
begin

playmusic "bgm/lfo_bliss.ogg"
background 17

camera_pos 500

add 9000 3333 800 0 350
add 9001 1 970 0 350

add 9013 3 550 0 245
add 9014 4 600 0 245

add 9506 5006 1140 0 248
add 9041 5002 1185 0 255
DoAction 9041 1
add 9042 5005 1225 0 245
add 9043 5002 1260 0 255

faceleft 9506
faceleft 9041
faceleft 9042
faceleft 9043

faceleft 9001
faceright 9000

talkEX 2510 11 11
wait_complete

Fadeout 0 1
end