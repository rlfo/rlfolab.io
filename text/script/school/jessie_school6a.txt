; this file must be in UNICODE format
; Jessie beats up Turtle

begin
background 6
playmusic "bgm/lfo_humor.ogg"
background 7
camera_pos 420

; Jessie (normal)
add 9071 71 790 0 300

; Turtle
add 9611 61 890 0 300
faceleft 9611

; Other members
add 9612 61 550 0 275
add 9613 61 550 0 325
add 9614 61 550 0 375 
add 9615 61 550 0 425

add 9616 61 1120 0 275 
add 9617 61 1120 0 325
add 9618 61 1120 0 375
add 9619 61 1120 0 425
faceleft 9616
faceleft 9617
faceleft 9618
faceleft 9619

; Jessie Beat up Turtle
doaction 9071 500
wait_complete
doaction 9071 520
wait_complete
talkEx 1000 151 151
wait_complete
move 9611 950 300
talkEx 1000 152 158
wait_complete

; Ying Pang Shows up
add 9620 61 300 0 300
move 9620 700 300
faceleft 9071
talkEx 1000 159 162
wait_complete

; Dramaster & Louis Shows up
add 9016 16 300 0 320
move 9016 600 320

add 9072 72 300 0 350
move 9072 540 350

faceleft 9620
faceleft 9071

; Team Member move off a bit
moveEx 9612 650 220 1
moveEx 9613 750 220 1
moveEx 9614 850 220 1
moveEx 9615 950 220 1

moveEx 9616 650 550 1
moveEx 9617 750 550 1
moveEx 9618 850 550 1
moveEx 9619 950 550 1

talkEx 1000 163 180
wait_complete


; Start Fighting Louis
playmusic "bgm/lfo_courage.ogg"
moveEx 9611 1050 220 1
moveEx 9620 1050 550 1
move 9072 620 280 
move 9016 680 350
moveEx 9071 1000 350 1
talkEx 1000 181 183
wait_complete
end