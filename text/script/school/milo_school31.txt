; this file must be in UNICODE format
begin
background 31
playmusic "bgm/lfo_normal.ogg"

background_offsetx -100 

; Milo = 9000
addEX 9054 54 1450 0 400 1
faceleft 9054
; student
addEX 9508 5008 700 0 370 1
faceleft 9508

;============== Milo asks the way to practice room ==================
camera_pos 800
moveEX 9054 1450 400 1
wait_complete

move 9508 1150 400
wait_complete

move 9508 1300 350
wait_complete

talkEX 604 1 4
wait_complete

move 9508 1350 400
talkEX 604 10 13
wait_complete

run 9508 1500 280
wait_complete
remove 9508

talkEX 604 14 14
wait_complete
end


