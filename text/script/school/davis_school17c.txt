; this file must be in UNICODE format
begin
background 17
playmusic "bgm/lfo_humor.ogg"
; davis = 9001
camera_pan 100
add 9001 1    600 0 350
add 9010 5006 630 0 375
faceleft 9001
faceleft 9010
add 9003 3 100 0  350
wait_complete
;============== Part 17 ==================
run 9003 500 350 
wait_complete
talk 311
wait_complete
camera_follow 9001
run 9001 500 350
wait_complete
faceleft 9003
run 9001 10 350
wait_complete
end