; this file must be in UNICODE format
begin
background 31

background_offsetx -100

playmusic "bgm/lfo_humor.ogg"

add 9003 3 10 0 400

; ricky and victor
addEX 9051 51  1150 0 405 1
addEX 9055 55  1300 0 410 1
;faceleft 9051
faceleft 9055

addEX 9201 206 1270 0 405 0
changeholder 9201 9055

camera_follow 9051



talkEX 704 0 1
wait_complete

faceleft 9051
talkEX 704 2 4
wait_complete


;============== cutie attacks by bees  ==================
camera_follow 9003
move 9051 1200 405
move 9055 1150 405
run 9003 450 400
wait_complete

camera_pan 300
DoAction 9055 560
wait_complete
talkEX 503 0 0
wait_complete

addEX 9302 3002 900 0 400 0
addEX 9312 3002 900 0 400 0
addEX 9322 3002 900 0 400 0 
addEX 9332 3002 900 0 400 0
addEX 9342 3002 900 0 400 0
addEX 9352 3002 900 0 400 0


move 9302 760 370
move 9332 830 380
move 9312 790 390
move 9342 850 400
move 9352 820 410
move 9322 770 420

talkEX 503 1 1
wait_complete

;============== victor and ricky come out to save  ==================
camera_follow 9051

runEX 9051 700 350 0
runEX 9055 650 360 0
wait_complete

runEX 9051 700 400 0
runEX 9055 650 410 0
wait_complete

camera_pan 300
talkEX 503 2 4
wait_complete

move 9302 730 380
move 9312 720 400
move 9322 710 420 
move 9332 760 390
move 9342 750 410
move 9352 800 400
wait_complete
DoAction 9051 500
DoAction 9055 500
wait_complete
DoAction 9051 510
DoAction 9055 510
wait_complete
DoAction 9051 520
DoAction 9055 520
wait_complete
addEX 9362 3002 900 0 400 0
addEX 9372 3002 900 0 400 0
addEX 9382 3002 900 0 400 0
wait_complete

move 9302 760 370
move 9362 830 380
move 9312 790 390
move 9372 850 400
move 9382 820 410
move 9322 770 420
moveEX 9332 680 390 0
moveEX 9342 660 410 0
moveEX 9352 690 430 0
;DoACtion 9382 500
;wait_complete

talkEX 503 5 5
wait_complete

faceright 9302
DoAction 9332 500
DoAction 9342 500
DoAction 9352 500
talkEX 503 6 6
wait_complete


end


