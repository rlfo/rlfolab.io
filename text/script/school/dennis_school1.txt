; this file must be in UNICODE format
begin
background 26
playmusic "bgm/lfo_normal.ogg"

;background_offsetx -200 
background_offsetx 50 

; dennis = 9000
addEX 9000 4 300 0 350 1
; ricky and victor
addEX 9001 70 1310 0 350 1
add 9002 5014 1400 0 370
addEX 9003 3021 1260 0 380 1

; boy 
addEX 9010 3005 1480 0 370 1
faceleft 9010
wait_complete

;============== Dennis arrives the scene ==================
camera_follow 9000
run 9000 800 350
wait_complete
talkEX 801 0 2
wait_complete

camera_pan 950
wait_complete
DoAction 9002 500
wait_complete
talkEx 801 12 12
DoAction 9002 500
wait_complete
run 9000 1000 350
talkEX 801 3 3
DoAction 9002 500
wait_complete

;============== Dennis tries to stop them ==================
;camera_pan 900
faceleft 9001
faceleft 9002
faceleft 9003
talkEX 801 4 6
wait_complete

faceright 9001
faceright 9002
faceright 9003
talkEX 801 7 7
wait_complete

;============== Dennis runs to the dog ==================
;move 9002 1450 370
run 9000 1400 370
wait_complete
;addEX 9802 802 1500 0 370 1
;faceleft 9802
;DoAction 9802 8010
doaction 9002 600
run 9000 1470 370
wait_complete

talkEX 801 16 16
wait_complete

faceleft 9000
talkEX 801 17 17
wait_complete

DoAction 9002 740
wait_complete
talkEX 801 18 18
wait_complete

moveEx 9002 1380 390 0
move 9000 1460 390
wait_complete

talkEX 801 19 20
wait_complete


talkEX 801 21 27
wait_complete

end


