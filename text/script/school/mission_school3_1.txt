; this file must be in UNICODE format
begin
background 27

playmusic "bgm/lfo_humor.ogg"
; victor = 9051  yourself = 9000
camera_pan 250

add 9000 3333 200 0 400
add 9051 5001 300 0 400

add 9055 55 1000 0 400
add 9063 63 1080 0 380
add 9013 3013 1080 0 420
add 9011 3011 1130 0 430
faceleft 9055
faceleft 9063
faceleft 9013
faceleft 9011

move 9000 450 380
move 9051 500 400

move 9055 800 400
move 9063 880 370
move 9013 880 430
move 9011 930 440

wait_complete
talkEX 1000 3 6
wait_complete

camera_pan 270

move 9055 700 400
move 9063 780 370
move 9013 780 430
move 9011 830 440

run 9000 580 400

talkEX 1000 7 7
wait_complete


Fadeout 0 1
end