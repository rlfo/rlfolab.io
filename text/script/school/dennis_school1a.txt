; this file must be in UNICODE format
begin
background 26
playmusic "bgm/lfo_humor.ogg"

background_offsetx -100 

; dennis = 9000
addEX 9000 4 1570 0 370 1
; girl and victor
addEX 9001 70 1700 0 320 1
add 9002 5014 1450 0 370
addEX 9003 3021 1300 0 400 1
faceleft 9000
faceleft 9001
DoAction 9001 733 
DoAction 9003 733 

; boy 
;addEX 9010 3005 1580 0 370 1
;faceleft 9010
wait_complete

;============== Dennis arrives the scene ==================
camera_pos 1100
talkEX 802 23 23
wait_complete

DoAction 9001 740
DoAction 9003 740
wait_complete

runEX 9001 1400 350 0
runEX 9003 1350 390 0

talkEX 802 0 8
wait_complete

;camera_pan 1300
move 9000 1650 370
wait_complete

talkEX 802 9 9
wait_complete

talkEX 802 10 11
wait_complete

move 9002 1480 370
talkEX 802 12 12
wait_complete

faceleft 9000
talkEX 802 13 19
wait_complete

move 9000 2000 370
talkEX 802 20 20
wait_complete

fadein 0 2
background_offsetx -200 
camera_pos 1600
character_pos 9000 1550 370
wait_complete

move 9000 2000 370
wait_complete
talkEX 802 21 22
wait_complete

run 9000 2450 370
wait_complete

end


