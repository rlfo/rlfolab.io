; this file must be in UNICODE format
begin
background 25
playmusic "bgm/lfo_courage.ogg"

; Milo = 9000
addEX 9054 54 400 0 370 1
; ricky and victor
addEX 9051 51 800 0 370 0
addEX 9055 55 850 0 380 0
faceleft 9051
faceleft 9055

;============== Milo meets ricky ==================
camera_follow 9051

run 9051 500 370
run 9055 550 380

DoAction 9054 8200
wait_complete
DoAction 9054 8000
wait_complete

playmusic "bgm/lfo_bliss.ogg"
talkEX 608 0 0
wait_complete

DoAction 9051 740
DoAction 9055 740
wait_complete

talkEX 608 1 9
wait_complete
end


