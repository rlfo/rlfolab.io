; this file must be in UNICODE format
begin
background 31

;background_offsetx -4000

playmusic "bgm/lfo_humor.ogg"

; stinky = 9000
add 9004 4 10 0 350

; stinky = 9000
add 9000 2 1500 0 410
faceleft 9000

; ricky and victor
add 9063 63 1370 0 405
add 9163 63 1330 0 415
add 9055 55 1260 0 410
add 9051 51 1160 0 410
; boy & coke
add 9011 501 1950 0 278
changeholder 9011 9002
wait_complete

;============== victor asks for coke ==================
camera_pos 100
run 9004 500 350
wait_complete       

talkEX 803 0 0	
wait_complete

camera_follow 9004
run 9004 880 350
wait_complete


camera_pan 750
talkEX 803 1 4	
wait_complete

move 9004 1000 540
wait_complete
move 9004 1200 550
wait_complete
remove 9004


fadein 0 5
stopmusic
remove 9000
remove 9055
remove 9051
remove 9063
remove 9163

;background_offsetx -4800
background_offsetx -5000
;camera_pos 5900
camera_pos 6350
; stinky = 9002
add 9002 2 6300 0 400
faceleft 9002
; ricky and victor
add 9055 55 6120 0 350
add 9063 63 5950 0 370
add 9051 51 6050 0 400

DoAction 9063 833
DoAction 9051 733
DoAction 9055 733
wait_complete

;============== Dennis back from toilet ==================
playmusic "bgm/lfo_humor.ogg"

add 9004 4 6500 0 600
faceleft 9004

move 9004 6600 450 
wait_complete

talkEX 803 5 6
wait_complete

camera_pan 5900
faceleft 9004
talk 27
wait_complete

run 9002 6400 280
wait_complete
remove 9002
DoAction 9055 740
DoAction 9063 740
DoAction 9051 740
wait_complete
run 9055 6400 280
run 9063 6400 280
run 9051 6400 280
wait_complete
remove 9055
remove 9051
remove 9063

camera_pan 6200
talkEX 803 7 7
wait_complete
end


