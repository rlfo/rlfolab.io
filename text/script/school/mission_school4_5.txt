; this file must be in UNICODE format

begin
background 5
playmusic "bgm/lfo_stadium3.ogg"

;beginskiptalk

camera_pos 450

add 9033 3333 1400 0 350

; Milo, Victor, Ricky
add 9054 54 200 0 480
add 9060 60 125 0 300
add 9160 60 220 0 280
add 9051 51 80 0 280
add 9055 55 80 0 280

; Woody, Louis
add 9005 5 110 0 240
add 9016 16 80 0 190
add 9075 61 100 0 250
add 9074 61 170 0 220

; Davis, stinky, cutie, dennis
addEX 9001 1 280 0 300 1
addEX 9102 2 250 0 300 1
addEX 9003 3 200 0 300 1 
add 9004 4 150 0 305
addEX 9501 5001 240 0 300 1
add 9311 5005 200 0 420
add 9321 5002 100 0 420

add 9065 5006 200 0 100 
add 9072 72   200 0 100 


background_offsetx 200

character_pos 9054 0 400
character_pos 9060 0 400
character_pos 9160 0 400
character_pos 9051 0 400
character_pos 9055 0 400
character_pos 9005 0 400
character_pos 9016 0 400
character_pos 9075 0 400
character_pos 9074 0 400
character_pos 9001 0 400
character_pos 9102 0 400
character_pos 9003 0 400
character_pos 9004 0 400
character_pos 9501 0 400
character_pos 9311 0 400
character_pos 9321 0 400
character_pos 9065 0 400
character_pos 9072 0 400
wait_complete

character_pos 9003 500 350
character_pos 9033 400 400
character_pos 9001 850 350
character_pos 9055 900 400
faceleft 9001
faceleft 9055
talkEX 2505 3 3
wait_complete

; leave the stage
move 9003 0 350 
move 9033 0 400
move 9001 0 350
move 9055 0 400

talkEX 2506 0 0
wait_complete

; move up to the stage
character_pos 9004 0 350
character_pos 9033 0 400
character_pos 9016 1100 350
character_pos 9054 1100 400
wait_complete

move 9004 500 350 
move 9033 450 400
move 9016 850 350
move 9054 900 400 
wait_complete

talkEX 2506 1 2
wait_complete

end