; this file must be in UNICODE format
begin
background 10

playmusic "bgm/lfo_humor.ogg"
camera_follow 9000

add 9000 3333 500 0 400

add 9001 51 400 0 400
add 9002 55 360 0 420
add 9013 3013 380 0 380
add 9014 3014 300 0 400
add 9023 3017 280 0 380
add 9024 3015 260 0 420

faceleft 9000

wait_complete

talkEX 1000 9 9
wait_complete

faceright 9000

run 9001 1200 300
run 9002 1200 300
run 9013 1080  280
run 9014 1000  300
run 9023 980  300
run 9024 920  300

wait_complete

Fadeout 0 1
end