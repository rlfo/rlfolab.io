; this file must be in UNICODE format
begin
background 19
camera_pos 1550
playmusic "bgm/lfo_humor.ogg"
; cutie = 9003  yourself = 9000
add 9000 3333 2050 0 400
add 9001 1    1850 0 400


add 9030 5003 2280 0 50
faceleft 9030

add 9023 5004 1590 0 370
faceright 9023

add 9032 5001 1620 0 450
faceright 9032

add 9033 5003 1740 0 50
faceleft 9033

add 9020 5008 2180 0 350
faceright 9020

add 9021 5004 2320 0 480
faceleft 9021

add 9031 5001 2180 0 500
faceleft 9031

faceright 9001
faceleft 9000

talkEX 2510 1 1
wait_complete

move 9001 2100 450
wait_complete

Fadeout 0 1
end




