; this file must be in UNICODE format
begin
background 17

playmusic "bgm/lfo_humor.ogg"
; cutie = 9003  yourself = 9000
add 9000 3333 0 0 340

;addEX 9112 3012 500  0 340 1
;addEX 9212 3012 500  0 440 1
;addEX 9312 3012 600  0 300 1
;addEX 9412 3012 600  0 480 1
addEX 9512 3012 700  0 340 1
;addEX 9612 3012 700  0 440 1
;addEX 9712 3012 800  0 300 1
;addEX 9812 3012 800  0 480 1
;addEX 9912 3012 900 0 340  1
;addEX 9922 3012 900 0 440  1
addEX 9003 3    650 0 342 1

faceright 9000
faceright 9003
camera_follow 9000
wait_complete


DoAction 9003 500 
playsound "SFX\lightHit.wav" 0 100 100
wait_complete
DoAction 9003 510 
playsound "SFX\lightHit.wav" 0 100 100
wait_complete
DoAction 9003 500 
playsound "SFX\lightHit.wav" 0 100 100
wait_complete
DoAction 9003 510 
playsound "SFX\lightHit.wav" 0 100 100
wait_complete
DoAction 9003 500 
playsound "SFX\lightHit.wav" 0 100 100
wait_complete
DoAction 9003 510 
playsound "SFX\lightHit.wav" 0 100 100
wait_complete

move 9000 470 340
wait_complete

faceleft 9003
move 9003 630 340
wait_complete

talkEX 2500 1 1
wait_complete

faceleft 9000
move 9000 420 340
wait_complete

move 9003 580 340
wait_complete

faceright 9000

talkEX 2500 2 2
wait_complete

Fadeout 0 1
end




