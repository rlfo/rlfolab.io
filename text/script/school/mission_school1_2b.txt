; this file must be in UNICODE format
begin
background 4
camera_pos 1450
playmusic "bgm/lfo_humor.ogg"
; cutie = 9003  yourself = 9000
add 9000 3333 2050 0 340
add 9004 4    1650 0 340
add 9007 5007 1350 0 340

faceright 9004
faceleft 9000
faceleft 9007

talkEX 2500 5 5
wait_complete

run 9004 1900 340
move 9007 1500 340
; camera_follow 9004
wait_complete

faceleft 9004
talkEX 2500 6 8
wait_complete

move 9007 1350 340

faceright 9004
talkEX 2500 9 9
wait_complete

move 9004 1950 360
wait_complete
faceright 9000
run 9004 2350 400

wait_complete

Fadeout 0 1
end




