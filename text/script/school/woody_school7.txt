; this file must be in UNICODE format
begin
background 7

background_offsetx -100

playmusic "bgm/lfo_normal.ogg"                                  


camera_pos 530
add 9002 61 1000 0 400
add 9001 61 1100 0 400

add 9261 61 650 0 265
add 9061 61 700 0 245
add 9361 61 740 0 265
add 9161 61 770 0 245

add 9005 5  860 0 280

add 9044 61 1050 0 245
add 9041 61 1085 0 255
add 9042 61 1125 0 245
add 9043 61 1160 0 255


faceleft 9001
faceleft 9506
faceleft 9041
faceleft 9042
faceleft 9043
faceleft 9044


DoAction 9001 500
DoAction 9002 210
wait_complete
DoAction 9001 510
wait_complete
DoAction 9001 520
wait_complete
DoAction 9002 470
DoAction 9002 530
wait_complete

                                                 
talkEX 1501 0 0  
wait_complete

DoAction 9001 740
talkEX 1501 1 1  
wait_complete

moveEX 9001 1200 240 1
moveEX 9002 820 260 0
moveEX 9005 1050 400 1
talkEX 1501 2 3
wait_complete
moveEX 9044 800 400 0
wait_complete


talkEX 1501 4 8
wait_complete
end


