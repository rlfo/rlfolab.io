; this file must be in UNICODE format
begin
background 17
playmusic "bgm/lfo_normal.ogg"
; davis = 9001
add 9001 1 1000 0 280
add 9004 4 400 0 350

;============== Part 17 ==================
camera_pos 500
wait_complete
talk 316
wait_complete
move 9004 600 350
playmusic "bgm/lfo_delight.ogg"
talk 308
faceleft 9001
wait_complete
end


