; this file must be in UNICODE format
begin

background 13

camera_pan 400

playmusic "BGM/lfo_p5s6_2.ogg"

addex 9000 362 800 0 390 0
addex 9001 5097 650 0 360 0
addex 9002 3270 900 0 391 1
faceleft 9002

doaction 9000 8000
doaction 9002 200
wait_complete
doaction 9002 520
;wait_timeex 750
;doaction 9002 510

wait_timeex 500
camera_pan 300

talkex 1000 129 130
wait_complete

doaction 9000 8410
talkex 1000 131 135
wait_complete

moveex 9002 750 391 1
talkex 1000 136 138
wait_complete

camera_follow 9002
moveex 9002 1000 381 0
moveex 9001 850 380 0
wait_timeex 800

camera_pan 300
talkex 1000 139 139
wait_complete

faceleft 9002
faceleft 9001
talkex 1000 140 153
wait_complete

moveex 9001 500 381 0
wait_time 1
moveex 9000 600 390 0
wait_complete
talkex 1000 154 156
wait_complete

faceleft 9000
talkex 1000 157 166
wait_complete

doaction 9000 9060
playsound "SFX/milo_sp1.wav" 0 100 1
wait_timeex 200
doaction 9000 9061
wait_timeex 300
doaction 9000 0
doaction 9001 8020
wait_time 1
playsound "SFX/ready_transform.wav" 0 100 1
talkex 1000 167 170
wait_complete

moveex 9002 900 391 1
talkex 1000 171 173
wait_complete

moveex 9000 750 390 0
faceright 9001
wait_complete

talkex 1000 174 175
wait_complete

moveex 9002 1500 390 0
moveex 9000 1500 391 0
wait_time 2

remove 9002
remove 9000
talkex 1000 176 176
wait_complete

fadeout 0 2
wait_time 2
playmusic "BGM/lfo_p5s1_2.ogg"
fadein 0 2

background 20

camera_pos 0

addex 9050 5099 180 0 420 0
doaction 9050 8000

addex 9051 845 330 -120 480 0
doaction 9051 1610

addex 9052 845 130 -170 480 0
doaction 9052 1620

addex 9003 295 350 0 330 0
faceleft 9003
wait_timeex 300

talkex 1000 177 177
wait_complete

remove 9051
remove 9052
doaction 9050 8550
fadeout 1 1
wait_time 1
addex 9060 845 190 -190 480 0
doaction 9060 1610
wait_timeex 300

remove 9060

doaction 9050 8600
wait_time 1
talkex 1000 178 178
wait_complete
talkex 1000 179 179
moveex 9003 300 350 1
wait_complete

doaction 9050 8610
wait_complete

talkex 1000 180 180
wait_complete
wait_time 1

talkex 1000 181 182
wait_complete


fadeout 0 2
stopmusic
wait_time 2
fadein 0 1

background 0

camera_pan 2400

wait_timeex 1200

talkex 1000 183 186
wait_complete

wait_time 1

playmusic "BGM/lfo_p4s4.ogg"
talkex 1000 187 187
wait_complete

fadeout 1 1
wait_time 1
fadein 1 3

background 21

camera_pos 1000

addex 9009 7018 1750 0 390 0
doaction 9009 9499

addex 9008 7077 1350 0 390 0
faceleft 9008

camera_follow 9008
moveex 9008 900 390 1

addex 9005 7002 1150 0 390 0
doaction 9005 8960

addex 9006 274 980 0 360 0
addex 9007 282 1020 0 410 0
doaction 9007 8050

wait_time 3

faceleft 9005
doaction 9005 8950

talkex 1000 188 190
wait_complete

moveex 9005 800 390 1
wait_timeex 500
faceleft 9007
wait_timeex 500
faceleft 9006
wait_complete

talkex 1000 191 194
wait_complete

doaction 9006 910
talkex 1000 195 198
wait_complete

faceright 9005
talkex 1000 199 200
wait_complete

doaction 9006 0
wait_timeex 500
doaction 9007 0
wait_timeex 500

talkex 1000 201 201
wait_complete

moveex 9006 900 360 1
moveex 9007 940 410 1
wait_complete

talkex 1000 202 203
wait_complete
wait_time 1

talkex 1000 204 204
wait_complete

faceleft 9005
camera_follow 9006 
runex 9006 200 360 1
runex 9007 200 360 1
wait_timeex 1800
remove 9006
remove 9007
wait_time 1

moveex 9005 400 390 1
wait_time 1

talkex 1000 205 205
wait_complete

faceright 9005
talkex 1000 206 206
wait_complete

camera_follow 9019
addex 9019 7018 950 0 390 0
wait_complete
runex 9019 600 390 1
wait_complete

talkex 1000 207 210
wait_complete

moveex 9019 550 390 1
wait_complete
doaction 9019 8310
wait_time 1
talkex 1000 211 214
wait_complete

moveex 9005 200 390 1
wait_timeex 800
remove 9005
runex 9019 200 390 1
wait_timeex 700
remove 9019

fadeout 0 1
wait_time 1

end