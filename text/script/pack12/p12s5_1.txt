; this file must be in UNICODE format
begin

background 0

talkex 1000 0 2
wait_complete

playmusic "BGM/lfo_p7s1_1.ogg"

background 24
camera_pos 1040

addex 9151 3197 1320 0 390 0

addex 9170 7274 1480 0 381 0
faceleft 9170
addex 9171 7282 1550 0 461 0
faceleft 9171

addex 9100 7266 1300 0 422 0
addex 9101 7001 1650 0 461 0
faceleft 9101
addex 9102 7004 1380 0 450 0
faceleft 9102
addex 9103 7269 1180 0 421 0
addex 9104 5078 1600 0 360 0
faceleft 9104
addex 9105 7003 1480 0 420 0
faceleft 9105
addex 9106 7005 1620 0 421 0
faceleft 9106

talkex 1000 3 4
wait_complete

camera_pan 870

faceleft 9100
faceleft 9103
faceleft 9151

wait_timeex 500

addex 9091 272 780 -400 370 1
wait_complete
moveex 9091 920 390 0
wait_timeex 800

talkex 1000 5 8
wait_complete

addex 9092 277 740 -410 350 1
doaction 9092 520
wait_timeex 800

addex 9093 273 760 -420 422 1
wait_complete
moveex 9093 980 422 0

wait_complete

wait_timeex 500

talkex 1000 9 21
wait_complete

camera_follow 9093

doaction 9100 8800
doaction 9093 8300
wait_timeex 800

addex 9095 7077 1020 0 423 0
doaction 9095 8100

doaction 9100 200
doaction 9093 200
changeteam 9100 1
wait_timeex 600
doaction 9093 0
doaction 9100 0

wait_complete

camera_pan 870

talkex 1000 22 23
wait_complete

camera_pan 570

talkex 1000 24 24
wait_complete

remove 9095
remove 9093
addex 9096 7273 752 0 423 0
doaction 9096 1020

wait_complete

talkex 1000 25 25
wait_complete

doaction 9096 8000
wait_timeex 500
doaction 9100 200

camera_follow 9100
wait_complete

wait_timeex 1500

camera_follow 9103
moveex 9096 1000 423 0

wait_timeex 500

doaction 9091 8550
doaction 9092 8550
playsound "sfx/milo_sp1.wav" 0 100 0
wait_timeex 100
playsound "sfx/milo_sp1.wav" 0 100 0
wait_timeex 500
doaction 9091 0
doaction 9092 0

wait_complete

doaction 9103 660
doaction 9100 660
doaction 9102 660
talkex 1000 26 28
wait_complete

wait_timeex 500

doaction 9100 0
doaction 9102 0
doaction 9103 0
wait_complete

wait_timeex 800

talkex 1000 29 33
wait_complete

moveex 9103 1340 421 0
wait_complete

talkex 1000 34 35
wait_complete

faceleft 9103

camera_follow 9100
moveex 9100 1280 423 1
wait_complete

changeteam 9100 0
changeteam 9096 1

wait_timeex 200

doaction 9100 960
wait_timeex 300
fadein 1 1
wait_complete
doaction 9100 9300

doaction 9091 200
doaction 9092 200
doaction 9096 200

wait_timeex 500

camera_pan 540
wait_complete
addex 9200 800 800 -100 320 0
doaction 9200 730
wait_timeex 200
addex 9201 800 900 -100 500 0
doaction 9201 730

wait_timeex 1000

camera_pan 1000

runex 9151 1800 390 0
runex 9170 1800 381 0
runex 9171 1800 461 0
runex 9100 1800 422 0
runex 9101 1850 461 0
runex 9102 1880 450 0
runex 9103 1880 421 0
runex 9104 1800 360 0
runex 9105 1880 420 0
runex 9106 1820 421 0

doaction 9096 0
doaction 9092 0

wait_timeex 800

camera_follow 9096

remove 9151
remove 9170
remove 9171
remove 9100
remove 9101
remove 9102
remove 9103
remove 9104
remove 9105
remove 9106

wait_timeex 300

talkex 1000 36 38
wait_complete

fadeout 0 1
wait_time 1

end