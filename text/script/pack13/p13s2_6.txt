; this file must be in UNICODE format
begin

background 20

playmusic "BGM/lfo_p6s3_1.ogg"

camera_pos 500

addex 9000 7273 600 0 430 1
addex 9001 7193 650 0 350 1
addex 9002 7277 750 0 450 1
addex 9003 7192 680 0 470 1

addex 9010 6009 1250 0 321 0
addex 9011 6009 1000 0 341 0
addex 9012 6009 1050 0 381 0
addex 9013 6009 1200 0 401 0
addex 9014 6009 1000 0 461 0
addex 9015 6009 1220 0 501 0
addex 9016 6009 1020 0 521 0
addex 9017 6009 1220 0 551 0

doaction 9010 733
doaction 9011 733
doaction 9012 733
doaction 9013 733
doaction 9014 733
doaction 9015 733
doaction 9016 733
doaction 9017 733

faceleft 9010
faceleft 9012
faceleft 9013
faceleft 9015
faceleft 9016

addex 9021 6009 950 0 421 0
faceleft 9021

addex 9022 6008 1050 0 451 0
faceleft 9022

addex 9020 6009 1050 0 401 0
faceleft 9020

addex 9023 6009 1150 0 451 0
faceleft 9023

addex 9024 6009 1050 0 401 0
faceleft 9024

addex 9025 6009 1100 0 431 0
faceleft 9025

addex 9026 6009 1200 0 421 0
faceleft 9026

addex 9027 6009 1050 0 481 0
faceleft 9027

doaction 9002 8000
doaction 9022 200

talkex 1000 69 69
wait_complete

talkex 1000 70 76
wait_complete

playsound "SFX/milo_sp1.wav" 0 100 1
doaction 9022 8050
wait_timeex 800

doaction 9022 0
wait_timeex 1200

talkex 1000 77 81
wait_complete

runex 9003 830 453 0
wait_complete

doaction 9003 8300
runex 9022 1250 351 1
wait_timeex 600
doaction 9003 0
wait_complete

wait_timeex 500

talkex 1000 82 84
wait_complete

fadeout 0 1
wait_time 1

end