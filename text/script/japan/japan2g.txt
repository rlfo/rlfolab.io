; this file must be in UNICODE format
begin
background 16
playmusic "bgm/lfo_devil6.ogg"

background_offsetx 510

camera_pos 300

; 9001-davis 9002-stinky 9003-cutie 9005-woody
; 9007-john  9004-dennis 9197-kiki  9068-bochai
; 9011-jace  9006-henry 9071-jessie  9054-milo
addEX 9071 71 870 0 405 1
addEX 9011 194 950 0 390 1
addEX 9003 3 830 0 400 1
addEX 9004 4 710 0 425 1
addEX 9001 1 600 0 418 1
addEX 9005 5 710 0 360 1
addEX 9007 7 760 0 385 1

Faceleft 9071 
Faceleft 9011 
Faceleft 9003
Faceleft 9004
Faceleft 9001
Faceleft 9005
Faceleft 9007

talkEX 1000 120 122
wait_complete

camera_pan -230
wait_complete

playmusic "bgm/lfo_courage.ogg"

addEX 9102 3037 150 0 395 1

playsound "sfx/jump.wav" 0 100 0

Doaction 9102 30
wait_complete

addEX 9200 186 150 0 375 0

talkEX 1000 123 123
MoveEX 9071 490 405 1
MoveEX 9011 500 360 1
MoveEX 9003 450 450 1
MoveEX 9004 410 425 1
MoveEX 9001 350 418 1
MoveEX 9005 400 360 1
MoveEX 9007 460 385 1
wait_complete

talkEX 1000 124 129
wait_complete

addEX 9112 3037 70 0 365 1
addEX 9113 3037 50 0 425 1
addEX 9114 3037 230 0 365 1
addEX 9115 3037 250 0 425 1

Doaction 9112 30
Doaction 9113 30
Doaction 9114 30
Doaction 9115 30
wait_complete

playsound "sfx/jump.wav" 0 100 0

addEX 9201 3085 70 0 345 0
addEX 9202 3085 50 0 405 0
addEX 9203 3085 230 0 345 0
addEX 9204 3085 250 0 405 0
talkEX 1000 130 130
wait_complete

talkEX 1000 131 131
wait_complete

end