; this file must be in UNICODE format
begin
background 11
playmusic "bgm/lfo_greek2.ogg"
background_offsetx 200
camera_pos 350

; 9001-davis 9002-stinky 9003-cutie 9005-woody
; 9007-john  9004-dennis 9197-kiki  9068-bochai
; 9011-jace  9006-henry 9071-jessie  9054-milo
addEX 9071 71 690 0 455 1
addEX 9011 194 450 0 440 1
addEX 9003 3 780 0 450 1
addEX 9004 4 560 0 475 1
addEX 9001 1 800 0 468 1
addEX 9005 5 560 0 410 1
addEX 9007 7 610 0 435 1

MoveEX 9071 820 455 0
MoveEX 9011 590 440 0
MoveEX 9003 870 470 0
MoveEX 9004 660 475 0
MoveEX 9001 920 458 0
MoveEX 9005 610 420 0
MoveEX 9007 700 425 0
wait_complete

talkEX 1000 102 105
wait_complete

end