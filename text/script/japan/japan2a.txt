; this file must be in UNICODE format
begin
background 0
playmusic "bgm/lfo_greek2.ogg"

background_offsetx 520

camera_pos 270

talkEX 1000 0 0
wait_complete

; 9001-davis 9002-stinky 9003-cutie 9005-woody
; 9007-john  9004-dennis 9197-kiki  9068-bochai
; 9011-jace  9106-henry
addEX 9001 1 490 0 415 1
addEX 9002 2 600 0 410 1
addEX 9005 5 440 0 380 1
addEX 9003 3 340 0 378 1
addEX 9007 7 400 0 385 1
addEX 9004 4 520 0 370 1
addEX 9068 68 730 0 400 1
addEX 9197 197 680 0 420 1
addEX 9011 194 640 0 360 1
addEX 9106 6 590 0 355 1

MoveEX 9001 590 415 0
MoveEX 9002 700 410 0
MoveEX 9005 540 380 0
MoveEX 9003 440 378 0
MoveEX 9007 500 385 0
MoveEX 9004 620 370 0
MoveEX 9068 830 400 0
MoveEX 9197 780 420 0
MoveEX 9011 740 360 0
MoveEX 9106 690 355 0
wait_complete

talkEX 1000 1 8
wait_complete

talkEX 1000 184 187
wait_complete

RunEX 9001 1190 415 0
RunEX 9002 1100 410 0
RunEX 9005 1140 380 0
RunEX 9003 1140 378 0
RunEX 9007 1100 385 0
RunEX 9004 1120 370 0
RunEX 9068 1230 400 0
RunEX 9197 1180 420 0
RunEX 9011 1140 360 0
RunEX 9106 1090 355 0
wait_complete

end