; this file must be in UNICODE format
begin
background 7
playmusic "bgm/lfo_lantau4.ogg"

background_offsetx 300

camera_pos 350

; 9001-davis 9002-stinky 9003-cutie 9005-woody
; 9007-john  9004-dennis 9197-kiki  9068-bochai
; 9011-jace  9006-henry
addEX 9001 1 570 0 465 1
addEX 9005 5 440 0 430 1
addEX 9003 3 560 0 518 1
addEX 9007 7 530 0 455 1
addEX 9004 4 620 0 450 1
addEX 9011 194 510 0 400 1
addEX 9006 6 690 0 385 1
addEX 9002 2 480 0 520 1
addEX 9197 197 430 0 490 1
addEX 9068 68 450 0 495 1

; 9071-jessie  9054-milo
addEX 9071 71 510 0 495 1
addEX 9054 54 550 0 400 1

; 9201-3 enemies
addEX 9202 3062 930 0 440 0
addEX 9203 3084 900 0 475 0
addEX 9204 3062 880 0 380 0
addEX 9205 3084 1000 0 485 0
addEX 9206 3084 1070 0 430 0
addEX 9207 3062 1120 0 475 0

Faceleft 9205
Faceleft 9202
Faceleft 9203
Faceleft 9204
Faceleft 9206
Faceleft 9207

wait_time 1

talkEX 1000 29 29
Doaction 9006 8000
RunEX 9204 870 410 1
wait_complete

RunEX 9204 870 385 1
wait_complete

talkEX 1000 30 30
Doaction 9204 8000
wait_complete

talkEX 1000 31 32
wait_complete

RunEX 9001 700 385 0
RunEX 9004 760 375 0
wait_complete

camera_pan 400

Doaction 9001 8000
Doaction 9004 8300
wait_complete

Doaction 9204 700
wait_complete

wait_time 1

addEX 9102 3037 1150 0 350 1

playsound "sfx/jump.wav" 0 100 0

Doaction 9102 30
wait_complete

remove 9204

Doaction 9006 311 
wait_complete

talkEX 1000 34 36
wait_complete

talkEX 1000 37 37
RunEX 9071 610 495 0
RunEX 9054 590 440 0
RunEX 9007 810 450 0
wait_complete

talkEX 1000 38 38
Doaction 9007 8000

wait_time 1

Doaction 9202 700
Doaction 9203 700
Doaction 9205 700
wait_complete

talkEX 1000 39 39
wait_complete

addEX 9103 3037 1020 0 450 1
addEX 9104 3037 950 0 500 1
addEX 9105 3037 900 0 510 1

playsound "sfx/jump.wav" 0 100 0

remove 9202
remove 9203
remove 9205
wait_complete

Doaction 9103 30
Doaction 9104 30
Doaction 9105 30
wait_complete

talkEX 1000 40 41
wait_complete

RunEX 9206 1400 430 0
RunEX 9207 1450 475 0

camera_pan 200
wait_complete

MoveEX 9001 650 435 1
MoveEX 9004 700 400 1
MoveEX 9007 740 450 1
wait_complete

talkEX 1000 188 199
wait_complete

talkEX 1000 63 66
wait_complete

talkEX 1000 159 160
wait_complete

end