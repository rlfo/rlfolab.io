; this file must be in UNICODE format
begin

background 20

wait_timeex 500

talkex 1000 0 0
wait_timeex 500
playsound "sfx/julian_sp3.wav" 0 100 0
wait_complete

playmusic "BGM/lfo_heart.ogg"

talkex 1000 1 3
wait_timeex 500
playsound "sfx/julian_sp3.wav" 0 100 0
wait_complete

talkex 1000 4 5
wait_time 1
playsound "sfx/julian_sp3.wav" 0 100 0
wait_complete

talkex 1000 6 7
wait_time 1
playsound "sfx/julian_sp3.wav" 0 100 0
wait_complete

talkex 1000 8 9
wait_timeex 500
playsound "sfx/julian_sp3.wav" 0 100 0
wait_time 1
playsound "sfx/julian_sp3.wav" 0 100 0
wait_complete

wait_timeex 500

playmusic "BGM/lfo_school1.ogg"

background 10

camera_pos 210

addex 9001 278 550 0 350 0
addex 9002 278 700 0 350 0
faceleft 9002

doaction 9001 8400

fadein 1 2
wait_time 2

talkex 1000 10 15
wait_complete

addex 9100 7077 10 0 380 0
addex 9101 7077 10 0 200 0
addex 9102 7077 10 0 300 0
addex 9103 7077 10 0 400 0
playmusic "BGM/p5_story.ogg"
doaction 9100 8600
talkex 1000 16 16
wait_timeex 500
doaction 9101 8600
doaction 9102 8600
wait_time 2
doaction 9103 8600
wait_complete

faceleft 9001
faceright 9002
wait_timeex 200
run 9002 750 350
wait_timeex 200
run 9001 550 400
wait_complete

wait_timeex 500

faceright 9001
wait_timeex 200
faceleft 9002
wait_complete

wait_time 1

talkex 1000 17 17
wait_complete

faceright 9001
wait_timeex 200
faceleft 9002
wait_complete

talkex 1000 18 19
wait_complete

faceright 9002
wait_timeex 200
faceleft 9001
wait_complete

talkex 1000 20 20
doaction 9001 910
wait_complete

wait_timeex 500
doaction 9001 0
faceright 9001
wait_timeex 200
faceleft 9002
wait_complete

talkex 1000 21 23
wait_complete

talkex 1000 24 35
faceleft 9001
wait_complete

talkex 1000 36 36
faceright 9001
wait_complete

talkex 1000 37 46
wait_complete

fadeout 0 1
wait_time 1

playmusic "BGM/lfo_p4s2.ogg"

background 11

camera_pos 200

addex 9001 278 10 0 450 0
addex 9002 278 10 0 450 0

fadein 0 2
wait_time 3

move 9001 300 275
wait_timeex 500
move 9002 250 280
wait_complete

wait_timeex 500

talkex 1000 47 59
wait_complete

fadeout 0 1
wait_time 1

end