; this file must be in UNICODE format
begin

playmusic "BGM/lfo_p7s2_2.ogg"

background 12

camera_pos 200
addex 9000 5080 0 0 340 0
addex 9001 7001 0 0 380 0
addex 9002 194 	0 0 310 0
addex 9003 7005 0 0 390 0
addex 9004 7004 0 0 330 0
addex 9005 260 	0 0 300 0
addex 9006 71 	0 0 370 0
addex 9007 5078 0 0 350 0
addex 9008 196 	0 0 400 0
addex 9009 7197 0 0 390 0
addex 9010 7006 0 0 320 0
addex 9011 3216 0 0 400 1

wait_time 1

run 9000 800 340
move 9001 720 300   
move 9002 660 310
move 9003 610 390   
move 9004 570 330   
move 9005 400 300
move 9006 530 370
move 9007 330 380   
move 9008 710 400
move 9009 450 390   
move 9010 490 320   
wait_complete

talkex 1000 3 4
wait_complete

move 9008 860 400
wait_complete

talkex 1000 5 5
wait_complete

playmusic "BGM/lfo_p7s3_1.ogg"

run 9011 700 400
wait_complete
doaction 9011 8200
wait_complete
run 9011 920 400
wait_complete
doaction 9011 500
wait_complete
run 9011 1500 400
wait_complete

remove 9008
remove 9011
wait_complete

talkex 1000 6 7
wait_complete

camera_pan 700

run 9000 1220 340
move 9001 1150 300   
move 9002 1090 310
move 9003 1030 390   
move 9004 970 330   
move 9005 820 300
move 9006 930 370
move 9007 760 380   
move 9008 1110 400
move 9009 850 390   
move 9010 890 320  
wait_complete

talkex 1000 8 10
wait_complete

move 9004 1300 450
wait_complete

wait_time 1

talkex 1000 11 15
wait_complete

addex 9008 196  1751 0 381 1
addex 9011 3216 1911 0 380 1
addex 9012 3225 1751 0 256 1
faceleft 9011
faceleft 9012
doaction 9008 800
wait_complete

camera_pan 1200

wait_time 1

talkex 1000 16 18
wait_complete

end