; this file must be in UNICODE format

begin

background  4
playmusic "BGM/lfo_greek2.ogg"

camera_pos 1200

addex 9000 11 800 0 390 0
addex 9001 5013 800 0 400 0

fadeout 0 0
wait_time 0

fadein 0 1
wait_time 1

run 9000 1750 390
move 9001 1400 400
wait_complete
faceleft 9000
wait_timeex 500
faceright 9000
wait_timeex 500

run 9000 1550 300
wait_timeex 500
remove 9000

faceleft 9001
wait_timeex 500
faceright 9001
wait_complete

move 9001 1550 300
wait_complete
remove 9001
wait_complete

fadeout 0 1
wait_time 1

end
