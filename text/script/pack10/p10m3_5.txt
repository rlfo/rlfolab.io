; this file must be in UNICODE format

begin

background 10
playmusic "BGM/lfo_jap3.ogg"

camera_pos 620

addex 9001 286 860 0 460 0
addex 9002 9 900 0 280 0
addex 9003 3228 800 0 430 0
addex 9004 3228 800 0 500 0
addex 9005 3227 750 0 400 0
addex 9006 3227 750 0 470 0
addex 9007 3227 750 0 520 0
addex 9008 3068 680 0 400 0
addex 9009 3068 680 0 460 0
addex 9010 3068 680 0 540 0

addex 9020 288 1150 0 460 0
addex 9021 289 1150 0 280 0
addex 9022 174 1220 0 500 0
addex 9023 287 1280 0 430 0
addex 9024 285 1280 0 520 0
addex 9025 3230 1350 0 400 0
addex 9026 3230 1350 0 550 0
faceleft 9020 
faceleft 9021
faceleft 9022
faceleft 9023
faceleft 9024
faceleft 9025
faceleft 9026

giveitem 9002 124

fadeout 0 0
wait_time 0

fadein 0 1
wait_time 1

wait_time 2

playsound "sfx/049.wav" 0 100 0
addex 9030 825 1080 -100 200 0
addex 9031 825 952 -100 200 0
addex 9032 825 800 -100 200 0
addex 9033 825 682 -100 200 0
addex 9034 825 1151 -100 200 0
addex 9035 825 1230 -100 200 0
addex 9036 825 1310 -100 200 0
addex 9037 825 1440 -100 200 0
addex 9038 825 1280 -100 400 0
addex 9039 825 1180 -100 400 0
addex 9040 825 910 -100 400 0
addex 9041 825 790 -100 400 0
addex 9042 825 1290 -100 400 0
addex 9043 825 1320 -100 400 0
addex 9044 825 1440 -100 400 0
addex 9045 825 1530 -100 400 0
addex 9046 825 950 -100 500 0
addex 9047 825 854 -100 500 0
addex 9048 825 711 -100 500 0
addex 9049 825 685 -100 500 0
addex 9050 825 1050 -100 500 0
addex 9051 825 1270 -100 500 0
addex 9052 825 1340 -100 500 0
addex 9053 825 1120 -100 500 0
doaction 9030 28
doaction 9031 28
doaction 9032 28
doaction 9033 28
doaction 9034 28
doaction 9035 28
doaction 9036 28
doaction 9037 28
doaction 9038 28
doaction 9039 28
doaction 9040 28
doaction 9041 28
doaction 9042 28
doaction 9043 28
doaction 9044 28
doaction 9045 28
doaction 9046 28
doaction 9047 28
doaction 9048 28
doaction 9049 28
doaction 9050 28
doaction 9051 28
doaction 9052 28
doaction 9053 28
wait_complete

doaction 9002 870
wait_complete
doaction 9002 760
wait_complete

talkex 1000 68 70
wait_complete

changeteam 9021 1
wait_complete

doaction 9002 8600
wait_complete

playsound "sfx/049.wav" 0 100 0
addex 9060 825 1150 -100 280 0
addex 9061 825 1100 -100 280 0
addex 9062 825 1280 -100 280 0
addex 9063 825 682 -100 200 0
addex 9064 825 1151 -100 200 0
addex 9065 825 1230 -100 200 0
addex 9066 825 1310 -100 200 0
addex 9067 825 1440 -100 200 0
addex 9068 825 1280 -100 400 0
addex 9069 825 1180 -100 400 0
addex 9070 825 910 -100 400 0
addex 9071 825 790 -100 400 0
addex 9072 825 1290 -100 400 0
addex 9073 825 1320 -100 400 0
addex 9074 825 1440 -100 400 0
addex 9075 825 1530 -100 400 0
addex 9076 825 950 -100 500 0
addex 9077 825 854 -100 500 0
addex 9078 825 711 -100 500 0
addex 9079 825 685 -100 500 0
addex 9080 825 1050 -100 500 0
addex 9081 825 1270 -100 500 0
addex 9082 825 1340 -100 500 0
addex 9083 825 1120 -100 500 0
doaction 9060 28
doaction 9061 28
doaction 9062 28
doaction 9063 28
doaction 9064 28
doaction 9065 28
doaction 9066 28
doaction 9067 28
doaction 9068 28
doaction 9069 28
doaction 9070 28
doaction 9071 28
doaction 9072 28
doaction 9073 28
doaction 9074 28
doaction 9075 28
doaction 9076 28
doaction 9077 28
doaction 9078 28
doaction 9079 28
doaction 9080 28
doaction 9081 28
wait_complete

wait_time 1

talkex 1000 71 82
wait_complete

run 9002 1500 280
wait_complete

run 9022 1500 500
run 9023 1500 430
run 9024 1500 520
wait_complete

fadeout 0 1
wait_time 1

end
