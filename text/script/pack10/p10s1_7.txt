; this file must be in UNICODE format
begin

playmusic "BGM/lfo_chi2.ogg"

background 9

camera_pos 370

addex 9101 5080 520 0 320 0
addex 9102 7001 630 0 320 0
addex 9103 7004 400 0 350 0
addex 9104 7005 360 0 310 0
addex 9105 7006 440 0 300 0
addex 9106 5078 650 0 280 0
addex 9107 7197 420 0 385 0
addex 9108 71 490 0 280 0
addex 9109 194 400 0 270 0
addex 9110 196 530 0 250 0
addex 9111 260 330 0 260 0
addex 9112 7003 550 0 381 0
addex 9113 5081 480 0 350 0
addex 9114 7007 480 0 384 0
addex 9115 7051 580 0 280 0
addex 9116 55 360 0 382 0

addex 9200 3235 900 0 450 0

addex 9301 7195 1100 0 320 1
faceleft 9301
doaction 9301 833
wait_complete

addex 9302 832 850 -150 250 0
doaction 9302 210
wait_complete

doaction 9302 220
wait_complete

wait_time 1

talkex 1000 142 146
wait_complete

move 9200 950 320
wait_complete

wait_timeex 500

move 9200 950 400
wait_complete

wait_timeex 500

talkex 1000 147 148
wait_complete

fadeout 1 1
doaction 9200 8000
wait_timeex 500
remove 9200
wait_complete

wait_time 1

doaction 9302 210
playsound "sfx/stinky_sp2.wav" 0 100 0
wait_complete

wait_time 1

talkex 1000 149 149
wait_complete

fadeout 0 1
wait_time 1

playmusic "BGM/lfo_greek1.ogg"

background 13

camera_pos 1200

addex 9001 5080 1800 0 351 0
faceleft 9001
addex 9002 5081 1900 0 399 0
faceleft 9002
addex 9003 13 1850 0 450 0
faceleft 9003
addex 9004 5078 1500 0 320 0
addex 9005 7001 1450 0 400 0
addex 9006 7005 1550 0 450 0
addex 9007 263 1700 0 370 0
faceright 9007
addex 9008 11 1630 0 370 0
addex 9009 12 1750 0 420 0
faceright 9009
addex 9010 7003 1800 0 400 0
faceright 9010
addex 9011 7004 1580 0 350 0

addex 9012 92 1000 0 351 0
addex 9013 92 1000 0 421 0
faceleft 9013

addex 9014 198 950 0 351 1

addex 9016 7018 700 0 421 1

addex 9017 7018 700 0 401 1

fadeout 0 0
wait_time 0

fadein 0 1
wait_time 1

wait_time 1

talkex 1000 150 151
wait_complete

doaction 9014 8300
wait_timeex 500

faceleft 9004
faceleft 9005
faceleft 9007
faceleft 9009
faceleft 9010
faceleft 9011
faceleft 9008
faceleft 9006
wait_timeex 500

doaction 9016 8700
wait_complete

wait_time 1

talkex 1000 152 152
wait_time 1

remove 9014
remove 9016
wait_complete

doaction 9017 8100
wait_timeex 500
doaction 9005 200
wait_timeex 1500

doaction 9005 0
wait_complete

move 9017 1440 401
wait_complete

wait_time 1

talkex 1000 153 157
wait_complete

faceleft 9017

addex 9501 7195 700 0 350 0
addex 9502 194 700 0 450 0
addex 9503 14 700 0 400 0

move 9012 700 351
move 9013 700 421

move 9501 1250 352
move 9502 1260 451
move 9503 1310 401
wait_complete

talkex 1000 159 176
wait_complete

move 9017 700 401
wait_complete

faceright 9010
wait_timeex 500
faceright 9003
faceright 9004
faceright 9005
faceright 9006
faceright 9007
faceright 9008
faceright 9009
faceright 9011
wait_timeex 500

talkex 1000 177 198
wait_complete

wait_time 1

addex 9601 55 700 0 403 0
addex 9602 7051 700 0 353 0
addex 9603 5001 700 0 329 0

move 9001 1440 350
move 9002 1500 400
wait_timeex 500

move 9602 1350 352
move 9603 1300 329
wait_complete

run 9601 1400 403
wait_complete
addex 9701 8002 1460 -20 400 0
playsound "sfx/hit1.wav" 0 100 0
doaction 9701 130
wait_complete
doaction 9601 700
wait_complete


wait_timeex 500

doaction 9601 0
wait_complete

faceleft 9003
faceleft 9004
faceleft 9005
faceleft 9006
faceleft 9007
faceleft 9008
faceleft 9009  
faceleft 9010
faceleft 9011
wait_timeex 500

talkex 1000 199 204
wait_complete

move 9001 900 351
move 9002 900 399
wait_complete

faceleft 9603

faceleft 9501
faceleft 9502
faceleft 9503
wait_timeex 500

talkex 1000 205 205
wait_complete

faceleft 9601

talkex 1000 206 206
wait_complete

faceleft 9602

talkex 1000 207 213
wait_complete

run 9003 700 450
run 9005 700 400
run 9006 700 450
run 9007 700 370
run 9008 700 370
run 9009 700 420
run 9010 700 400
run 9011 700 350
wait_timeex 500
run 9501 700 350 
run 9502 700 450
run 9503 700 400
wait_timeex 500
run 9601 700 449
run 9602 700 390
run 9603 700 349
wait_complete

wait_time 1

talkex 1000 214 214
wait_complete

fadeout 0 1
wait_time 1

end