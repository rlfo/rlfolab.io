; this file must be in UNICODE format

begin

camera_pan 0

background 20

talkex 1000 0 2
wait_complete

background 0

playmusic "BGM/lfo_p95s1_3.ogg"

addex 9001 265 100 0 400 0

addex 9002 159 400 0 450 0
addex 9003 70  480 0 460 0
faceleft 9003

addex 9004 65 600 0 350 0
addex 9005 160 680 0 360 0
faceleft 9005

wait_complete
moveex 9001 300 400 0

talkex 1000 3 4
wait_complete

fadeout 0 2
wait_time 2

background 1

addex 9006 5013 350 0 400 0
addex 9007 265 450 0 411 0
faceleft 9007

talkex 1000 5 11
wait_complete

moveex 9007 1000 411 0

fadeout 0 2
wait_time 2

background 0

talkex 1000 12 13
wait_complete

moveex 9001 1000 400 0
talkex 1000 14 14
wait_complete

fadeout 0 1
wait_time 1

end
