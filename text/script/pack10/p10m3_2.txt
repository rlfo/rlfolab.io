; this file must be in UNICODE format

begin

background 4
playmusic "BGM/lfo_lantau1.ogg"

camera_pos 600

addex 9001 9 500 0 400 0
addex 9002 286 1200 0 400 0
addex 9003 3068 300 0 400 0
faceleft 9002

giveitem 9001 124

fadeout 0 0
wait_time 0

fadein 0 1
wait_time 1

move 9001 850 400
move 9003 750 400
wait_complete

wait_time 0.5

talkex 1000 22 26
wait_complete

doaction 9002 1500
wait_complete

addex 9010 3037 1050 0 400 0
doaction 9010 30
playsound "sfx/ball.wav" 0 100 0
addex 9011 186 1050 0 400 0
faceleft 9011

talkex 1000 27 34
wait_complete

fadeout 0 1
wait_time 1

end
