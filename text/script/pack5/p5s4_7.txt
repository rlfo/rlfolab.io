; this file must be in UNICODE format

begin

background 13
playmusic "bgm\lfo_devil6.ogg"
camera_pos 700
addex 9001 2 1100 0 400 0
addex 9002 3193 1300 0 400 1
faceleft 9002

doaction 9002 500
doaction 9001 420
wait_complete

doaction 9002 8000
doaction 9001 8600
wait_complete

camera_pan 800
wait_timeex 500
 
talkex 1000 184 185
wait_complete

move 9002 1100 400
wait_time 1
camera_pan 600
talkex 1000 186 188
wait_complete

talkex 1000 189 189
wait_complete

faceright 9002
talkex 1000 190 191
wait_complete

move 9001 1000 400
talkex 1000 192 192
wait_complete

faceleft 9002
talkex 1000 193 200
wait_complete

faceleft 9001
wait_timeex 500
faceright 9001
wait_timeex 500

talkex 1000 201 204
wait_complete

move 9001 900 400
talkex 1000 205 206
wait_complete

faceright 9001
talkex 1000 207 216
wait_complete

camera_shake 650
wait_time 2

talkex 1000 217 221
wait_complete

fadeout 1 1
wait_time 1

;realworld background
background 84
playmusic "bgm\lfo_p5s4_2.ogg"
camera_pos 1000
addex 9001 2 1400 0 450 0
doaction 9001 733
fadein 1 1
wait_time 1

talkex 1000 222 222
wait_complete

;doaction 9001 910
;wait_complete

talkex 1000 223 226
wait_complete

addex 9011 191 600 0 405 1
addex 9012 192 50 0 455 1
addex 9013 193 0 0 355 1
doaction 9011 8100
runex 9012 800 455 0
runex 9013 750 355 0
camera_pan 600
playmusic "bgm\lfo_devil2.ogg"
wait_complete

camera_pan 1000
wait_complete

talkex 1000 227 227
wait_complete

;doaction 9001 740
addex 9011 191 900 0 405 1
addex 9012 192 850 0 455 1
addex 9013 193 800 0 355 1
runex 9011 1200 405 0
runex 9012 1150 455 0
runex 9013 1100 355 0
;moveex 9001 1380 400 1
wait_complete

talkex 1000 228 232
wait_complete

;faceright 9001
talkex 1000 233 244
wait_complete

camera_shake 1050
;faceright 9001
;doaction 9001 200
doaction 9011 200
doaction 9012 200
doaction 9013 200

addex 9100 803 1470 -190 480 0
doaction 9100 160
playsound "sfx/bomb.wav" 0 100 0
fadeout 1 0
wait_timeex 500

addex 9101 803 1520 -160 480 0
doaction 9101 160
playsound "sfx/bomb.wav" 0 100 0
fadeout 1 0
wait_timeex 500

addex 9102 803 1490 -170 480 0
doaction 9102 160
playsound "sfx/bomb.wav" 0 100 0
fadeout 1 0
wait_timeex 500

addex 9103 803 1510 -180 480 0
doaction 9103 160
playsound "sfx/bomb.wav" 0 100 0
fadeout 1 0
wait_timeex 500

addex 9100 803 1470 -190 480 0
doaction 9100 160
playsound "sfx/bomb.wav" 0 100 0
fadeout 1 0
wait_timeex 500

addex 9101 803 1520 -160 480 0
doaction 9101 160
playsound "sfx/bomb.wav" 0 100 0
fadeout 1 2
wait_timeex 500

addex 9102 803 1490 -170 480 0
doaction 9102 160
playsound "sfx/bomb.wav" 0 100 0
wait_timeex 500

addex 9103 803 1510 -180 480 0
doaction 9103 160
playsound "sfx/bomb.wav" 0 100 0
wait_timeex 1000

background 85
playmusic "bgm\lfo_devil5.ogg"
camera_pos 1000
addex 9011 191 1200 0 405 1
addex 9012 192 1150 0 455 1
addex 9013 193 1100 0 355 1
doaction 9011 200
doaction 9012 200
doaction 9013 200
addex 9020 5042 1500 -100 420 1
fadein 1 1
wait_complete

moveex 9011 1400 405 0
moveex 9012 1350 455 0
moveex 9013 1300 355 0
wait_complete

talkex 1000 245 248
wait_complete

moveex 9012 1480 420 0
wait_complete

doaction 9012 300
remove 9020
giveitem 9012 5042
wait_complete

moveex 9012 880 450 0
moveex 9013 860 355 0
wait_time 1
faceleft 9011
wait_complete

moveex 9011 1380 300 1
wait_complete

talkex 1000 249 249
wait_complete

fadeout 0 2
wait_time 2

end