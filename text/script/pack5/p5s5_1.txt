; this file must be in UNICODE format

begin

background 1
playmusic "bgm\lfo_heart.ogg"

addex 9001 4 800 0 450 0
doaction 9001 733
addex 9002 3145 750 0 460 0
addex 9003 3065 900 0 450 0
addex 9004 3089 700 0 410 0
addex 9005 3121 820 0 420 0
faceleft 9005
faceleft 9003
camera_follow 9001
playsound "sfx/birds.wav" 0 100 0

talkex 2000 0 0
wait_complete

talkex 1000 0 4
wait_complete

doaction 9001 310
run 9002 1300 520 
run 9003 300 370 
run 9004 1300 450
run 9005 300 480
wait_complete

faceleft 9001
wait_timeex 700
faceright 9001
wait_timeex 700
talkex 1000 5 5
wait_complete

talkex 1000 6 6
wait_complete

addex 9006 265 300 0 420 0
remove 9003
remove 9004
remove 9005
move 9006 700 420
faceleft 9001
talkex 1000 7 17
wait_complete

run 9002 900 440
talkex 1000 18 19
wait_complete

faceright 9001
talkex 1000 20 22
wait_complete

faceleft 9001
talkex 1000 23 31
wait_complete

playmusic "BGM\lfo_devil1.ogg"
move 9006 600 420
talkex 1000 32 34
wait_complete

move 9001 750 450
talkex 1000 35 35
wait_complete

faceright 9006
talkex 1000 36 46
wait_complete

move 9006 920 420 
wait_complete
faceleft 9006
faceright 9001
wait_timeex 700

move 9006 1300 420
talkex 1000 47 47
wait_complete

camera_pan 400
faceright 9002
;move 9006 1300 420
run 9001 1250 450
fadeout 1 1
wait_time 1

end