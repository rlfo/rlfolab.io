; this file must be in UNICODE format
begin

background 1
playmusic "bgm/lfo_china2.ogg"
camera_pos 200

addex 9001 74 250 0 300 1
addex 9002 5  200 0 500 1
addex 9003 262 300  0 400 1

move 9001 700 450
move 9002 600 500
move 9003 700 450
wait_complete

move 9003 850 480
talkex 1000 1 3
wait_complete

faceleft 9001 
talkex 1000 4 5
wait_complete

faceleft 9003
talkex 1000 6 8
wait_complete

camera_follow 9001
talkex 1000 9 9
wait_complete

end


