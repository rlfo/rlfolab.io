begin

; ---Win dino
playmusic "bgm/lfo_p4s1.ogg"

background 0  
camera_pos 250

addex  9000  261   580 0 475   1
addex  9001  1     550 0 420   1                            
addex  9002  3129   800 0 420   2 
addex  9003  3129   900 0 480   2
faceleft 9002
faceleft 9003
wait_complete 

run 9001 700 420
wait_complete
doaction 9001  510
wait_complete
doaction 9002 500
doaction 9001 420
wait_complete

doaction 9002 8200
doaction 9003 8200
doaction 9001 210
doaction 9000 210
wait_complete
doaction 9000 0
doaction 9001 8200
doaction 9002 210

run 9003 680 475
wait_complete 
doaction 9003 510
doaction 9000 210
wait_complete 

doaction 9000 8000
doaction 9003 460
doaction 9001 8000
;faceright 9003
wait_complete
run 9001 800 420
doaction 9003 8000
wait_complete

doaction 9001 8100
wait_timeex 600
doaction 9002 733
camera_pan 300
wait_complete

run 9003 750 460
run 9001  850 460  
wait_complete
doaction 9001 8600
wait_complete

move 9001 750 460
camera_follow 9000
wait_complete
talkex 1000 38 38
wait_complete
doaction 9000 740
wait_complete
talkex 1000 39 43
wait_complete
;move 9001  950 560
wait_complete
talkex 1000 44 48
wait_complete

playmusic "BGM/lfo_p4s3.ogg"

move 9000  1400 500
camera_follow 9000

talkex 1000 49 49
wait_complete
move 9001 1250  510
wait_complete

moveex 9000 1550 400 1
move 9001 1450 410
talkex 1000 50 50
wait_complete

talkex 1000 51 51
wait_complete
faceleft 9000
talkex 1000 52 53
wait_complete
faceright 9000
wait_complete
talkex 1000 54 54
wait_complete

move 9001 1500 420
wait_complete
talkex 1000 55 55
wait_complete
faceleft 9000
talkex 1000 56 59
wait_complete
faceright 9000
wait_complete
talkex 1000 60 62
wait_complete
faceleft 9000
wait_complete
talkex 1000 63 63
wait_complete
move 9001 1520 420
wait_complete

move 9001 1520 420
wait_complete

move 9001 1800 400
move 9000 1800 400
fadeout 0 1
wait_time 1
remove 9001
remove 9000
; ---Found tree
playmusic "BGM\lfo_usa1.ogg"

background 3  
camera_pos 300

fadein 0 1
wait_time 1

addex  9000  261   180 0 440   1
addex  9001  1     150 0 390   1                            

move 9000 650  440
move 9001 550  390
wait_complete
talkex 1000 64 64
wait_complete
move 9001 800 390
wait_complete
talkex 1000 65 65
wait_complete
faceleft 9001
wait_complete
talkex 1000 66 67
wait_complete
move 9000 720 420
wait_complete
talkex 1000 68 70

wait_complete
fadein 0 1

; ---VO
background 99
talkex 2000 0 0
wait_complete
wait_timeex 500

; ---Eat fruit
fadeout 0 1
wait_time 1

background 0 
camera_pos 1200

fadein 0 1
wait_time 1
addex  9000  261   1800 0 400   1
addex  9001  1     1000 0 450    1                            

run 9001 1400 450
talkex 1000 71 71
wait_complete
faceleft 9000
wait_complete
talkex 1000 72 72
wait_complete
move 9001 1700 420
talkex 1000 73 73
wait_complete
talkex 1000 74 79
wait_complete
faceright 9000
wait_complete
talkex 1000 80 81
wait_complete
faceleft 9000
wait_complete
talkex 1000 82 88
wait_complete

move 9000 1500 400

wait_complete
talkex 1000 89 90
faceleft 9001 
wait_complete
faceright 9000
wait_complete
talkex 1000 91 92
wait_complete
move 9001 1600 380
wait_complete
talkex 1000 93 98
wait_complete

;------
move 9000 1450 430
wait_complete
talkex 1000 99 99
wait_complete
;faceright 9001
wait_complete
talkex 1000 100 101
wait_complete
move 9001 1550 410
wait_complete
talkex 1000 102 102
wait_complete
faceleft 9000
talkex 1000 103 105
wait_complete
faceright 9000
talkex 1000 106 106
wait_complete

fadein 0 1


background  21
camera_pos 130
playmusic "BGM\lfo_p4s3.ogg"

add  9000  261  300 0 370  
add  9010  193  700 0 370 
faceleft 9010
doaction 9000  8000
wait_complete
doaction 9000  510
talkex 1000 107 107
wait_complete
talkex 1000 108 109
wait_complete
move 9010 530 370
wait_complete
doaction 9010  500
wait_complete
doaction 9000  0
wait_complete
talkex 1000 110 111
wait_complete
move 9010 100 390
wait_complete
faceleft 9000
wait_complete
talkex 1000 112 112
wait_complete

add  9011  191  1000 0 370 
move 9011 800 380
faceleft 9011
wait_complete
talkex 1000 113 113
wait_complete
faceright 9000
wait_complete
talkex 1000 114 114
wait_complete
move 9011  600 380
wait_complete
talkex 1000 115 118
wait_complete
move 9011 300 380
wait_complete
faceright 9011
wait_complete
doaction 9000  8100
wait_complete
doaction 9000  8400
wait_complete

faceleft 9000
wait_complete
talkex 1000 119 119
wait_complete
faceleft 9011
wait_complete
talkex 1000 120 121
wait_complete
move 9011  100 390     
wait_complete

fadein 0 1

background 0
camera_pos 1200

addex  9000  261   1450 0 430   1
addex  9001  1     1550 0 410    1                            
faceright 9000
faceleft 9001
talkex 1000 122 124
wait_complete
faceleft 9001
move 9001  1650 410
wait_complete
talkex 1000 125 128
wait_complete
faceright 9001
talkex 1000 129 130
wait_complete
fadein 0 1


background 82
camera_pos 700
add  9011  191  800 0 200 
faceleft 9011
wait_complete
add  9000  261  1600 0 500
move 9000 1400  500
faceleft 9000
wait_complete
talkex 1000 131 131
wait_complete
move 9000 980 350 

camera_pan 600
wait_complete
talkex 1000 132 134
wait_complete
faceright 9011 
wait_complete
talkex 1000 135 139
wait_complete
move 9000  880  300
wait_complete
talkex 1000 140 144
wait_complete
move 9011 1100 400 
wait_complete
talkex 1000 145 145
wait_complete
faceright 9000
wait_complete
talkex 1000 146 146
wait_complete
faceleft 9011
wait_complete
talkex 1000 147 147
wait_complete
move 9000  900 350
wait_complete
talkex 1000 148 149

wait_complete
fadein 0 1


background 0 
camera_pos 1200

addex  9000  261   1450 0 430    1
addex  9001  1     1650  0 410    1 
faceleft 9001
talkex 1000 150 152
wait_complete
;move 9001  1350 430
faceright 9001
wait_complete
talkex 1000 153 156
wait_complete

move 9000 1400  410
wait_complete
talkex 1000 157 157
wait_complete
faceleft 9001
wait_complete
talkex 1000 158 158
wait_complete
faceright 9000
wait_complete
talkex 1000 159 162
wait_complete
move 9000  1750 420
wait_complete
talkex 1000 163 163
wait_complete
faceright 9001
;moveex 9001 1550 390 0
faceright 9001
wait_complete
talkex 1000 164 164
wait_complete
faceleft 9000
wait_complete
talkex 1000 165 167
wait_complete
fadein 0 1

end

