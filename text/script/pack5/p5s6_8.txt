begin

playmusic "bgm/lfo_p5s6_2.ogg"

background  31
camera_pos 1000
background_offsetx 1000

addex  9001  7001       400  0 350    1
addex  9000  261     500  0 400    1  
addex  9003  3039    150  0 400    2
addex  9004  3039    100  0 350    2
;add  9002  17   720 0 380   
doaction 9002  733 
faceleft 9000
faceleft 9001
wait_complete

run 9003 400 400
run 9004 260 350
wait_complete
doaction 9003  520
doaction 9000  210
doaction 9004  8200
doaction 9001  210
wait_complete
doaction 9000  510
doaction 9003  470
doaction 9004  8000
wait_complete
camera_pan 10
doaction 9001  8200
doaction 9004  400
wait_complete
doaction 9003  8200
doaction 9004  8200
doaction 9001  210
doaction 9000  210
wait_complete
;run 9000  320  400
doaction 9000 100
;doaction 9001  8000
doaction 9004  210
wait_complete
doaction 9004  0
doaction 9001  8600
doaction 9000  8200
wait_complete
talkex 1000 199 199 
wait_complete
faceright 9000
wait_complete

camera_pan 0
move 9000  620 400 
talkex 1000 200 200
wait_complete
faceright 9001
talkex 1000 201 201
wait_complete
move 9001 550 380
wait_complete
add 9010 264  50 0 380
doaction 9010 8100
playmusic "BGM\lfo_devil2.ogg"
wait_complete
doaction 9001 470
faceleft 9001
faceleft 9000
talkex 1000 202 204
wait_complete

faceleft 9000
wait_complete
talkex 1000 205 205
wait_complete
camera_pan 50
run 9010 320 400
talkex 1000 206 206
wait_complete
move 9001  450 380
wait_complete
talkex 1000 207 207
wait_complete
faceleft 9010
talkex 1000 208 208
wait_complete
talkex 1000 209 211
wait_complete
faceright 9010
talkex 1000 212 214
wait_complete
move 9000  550 400
wait_complete
talkex 1000 215 218
wait_complete
faceleft 9010
wait_complete
talkex 1000 219 220
wait_complete
faceright 9010
wait_complete
talkex 1000 221 222
wait_complete
faceright 9000
wait_complete
talkex 1000 223 224
wait_complete	
faceleft 9000
talkex 1000 225 228
wait_complete
add 9011 56 100 0 400
run 9011  150 400
wait_complete
talkex 1000 229 229
wait_complete
faceleft 9010
talkex 1000 230 231
wait_complete
run 9010 2 400
run 9011 20 400

talkex 1000 232 232
wait_complete
remove  9010
move 9000 400 400
wait_complete
talkex 1000 233 236
wait_complete

run 9000 20  400
run 9001 30 380

wait_complete
fadein 0 1

end

