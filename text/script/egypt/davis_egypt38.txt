; this file must be in UNICODE format
begin
background 38
playmusic "bgm/lfo_china3.ogg"

background_offsetx -1200

add 9080 82 1800 0 380 
faceleft 9080 

addEX 9001 1 850 0 500 1
addEX 9008 8 800 0 500 1

;============== on the way looking for fat worm ==================
camera_follow 9001

run 9001 1500 380
run 9008 1400 420
wait_complete

camera_pan 1150
talkEX 1017 0 0
wait_complete

talkEX 1017 1 3
wait_complete

camera_pan 1200
runEX 9080 1800 400 1
run 9008 1700 420
wait_complete

DoAction 9080 8100
DoAction 9008 470
talkEX 1017 4 4
wait_complete

camera_pan 1300
run 9001 1650 400
wait_complete
DoAction 9001 8200
DoAction 9080 210
;wait_complete

run 9008 1850 420
wait_complete

DoAction 9080 0
run 9008 2200 420
talkEX 1017 5 6
wait_complete


end
