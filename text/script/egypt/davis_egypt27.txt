
; this file must be in UNICODE format
begin
background 27
playmusic "bgm/lfo_rage.ogg"

camera_pos 300
addEX 9001 1 10 0 300 7
addEX 9008 8 10 0 310 7

addEX 9901 165 900 0 280 1
addEX 9902 165 800 0 290 1
addEX 9903 165 850 0 350 1
addEX 9904 165 890 0 420 1
addEX 9905 165 820 0 440 1
addEX 9907 165 700 0 280 1
addEX 9908 165 710 0 340 1
addEX 9909 165 750 0 360 1
addEX 9910 165 810 0 390 1
addEX 9911 165 730 0 450 1
addEX 9913 165 500 0 300 1
addEX 9914 165 620 0 320 1
addEX 9915 165 550 0 340 1
addEX 9916 165 600 0 370 1
addEX 9917 165 700 0 390 1
addEX 9918 165 600 0 430 1 

DoAction 9901 900 
wait_complete
DoAction 9902 900 
wait_complete
DoAction 9903 900 
wait_complete
DoAction 9904 900 
wait_complete
DoAction 9905 900 
DoAction 9907 900
wait_complete
DoAction 9908 900 
wait_complete
DoAction 9909 900 
wait_complete
DoAction 9910 900 
DoAction 9911 900 
wait_complete
DoAction 9913 900
wait_complete
DoAction 9914 900 
wait_complete
DoAction 9915 900 
wait_complete
DoAction 9916 900 
wait_complete
DoAction 9917 900 
wait_complete
DoAction 9918 900 
wait_complete

; dummy
add 9999 1 1800 0 500

;============== traps ==================
camera_pan 0
move 9001 400 320 
move 9008 360 380  
wait_complete

talkEX 1013 0 1 
wait_complete

camera_pan 300
talkEX 1013 2 3 
wait_complete

; ============ woody appears ========
camera_pan 0
add 9005 5 300 0 310
addEX 9006 6 250 0 350 1
faceleft 9001
faceleft 9008
talkEX 1013 4 11
wait_complete


; ============ ah la comes out  ========
camera_pan 600
addEX 9079 79 1500 0 500 1
DoAction 9901 0 
DoAction 9902 0 
DoAction 9903 0 
DoAction 9904 0 
DoAction 9905 0 
DoAction 9907 0
DoAction 9908 0 
DoAction 9909 0 
DoAction 9910 0 
DoAction 9911 0 
DoAction 9913 0
DoAction 9914 0 
DoAction 9915 0 
DoAction 9916 0 
DoAction 9917 0 
DoAction 9918 0
move 9079 1180 320
wait_complete


talkEX 1013 12 16
wait_complete


add 9502 507 1300 0 270
changeholder 9502 9079
talkEX 1013 17 19
wait_complete


move 9079 900 300
wait_complete

changeholder 9502 9999
moveEX 9079 1180 320 1
DoAction 9901 900 
wait_complete
DoAction 9902 900 
wait_complete
DoAction 9903 900 
wait_complete
DoAction 9904 900 
wait_complete
DoAction 9905 900 
DoAction 9907 900
wait_complete
DoAction 9908 900 
wait_complete
DoAction 9909 900 
wait_complete
DoAction 9910 900 
DoAction 9911 900 
wait_complete
DoAction 9913 900
wait_complete
DoAction 9914 900 
wait_complete
DoAction 9915 900 
wait_complete
DoAction 9917 900 
wait_complete
DoAction 9916 900 
DoAction 9918 900 
talkEX 1013 20 21
wait_complete

camera_pan 0
faceright 9001
faceright 9008
talkEX 1013 23 29
wait_complete

; ========= fat worm comes out ==========
camera_pan 600
faceright 9079
talkEX 1013 30 30
wait_complete

addEX 9313 167 1450 0 450 1
addEX 9323 167 1600 0 470 1
addEX 9078  78 1550 0 500 1
addEX 9333 167 1500 0 530 1
addEX 9343 167 1650 0 550 1
move 9313 1100 320
move 9323 1150 340
move 9078 1080 360
move 9333 1170 380
move 9343 1120 400

moveEX 9079 1300 270 1
wait_complete

talkEX 1013 31 31
wait_complete

camera_pan 0
talkEX 1013 32 35
wait_complete

camera_pan 600
talkEX 1013 36 38
wait_complete

add 9502 508 1300 0 270
changeholder 9502 9079

talkEX 1013 39 40
wait_complete

camera_pan 0
talkEX 1013 43 43
wait_complete

faceleft 9001
talkEX 1013 44 45
wait_complete

faceleft 9008
talkEX 1013 46 47
wait_complete

; ========= prepare to burn fat worm ==========
run 9001 500 550 
run 9008 600 550 
run 9005 600 550
run 9006 600 550
wait_complete

camera_pan 600
run 9001 1500 550 
run 9008 1500 550 
run 9005 1500 550
run 9006 1500 550
faceright 9078
moveEX 9313 1200 400 1
moveEX 9323 1300 400 1
moveEX 9333 1250 420 1
moveEX 9343 1350 420 1
talkEX 1013 41 42
wait_complete

add 9091 901 450 0 370
changeholder 9091 9006
character_pos 9006 1500 300
faceleft 9006
move 9079 1200 300 
wait_complete
DoAction 9006 1600
wait_complete
add 9512 508 1230 0 300
changeholder 9502 9999 
DoACtion 9999 560
wait_complete

changeholder 9091 9999 
DoAction 9901 0 
DoAction 9902 0 
DoAction 9903 0 
DoAction 9904 0 
DoAction 9905 0 
DoAction 9907 0
DoAction 9908 0 
DoAction 9909 0 
DoAction 9910 0 
DoAction 9911 0 
DoAction 9913 0
DoAction 9914 0 
DoAction 9915 0 
DoAction 9916 0 
DoAction 9917 0 
DoAction 9918 0
wait_complete

camera_pan 830
faceright 9079
faceright 9313
faceright 9323
faceright 9333
faceright 9343
character_pos 9001 1450 260
character_pos 9008 1540 220
character_pos 9005 1570 290
character_pos 9006 1500 300


faceleft 9006
faceleft 9001
faceleft 9005
faceleft 9008
talkEX 1013 48 49
wait_complete

end
