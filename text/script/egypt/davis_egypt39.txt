; this file must be in UNICODE format
begin
background 39
playmusic "bgm/lfo_greek3.ogg"

add 9080 166 900 0 300 
DoAction 9080 900
faceleft 9080 

addEX 9008 8 600 0 350 1
addEX 9001 1 100 0 500 1


;============== on the way looking for fat worm ==================
camera_pos 200

run 9001 520 370
talkEX 1019 1 4
wait_complete

move 9008 650 350
talkEX 1019 5 11
wait_complete

faceleft 9008
talkEX 1019 12 12
wait_complete

faceright 9008
talkEX 1019 13 14
wait_complete

moveEX 9008 700 370 1
talkEX 1019 15 15
wait_complete


end
