; this file must be in UNICODE format
begin
background 22
background_offsetx 0
playmusic "bgm/lfo_rage.ogg"

camera_pos 500

AddEX 9004 265 1400 0 320 0
AddEX 9005 263 1550 0 350 0

faceleft 9005
faceleft 9006

faceleft 9020 
faceleft 9021
faceleft 9002
faceleft 9007
faceleft 9003
faceleft 9001
faceleft 9004


addex 9903 57 1020 0 330 1
addex 9904 50 1000 0 350 0
addex 9911 57 940 0 370 1
addex 9912 57 900 0 302 1

runex 9903 720 330 0
runex 9904 700 350 0
runex 9911 640 370 0
runex 9912 600 302 0



runex 9004 920 320 1
runex 9005 1050 350 1

talkex 1000 81 101
wait_complete

end