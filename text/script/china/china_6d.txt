; this file must be in UNICODE format
begin
background 15
playmusic "bgm/lfo_bliss.ogg"

background_offsetx 1500

camera_pos -450

; 9001-davis 9002-stinky 9003-cutie 9005-woody
; 9007-john  9004-dennis 9197-kiki  9068-bochai
; 9184-firstking
addEX 9001 1 20 0 400 1
addEX 9002 2 -30 0 400 1
addEX 9003 3 -150 0 400 1
addEX 9005 5 -100 0 405 1
addEX 9007 7 -210 0 400 1
addEX 9004 4 -360 0 400 1
addEX 9068 68 -320 0 408 1
addEX 9197 197 -280 0 405 1
addEX 9184 184 150 0 403 1

; 9011-jace ; 9106-henry
addEX 9011 194 -730 0 500 0
addEX 9106 6 -800 0 505 0
 
Faceleft 9184

talkEX 1000 80 80
wait_complete

Doaction 9184 733
wait_complete

wait_time 1

talkEX 1000 81 97
wait_complete

talkEX 1000 98 98
wait_complete

camera_pan -690

Faceleft 9001
Faceleft 9002
Faceleft 9003
Faceleft 9005
Faceleft 9007
Faceleft 9004
Faceleft 9068
Faceleft 9197

RunEX 9011 -500 400 0
RunEX 9106 -570 405 0
wait_complete

talkEX 1000 123 123
wait_complete

talkEX 1000 99 116
wait_complete

playsound "sfx/biggun.wav" 0 100 0

wait_time 2

talkEX 1000 117 122
wait_complete

playsound "sfx/biggun.wav" 0 100 0

Fadeout 1 2
wait_time 2

remove 9001
remove 9002
remove 9003
remove 9005
remove 9007
remove 9004
remove 9068
remove 9197
remove 9011
remove 9106

Fadeout 1 5
wait_time 5

end