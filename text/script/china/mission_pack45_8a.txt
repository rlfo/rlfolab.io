; this file must be in UNICODE format
begin
background 3
background_offsetx 0
playmusic "bgm/lfo_rage.ogg"

camera_pos 600

AddEX 9004  265 1100 0 370 0
AddEX 9101  262 1170 0 410 0 
AddEX 9102   72 1230 0 400 0
AddEX 9021   59 1200 0 480 1


AddEX 9005  263 750 0 400 0
AddEX 9007   11 800 0 340 1
AddEX 9006   12 850 0 420 1
AddEX 9003  198 890 0 390 0

AddEX 9020 5013 700 0 380 0

AddEX 9002   13 920 0 410 0
AddEX 9001   14 820 0 380 0 

faceleft 9004
faceleft 9101
faceleft 9102
faceleft 9004
faceleft 9021

 
talkex 1000 132 132
wait_complete


AddEX 9901 817 1170 0 410 0 
AddEX 9902 817 1100 0 370 0
AddEX 9903 817 1230 0 400 0
Doaction 9901 201
Doaction 9902 201
Doaction 9903 201
wait_time 1

DoAction 9004 733
DoAction 9101 733
DoAction 9102 733


talkex 1000 133 134
wait_complete

moveEX 9004  700 370 0
moveEX 9101  770 450 0 
moveEX 9102  630 400 0
talkex 1000 135 141
wait_complete

runEX 9021 1600 540 0
wait_complete



end