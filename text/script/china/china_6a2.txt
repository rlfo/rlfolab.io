; this file must be in UNICODE format
begin
background 6
playmusic "bgm/lfo_advent.ogg"

background_offsetx 1500

camera_pos -600

; 9001-davis 9002-stinky 9003-cutie 9005-woody
addEX 9001 1 -100 0 535 1
addEX 9002 2 120 0 540 1
addEX 9003 3 40 0 520 1
addEX 9005 5 -40 0 525 1

RunEX 9001 -300 535 1
RunEX 9002 -80 540 1
RunEX 9003 -160 520 1
RunEX 9005 -240 525 1
wait_complete

talkEX 1000 31 31
wait_complete

; 9183-evilgeneral
addEX 9183 183 -1000 0 530 1
addEX 9283 183 -1130 0 540 1

camera_pan -1350

wait_time 1

camera_pan -600

talkEX 1000 32 35
wait_complete

end