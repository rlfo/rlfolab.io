; this file must be in UNICODE format
begin
background 25
playmusic "bgm/lfo_rage.ogg"

background_offsetx -600

addEX 9333 3333 2150 0 340 1

addEX 9003 74 2100 0 380 1

addEX 9075 75 1000 0 350 2
addEX 9074 76 1050 0 380 2


character_pos 9075 2500 345
character_pos 9074 2450 395
wait_complete

camera_pos 1800
faceright 9009
faceleft 9075
faceleft 9074
talkEX 1000 30 32
wait_complete

run 9333 2400 360 
character_pos 9075 1500 345
character_pos 9074 1450 395
talkEX 1000 33 34
wait_complete
end
