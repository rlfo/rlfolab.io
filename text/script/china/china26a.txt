; this file must be in UNICODE format
begin
background 38
playmusic "bgm/lfo_rage.ogg"

background_offsetx -300

addEX 9333 3333  1450 0 380 1
;addEX 9009 9  1350 0 360 1
;addEX 9054 54 1500 0 400 1
;addEX 9007 7  1400 0 410 1
faceleft 9333
faceleft 9009
faceleft 9054
faceleft 9007

;addEX 9051 51 1350 0 390 1
;addEX 9055 55 1400 0 400 1

;DoAction 9051 733 
;DoAction 9055 833

addEX 9076 74 950 0 340 2
addEX 9074 75 1000 0 380 2
addEX 9075 76 900 0 400 2

;addEX 9003 3    1550 0 380 1
;addEX 9004 4    1600 0 420 1

faceleft 9003
faceleft 9004

camera_pos 850
talkEX 1000 44 45
wait_complete

faceleft 9074
faceleft 9075
faceleft 9076
DoAction 9074 460
DoAction 9075 460
DoAction 9076 460
wait_complete

end
