; this file must be in UNICODE format
begin
background 41
playmusic "bgm/lfo_courage.ogg"

addEX 9075 96 1000 0 320 1
faceleft 9075

addEX 9171 171 1100 0 280 1
addEX 9271 171 1150 0 340 1
faceleft 9171
faceleft 9271

addEX 9002 2  200 0 500 1
addEX 9004 4  150 0 550 1
addEX 9009 9   50 0 550 1
addEX 9007 7  100 0 600 1

addEX 9509 5009 1200 0 200 1
addEX 9510 5010 1250 0 230 1
addEX 9520 5010 1270 0 200 1
addEX 9519 5009 1300 0 220 1



faceleft 9509
faceleft 9510
faceleft 9519
faceleft 9520

camera_follow 9004
run 9002 750 350
run 9004 680 370


run 9009 630 360
run 9007 580 380
wait_complete

camera_pan 550
talkEX 1015 0 3
wait_complete


runEX 9004 850 320 0
wait_complete
DoAction 9004 8000
wait_complete

talkEX 1015 4 12
wait_complete




end
