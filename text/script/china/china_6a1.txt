; this file must be in UNICODE format
begin
background 0
playmusic "bgm/lfo_chi2.ogg"

background_offsetx 1000

camera_pos -550

; 9001-davis 9002-stinky 9003-cutie 9005-woody
addEX 9001 1 -180 0 385 1
addEX 9002 2 -400 0 380 1
addEX 9003 3 -500 0 390 1
addEX 9005 5 -340 0 385 1

RunEX 9001 20 385 1
RunEX 9002 -150 380 1
RunEX 9003 -260 390 0
RunEX 9005 -40 385 1
wait_complete

talkEX 1000 23 23
wait_complete

camera_pan -100
playmusic "bgm/lfo_rage.ogg"
addEX 9101 3070 1080 0 385 1
addEX 9102 3070 700 0 380 1
addEX 9103 3070 900 0 390 1
addEX 9105 3070 1040 0 385 1
RunEX 9101 520 385 1
RunEX 9102 350 380 1
RunEX 9103 460 390 1
RunEX 9105 640 385 1
faceright 9001
faceright 9002
faceright 9003
faceright 9004
faceright 9005
wait_complete

camera_pan -200
wait_complete

end