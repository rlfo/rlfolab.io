; this file must be in UNICODE format
begin
background 15
playmusic "bgm/lfo_courage.ogg"

background_offsetx 950

camera_pos 50

; 9001-davis 9002-stinky 9003-cutie 9005-woody
; 9007-john  9004-dennis 9197-kiki  9068-bochai
; 9184-firstking
addEX 9001 1 -20 0 358 1
addEX 9002 2 -80 0 430 1
addEX 9003 3 -260 0 410 1
addEX 9005 5 -120 0 385 1
addEX 9007 7 -310 0 410 1
addEX 9004 4 -460 0 400 1
addEX 9068 68 -410 0 378 1
addEX 9197 197 -360 0 405 1
addEX 9184 184 500 0 433 0

Faceleft 9184

talkEX 1000 69 69
wait_complete

camera_pan -650
wait_complete

RunEX 9001 20 378 0
RunEX 9002 -60 390 0
RunEX 9003 -200 420 0
RunEX 9005 -140 375 0
RunEX 9007 -250 410 0
RunEX 9004 -400 400 0
RunEX 9068 -350 378 0
RunEX 9197 -300 405 0
wait_complete

talkEX 1000 70 70
wait_complete

camera_pan -200

MoveEX 9001 280 408 0
MoveEX 9002 230 400 0
MoveEX 9003 90 380 0
MoveEX 9005 170 405 0
MoveEX 9007 0 410 0
MoveEX 9004 -150 400 0
MoveEX 9068 -100 408 0
MoveEX 9197 -50 405 0
wait_complete

talkEX 1000 71 79
wait_complete

end