; this file must be in UNICODE format
begin
background 13
playmusic "bgm/lfo_rage.ogg"

background_offsetx 550

camera_pos 150

; 9001-davis 9002-stinky 9003-cutie 9005-woody
addEX 9001 1 950 0 480 0
addEX 9002 2 200 0 464 0
addEX 9003 3 580 0 430 0
addEX 9005 5 420 0 400 0

Faceleft 9001
Faceleft 9003

; 9101-9115-stone soldier
addEX 9101 3070 250 0 614 1
addEX 9102 3070 290 0 544 1
addEX 9103 3070 350 0 474 1
addEX 9104 3071 400 0 510 1
addEX 9105 3071 430 0 400 1
addEX 9106 3071 440 0 670 1
addEX 9107 3071 470 0 512 1
addEX 9108 3071 550 0 522 1
addEX 9109 3071 530 0 622 1
addEX 9110 3071 720 0 571 1
addEX 9111 3071 680 0 481 1
addEX 9112 3071 600 0 391 1
addEX 9113 3071 720 0 613 1
addEX 9114 3071 760 0 443 1
addEX 9115 3071 800 0 463 1

Faceleft 9101
Faceleft 9103
Faceleft 9105
Faceleft 9106
Faceleft 9107
Faceleft 9109
Faceleft 9112
Faceleft 9113
Faceleft 9114

Doaction 9101 733
Doaction 9104 733
Doaction 9105 733
Doaction 9107 733
Doaction 9109 733
Doaction 9111 733
Doaction 9114 733
wait_complete

Doaction 9001 8400
Doaction 9002 8000
Doaction 9003 8100
Doaction 9005 8000
wait_complete

talkEX 1000 41 44
wait_complete

RunEX 9001 1000 480 0
RunEX 9002 1000 500 0
RunEX 9003 1000 510 0
RunEX 9005 1000 505 0
wait_complete

end