; this file must be in UNICODE format
; Stinky, Cutie, and Dennis arrive China.
; They tries to find the place of exam

begin
background 5
playmusic "bgm/lfo_advent.ogg"

;beginskiptalk

camera_pos 0
background_offsetx -2000
; arrow man
add 9521 3022 3200 0 480
; dog
add 9351 3005 3150 0 500

; gerson
add 9301 3020 2050 0 305
; judo b's 
add 9611 61 2125 0 300
faceleft 9611

; wood
add 9312 3012 3150 0 280
add 9313 3012 3400 0 490
; judo a's 
add 9601 60 3220 0 280
faceleft 9601
; judo c's
add 9621 62 3080 0 280

; bandit
add 9311 3021 2200 0 420
add 9321 3022 2100 0 420
faceleft 9311

background_offsetx 0

; Monks
add 9074 3023 770 0 220
add 9076 53 710 0 240
add 9075 3023 800 0 250
faceleft 9074
faceleft 9076
faceleft 9075


; stinky, cutie, dennis
addEX 9002 2 280 0 100 1
addEX 9004 4 240 0 150 1
addEX 9003 3 200 0 200 1 


; John & Rudolf
; Rudolf = 9009
; John = 9007
add 9009 9 250 0 150
add 9007 7 280 0 190

addEX 9051 51 200 0 100 1
addEX 9054 54 250 0 150 1
addEX 9055 55 150 0 200 1 

camera_pan 200

moveEX 9601 870 320 1
moveEX 9521 820 350 1
moveEX 9621 900 400 1

moveEX 9004 590 380 1
moveEX 9003 680 340 1 
moveEX 9002 600 300 1 

moveEX 9007 760 300 1
moveEX 9009 800 430 1

moveEX 9055 520 250 1
moveEX 9051 500 330 1
moveEX 9054 470 380 1

moveEX 9301 650 280 1
moveEX 9611 925 350 1
moveEX 9311 750 380 1
moveEX 9321 670 410 1

add 9065 65 200 0 100 
moveEX 9065 300 250 0
wait_complete

talkEX 1018 0 4
wait_complete

DoACtion 9004 900
talkEX 1018 5 5
wait_complete

DoAction 9004 0
faceright 9002
faceright 9004
talkEX 1018 6 10
wait_complete

faceleft 9002
faceleft 9004
talkEX 1018 11 17
wait_complete


end