; beaten tiger shark again

begin
background 42
playmusic "bgm/lfo_greek2.ogg"

camera_pan 1200


addEX 9008 161  1900 0 370 7
addEX 9005  74  1790 0 350 7
addEX 9003  3023  1820 0 400 7
addEX 9001 11  1700 0 390 7

background_offsetx -50
addEX 9004    16  1620 0 390 7
addEX 9009  3333  1660 0 340 7
addEX 9007   62  1570 0 350 7
addEX 9054    90  1500 0 400 7


faceleft 9008
faceleft 9005
faceleft 9001
faceleft 9003
faceleft 9002


talkEX 1000 23 23
wait_complete


end