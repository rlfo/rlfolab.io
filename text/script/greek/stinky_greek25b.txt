begin
background 26
playmusic "bgm/lfo_bliss.ogg"

background_offsetx 1000
add 9007  7 1430 0 310
add 9108  8 1300 0 280               
add 9055 55 1180 0 270
add 9051 51 1140 0 320
add 9154 54 1230 0 310
add 9071 71 1190 0 330              
add 9014 14 1320 0 340
add 9101  1 1360 0 360
add 9103  3 1300 0 390
add 9104  4 1390 0 410
add 9002  2 1240 0 380
add 9105  5 1130 0 400
add 9009  9  930 0 380

camera_pan 1100
addEX 9950 50 1800 0 340 1
faceleft 9950

add 9315 92 1870 0 360
add 9325 92 1730 0 390
add 9335 92 1900 0 400
add 9345 92 1780 0 420
faceleft 9315
faceleft 9335
faceleft 9345
DoAction 9315 733
DoAction 9325 733
DoAction 9335 733
DoAction 9345 733
wait_complete


talkEX 1017 0 0
wait_complete

move 9950 2000 350
talkEX 1017 1 1
wait_complete

runEX 9154 1630 310 1
runEX 9002 1740 350 1
runEX 9104 1790 380 1
talkEX 1017 3 3
wait_complete

move 9154 1570 310
move 9002 1510 380 
move 9104 1560 410 
talkEX 1017 4 4
wait_complete

talkEX 1017 5 5
wait_complete

playmusic "bgm/lfo_sorrow.ogg"
background_offsetx 1350
move 9014 1300 430

move 9007 1320 360
move 9108 1270 380
move 9101 1240 410
move 9103 1200 430
move 9104 1250 460
moveEX 9002 1160 450 0
move 9105 1200 480 

move 9154 1230 360
move 9008 1280 370
move 9051 1150 370
move 9071 1190 380
move 9055 1120 400
move 9107 1340 410
move 9009 1120 470
wait_complete
talkEX 1017 6 10
wait_complete



end