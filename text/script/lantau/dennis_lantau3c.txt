; this file must be in UNICODE format
begin
background 2
playmusic "bgm/lfo_courage.ogg"
; stinky = 9000
add 9002 2 700 0 370
add 9003 3 800 0 370
add 9001 1 1300 0 380
add 9005 5 1300 0 400
faceleft 9003
camera_pos 350

;============== dennis comes back ==================

add 9004 4 200 0 400
add 9501 5009 300 0 400
add 9506 5006 50 0 400
wait_complete
run 9004 595 370
move 9501 585 390
move 9506 550 395
wait_complete
faceleft 9002
talk 528
wait_complete
faceright 9002
faceright 9003
run 9001 920 370
run 9005 990 390
talk 529
wait_complete
faceleft 9002
faceleft 9003
talk 530
wait_complete



end

