; this file must be in UNICODE format
begin
background 19
playmusic "bgm/lfo_sorrow.ogg"
add 9001 3333   1530 0 350
faceleft 9001

;=============== 18 monks appear =================
add 9053 53 700 0 400
add 9153 53 750 0 400
add 9253 53 700 0 400
add 9353 53 700 0 400

add 9453 53 700 0 400
add 9553 53 700 0 400

add 9653 53 700 0 400
add 9753 53 700 0 400
add 9853 53 700 0 400
add 9953 53 700 0 400
add 9063 53 700 0 400

add 9263 53 700 0 400
add 9363 53 700 0 400
            
add 9463 53 700 0 400
add 9563 53 700 0 400
add 9663 53 700 0 400
add 9763 53 700 0 400

add 9863 53 700 0 400

faceleft 9001
faceleft 9002
faceleft 9003
faceleft 9004
faceleft 9506

camera_follow 9001
move 9001 1230 350
wait_complete

playmusic "bgm/lfo_courage.ogg"
camera_pan 550

moveEX 9053 650  250 0
moveEX 9153 710  250 0
moveEX 9253 780  250 0
moveEX 9353 910  250 0
    
moveEX 9453 760  295 0
moveEX 9553 890  295 0
    
moveEX 9653 610  335 0
moveEX 9753 670  335 0
moveEX 9853 740  335 0
moveEX 9953 810  335 0
moveEX 9063 870  335 0
    
moveEX 9263 590  380 0
moveEX 9363 720  380 0
    
moveEX 9463 570  430 0
moveEX 9563 700  430 0
moveEX 9663 770  430 0
moveEX 9763 850  430 0     
    
moveEX 9863 980  350 0
wait_complete

faceright 9053
faceright 9153
faceright 9253
faceright 9353
faceright 9453
faceright 9553
faceright 9653
faceright 9753
faceright 9853
faceright 9953
faceright 9053
faceright 9053
faceright 9163
faceright 9263
faceright 9363
faceright 9463
faceright 9563
faceright 9663
faceright 9763

talk 506
wait_complete
end


