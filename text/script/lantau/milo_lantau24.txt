; this file must be in UNICODE format
begin
background 24
playmusic "bgm/lfo_sorrow.ogg"

add 9002 51 360 0 400
add 9004 55 310 0 430
add 9003 71 370 0 490
add 9001 54 380 0 450

add 9064 64 1700 0 320
add 9014 14 1750 0 350
faceleft 9014
faceleft 9064

camera_follow 9001
move 9002 660 300
move 9004 630 330
move 9003 650 390
move 9001 700 350
wait_complete

;=============== Meet fox =================          
talkEX 812 0 0
wait_complete

camera_pan 1200
talkEX 812 1 1
run 9002 1420 300
run 9004 1380 320
run 9003 1430 390
run 9001 1450 350
wait_complete

;=============== introduce budda ================= 
talkEX 812 2 5
wait_complete

run 9004 1510 310
talkEX 812 6 13
wait_complete

fadein 0 5 
stopMusic
remove 9064
background_offsetx -300
camera_pos 1200
character_pos 9014 1650 320


character_pos 9003 1440 300
character_pos 9002 1370 330
character_pos 9004 1400 370
character_pos 9001 1450 390
wait_complete

playmusic "bgm/lfo_sorrow.ogg"
talkEX 812 14 28
wait_complete


fadein 0 5
character_pos 9002 1350 330
character_pos 9004 1370 370
wait_complete

talkEX 813 0 6
wait_complete

faceleft 9001
faceleft 9003
talkEX 813 7 15
wait_complete

move 9001 1430 370
wait_complete
talkEX 813 16 16
wait_complete

move 9004 1350 370
wait_complete
talkEX 813 17 17
wait_complete

move 9004 1370 370
wait_complete
talkEX 813 18 18
wait_complete

faceright 9001
faceright 9003
talkEX 813 19 20
wait_complete

end


