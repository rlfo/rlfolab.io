; this file must be in UNICODE format
begin
background 1
camera_pos 800
playmusic "bgm/lfo_courage.ogg"

add 9801 3023 2000 0 360
add 9064   64 1900 0 380
add 9803 3023 2000 0 410


add 9023 3023 900 0 360

add 9123 3023 1450 0 350
add 9055 55 1520 0 350
faceleft 9055

add 9223 3023 1300 0 400
add 9054 54 1400 0 400
faceleft 9054

add 9323 3023 1380 0 450
add 9051 51 1460 0 450
faceleft 9051

talkEX 1000 11 11
wait_complete

DoAction 9054 8000
DoAction 9055 8000
DoAction 9051 8100
wait_complete

talkEX 1000 13 14
wait_complete

camera_pan 1000
move 9801 1650 360
move 9064 1600 380
move 9803 1700 410
faceright 9055
faceright 9054
faceright 9051
talkEX 1000 15 16
wait_complete


end