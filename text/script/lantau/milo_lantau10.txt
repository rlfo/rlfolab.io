; this file must be in UNICODE format
begin
background 10
;playmusic "bgm/lfo_sorrow.ogg"
playmusic "bgm/lfo_heart.ogg"

addEX 9014 14 1150 0 300 1
addEX 9064 64 1250 0 380 1
faceleft 9014
faceleft 9064
DoAction 9064 8150


addEX 9004 71 580 0 450 1
addEX 9005 54 600 0 480 1
addEX 9001 51 640 0 510 1
addEX 9003 55 520 0 520 1

camera_pos 610

run 9002 980 355
run 9004 880 340
move 9001 920 390
move 9005 840 370
move 9003 800 410
wait_complete


;=============== meet thunder=================          
talkEX 814 0 2
wait_complete

talkEX 814 5 5
wait_complete

DoAction 9064 0
talkEX 814 6 6
wait_complete

faceright 9014
talkEX 814 7 8
wait_complete

faceleft 9014
talkEX 814 9 10
wait_complete

faceright 9014
talkEX 814 11 12
wait_complete

faceleft 9014
move 9014 1150 400
wait_complete

move 9014 500 500
talkEX 814 13 21
wait_complete

add 9164 64 1250 0 380
add 9264 64 1250 0 380
add 9364 64 1250 0 380
add 9464 64 1250 0 380
add 9564 64 1250 0 380
add 9664 64 1250 0 380
moveEX 9164 1200 340 1
moveEX 9264 1310 340 1
moveEX 9364 1160 380 1
moveEX 9464 1340 380 1
moveEX 9564 1200 420 1
moveEX 9664 1310 420 1
Playsound "SFX/johnnyconfirm.wav" 0 100 0
wait_complete

talkEX 814 22 31
wait_complete
end