; this file must be in UNICODE format
begin
playmusic "bgm/lfo_rage.ogg"
background 6
camera_pos 380 

addex 9001 15   600 0 360   0
addex 9002 195  670 0 390   0

addex 9003 3067 800 0 340   1
addex 9004 3067 920 0 380   1
addex 9005 3092 900 0 320   1
addex 9006 3092 940 0 440   1
addex 9007 3093 810 0 380   1
addex 9008 3093 860 0 410   1
faceleft 9003
faceleft 9004
faceleft 9005
faceleft 9006
faceleft 9007
faceleft 9008
wait_complete
move 9002 700 390
move 9007 760 390
wait_complete
doaction 9002 510
doaction 9007 500
wait_complete
faceright 9007
faceleft 9002
doaction 9002 8200
doaction 9008 210
doaction 9004 210
doaction 9006 210
wait_complete 
run 9001  700 340
run 9006  850 400
wait_complete
faceright 9002
doaction 9002 8200
doaction 9001 8200
doaction 9003 500
doaction 9005 510
wait_complete
camera_pan 500 
talkex 1000 17 17
wait_complete
move 9001  950 350
talkex 1000 18 20
wait_complete
move 9001 1100 350
wait_time 1
faceleft 9001
talkex 1000 21 22
wait_complete
faceright 9001
move 9001 1400 350
move 9002 1350 380
wait_time 1
fadein 0 1
end


