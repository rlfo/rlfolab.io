; this file must be in UNICODE format
begin

background 1
camera_pos 200

playmusic "bgm/lfo_devil4.ogg"

;9000 rova,9001 tobi,9002 meju,9005 stone,9006 bomb,9007 move effect,9051~60 dinosaur,9041~50 lionking,9031~9040 indian

addex 9000 198 776 0 450 0
addex 9002 5028 370 0 440 0
addex 9001 260 586 0 440 0
faceleft 9001
camera_follow 9000


character_pos 9000 640 400
character_pos 9001 720 415
wait_timeex 200
doaction 9001 500
doaction 9000 200
wait_complete
addex 9007 816 660 30 370 0
doaction 9007 440


character_pos 9000 300 500
doaction 9000 0
addex 9008 816 320 30 470 0
doaction 9008 440
wait_complete
runex 9001 450 505 1
wait_complete
doaction 9001 510
addex 9007 816 320 30 470 0
doaction 9007 440



character_pos 9000 750 500
faceleft 9000
addex 9008 816 770 30 470 0
doaction 9008 440
wait_complete
runex 9001 600 505 0
wait_complete
doaction 9001 520
addex 9007 816 770 30 470 0
doaction 9007 440




character_pos 9000 420 460
faceright 9000
addex 9008 816 440 30 430 0
doaction 9008 440
wait_complete
runex 9001 600 450 1
wait_complete


talkex 1000 55 56
wait_complete
moveex 9001 550 450 1
wait_timeex 200
moveex 9002 460 455 0
wait_complete
talkex 1000 57 67
wait_complete
faceleft 9002
talkex 1000 68 75
wait_complete
runex 9000 1100 450 0
runex 9001 1250 440 1
wait_timeex 200
faceright 9002
wait_timeex 1500 
talkex 1000 76 77
wait_complete

end