; this file must be in UNICODE format
begin
background 51
PlayMusic "BGM/lfo_egypt3.ogg"
camera_pos 100

addex 9000 7 240 0 330 0
addex 9001 194 270 0 370 0
addex 9002 74 150 0 340 0

moveex 9000 440 330 0
moveex 9001 470 370 1
moveex 9002 350 340 0
wait_complete

talkex 1000 85 86
wait_complete
camera_pan 250
moveex 9000 640 330 0
moveex 9001 670 370 0
moveex 9002 550 340 0
wait_complete

addex 9500 3037 850 0 250 0
addex 9501 3037 900 0 330 0
addex 9502 3037 840 0 380 0
addex 9503 3037 450 0 260 0
addex 9504 3037 350 0 330 0
addex 9505 3037 440 0 390 0

doaction 9500 30
doaction 9501 30
doaction 9502 30
doaction 9503 30
doaction 9504 30
doaction 9505 30

addex 9100 3050 850 0 250 1
addex 9101 3050 900 0 330 1
addex 9102 3050 840 0 380 1
addex 9103 3050 450 0 260 1
addex 9104 3050 350 0 330 1
addex 9105 3050 440 0 390 1

faceleft 9100
faceleft 9101
faceleft 9102

wait_complete
faceleft 9002
talkex 1000 87 89
wait_complete

end
