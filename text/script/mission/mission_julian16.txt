; this file must be in UNICODE format
begin
background 23

playmusic "bgm/lfo_advent.ogg"
camera_pos 0

;9001 jmother, 9002 jfather ; 9003 2headwolf

background_offsetx -100

addEX 9001 177 200 0 400 1
addEX 9002 178 390 0 390 1
addEX 9003 175 700 0 370 1

Faceleft 9003

camera_pan 100
wait_complete

talkEX 800 66 67
wait_complete

Doaction 9002 8000
wait_time 0.5
RunEX 9003 600 300 1
wait_complete

Doaction 9002 8100
wait_time 0.5

Doaction 9002 710
wait_complete

talkEX 800 68 69
wait_complete

camera_pan 150

MoveEX 9001 600 400 0
MoveEX 9003 630 370 0
wait_complete

talkEX 800 70 72
wait_complete

MoveEX 9001 690 420 0
MoveEX 9003 720 370 0
wait_complete

talkEX 800 73 75
wait_complete

stopmusic
wait_complete

Fadein 0 3
background_offsetx 850
camera_pos 850

;9001 jmother, 9002 jfather, 9003-9009 flower

playmusic "bgm/lfo_normal.ogg"

addEX 9001 177 -800 0 380 1
addEX 9002 178 200 0 390 1
addEX 9003 2131 100 0 320 1
addEX 9004 2131 50 0 280 1
addEX 9005 2131 270 0 240 1
addEX 9006 2131 0 0 370 1
addEX 9007 2131 320 0 430 1
addEX 9008 2131 -90 0 300 1
addEX 9009 2131 -40 0 410 1

Faceleft 9002
Faceleft 9003
Faceleft 9007
Faceleft 9009

Doaction 9002 733
wait_complete

wait_time 2

talkEX 800 76 76
wait_complete

wait_time 1

talkEX 800 77 77
wait_complete

camera_follow 9001

MoveEX 9001 -600 380 0
wait_complete

wait_time 2

camera_follow 9002

talkEX 800 78 80
wait_complete



Fadein 0 3
camera_pos 850

;9001 jmother, 9002 jfather, 9003-9009 flower

background_offsetx 850

addEX 9000 177 -600 0 380 1
addEX 9001 177 -300 0 380 1
addEX 9002 178 200 0 390 1
addEX 9003 2131 200 0 420 1
addEX 9004 2131 50 0 180 1
addEX 9005 2131 170 0 240 1
addEX 9006 2131 100 0 470 1
addEX 9007 2131 320 0 430 1
addEX 9008 2131 390 0 320 1
addEX 9009 2131 40 0 390 1

Faceleft 9002
Faceleft 9003
Faceleft 9007
Faceleft 9009

changeholder 9009 9001

Doaction 9002 733
wait_complete

stopmusic
wait_complete
playmusic "bgm/lfo_delight.ogg"

talkEX 800 109 109
wait_complete

talkEX 800 81 82
wait_complete

MoveEx 9001 0 380 0
wait_complete

changeholder 9009 9000
addEX 9009 2131 40 0 390 1

talkEX 800 83 83
wait_complete

wait_time 1

Doaction 9002 8100
wait_complete

wait_time 1

talkEX 800 84 87
wait_complete

; add fish
addEX 9011 402 100 0 380 1

wait_time 1

MoveEx 9001 -500 380 0
wait_complete

talkEX 800 88 90
wait_complete


Fadein 0 3

remove 9011

background_offsetx 850

addEX 9001 177 30 0 360 1
addEX 9002 178 200 0 400 1
addEX 9003 2131 -50 0 320 1
addEX 9004 2131 150 0 180 1
addEX 9005 2131 70 0 240 1
addEX 9006 2131 180 0 370 1
addEX 9007 2131 220 0 250 1
addEX 9008 2131 350 0 420 1
addEX 9009 2131 140 0 380 1

Faceleft 9002
Faceleft 9004
Faceleft 9008
Faceleft 9003
wait_complete

talkEX 800 110 110
wait_complete

MoveEX 9002 -150 400 0
talkEX 800 91 91
wait_complete

MoveEX 9002 180 400 1
talkEX 800 92 100
wait_complete


camera_pos 850

Fadein 0 3

;9001 jmother, 9002 jfather, 9003-9009 flower

background_offsetx 800

addEX 9001 177 150 0 390 1
addEX 9002 178 00 0 395 1
addEX 9003 2131 50 0 320 1
addEX 9004 2131 150 0 380 1
addEX 9005 2131 310 0 340 1
addEX 9006 2131 -60 0 370 1
addEX 9007 2131 220 0 230 1
addEX 9008 2131 -90 0 200 1
addEX 9009 2131 40 0 420 1

Faceleft 9001
Faceleft 9005
Faceleft 9007
Faceleft 9006
wait_complete

talkEX 800 111 111
wait_complete

talkEX 800 101 107
wait_complete

moveEX 9001 90 390 1
moveEX 9002 60 390 0
wait_complete

talkEX 800 108 108
wait_complete

Fadeout 1 3
wait_complete

end