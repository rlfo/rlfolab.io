; this file must be in UNICODE format
begin
background 21

playmusic "bgm/lfo_lantau1.ogg"
; Jace(194) = 9001, Fox(14) = 9002, Victor(51) = 9003, Ricky(55) = 9004
; Fuku(64) = 9011, Monk(3023) = 9012
; Teleport(4000) = 9099, Table = 9098(201), RoboMice(3102) = 9097

;==== scene 1 ====
camera_pos 50

addEX 9001 194 0 0 380 1
add 9011 64 550 0 280
add 9012 3023 700 0 280

Faceright 9001
Faceright 9011
Faceleft 9012

talkEX 1000 0 1
wait_complete

MoveEX 9001 450 380 0
wait_complete

talkEX 1000 2 5
wait_complete

fadeout 0 1
wait_time 1

remove 9001
remove 9011
remove 9012

;==== scene2 ====
background 22
camera_pos 900

addEX 9001 194 1400 0 380 1
addEX 9002 14 850 0 380 1

Faceright 9001
Faceright 9002

addEX 9098 201 1470 0 382 1
addEX 9097 3102 1470 0 325 1

faceleft 9097

fadein 0 1
wait_complete

MoveEX 9002 1300 380 0
wait_complete

talkEX 1000 6 6
wait_complete

faceleft 9001
talkEX 1000 7 7
wait_complete

fadeout 0 1
wait_time 1

remove 9002
faceright 9001
fadein 0 1
wait_complete

talkEX 1000 8 8
wait_complete

doaction 9001 900
talkEX 1000 9 9
wait_complete

addEX 9003 51 850 0 360 1
addEX 9004 55 800 0 390 1
MoveEX 9003 1300 360 0
MoveEX 9004 1250 390 0
wait_complete

talkEX 1000 10 10
wait_complete

doaction 9001 300
remove 9097
wait_time 1

faceleft 9001
talkEX 1000 11 12
wait_complete

MoveEX 9003 1100 360 1
MoveEX 9004 1050 390 1
RunEx 9001 980 380 0
camera_follow 9001
wait_complete

talkEX 1000 13 16
wait_complete

MoveEX 9001 1000 300 0
wait_complete

MoveEX 9003 1100 305 1
MoveEX 9004 1050 310 1
wait_complete

add 9099 4000 1031 0 240
fadeout 1 2
wait_time 2

remove 9001
remove 9003
remove 9004

;==== scene3 ====
background 0
playmusic "bgm/lfo_advent.ogg"
camera_pos 0

fadein 1 1
addEX 9001 194 270 0 380 1
faceright 9001
doaction 9001 450
wait_complete

talkEX 1000 17 17
wait_complete

fadein 1 1
addEX 9003 51 700 0 370 1
faceleft 9003
doaction 9003 460
doaction 9003 830
wait_complete

talkEX 1000 18 20
wait_complete

fadein 1 1
addEX 9004 55 450 0 355 2
faceright 9004
doaction 9004 450
doaction 9004 350
wait_complete

talkEX 1000 21 21
wait_complete

talkEX 1000 22 23
wait_complete

faceleft 9004
talkEX 1000 24 26
wait_complete

runEX 9004 680 380 1
wait_time 1

doaction 9003 740
wait_complete

faceright 9003
talkEX 1000 27 27
wait_complete

faceright 9003
doaction 9003 8100
doaction 9004 200
wait_time 2
runEX 9004 900 380 0

talkEX 1000 28 28
wait_complete

remove 9004
doaction 9003 8300
wait_complete

runEX 9003 900 380 0
talkEX 1000 29 29
wait_time 1

runEX 9001 900 380 0
wait_complete

end
