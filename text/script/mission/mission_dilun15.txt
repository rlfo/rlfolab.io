; this file must be in UNICODE format
begin

background 25
playmusic "bgm\lfo_normal.ogg"
camera_pos 200

addex 9001  5036  100 0 360 1
addex 9002 3143 100 0 400 1 
addex 9003 5037 700 0 410 1
move 9001 400 360
move 9002 300 400
faceleft 9003
wait_complete
talkex 1000 79 82
wait_complete
move 9001 975 300
wait_complete
remove 9001
wait_complete
talkex 1000 83 83
wait_complete
move 9002 450 400
talkex 1000 84 87
wait_complete 
playmusic "BGM\lfo_usa1.ogg"
move 9002 550 400
wait_complete
talkex 1000 88 97
wait_complete
faceright 9003

wait_complete
talkex 1000 98 99
wait_complete
faceleft 9003
talkex 1000 100 103
wait_complete
fadein 0 1


background 12
camera_pos 50

addex 9010 5037  1000 0 400 1 
addex 9011 3143   1000 0 450 1

move 9010 350 400
move 9011 400 450   
wait_complete
talkex 1000 104 105
wait_complete
move 9010 250 280
move 9011 300 380
wait_complete
remove 9010
remove 9011
wait_complete
fadein 0 1


end

