; this file must be in UNICODE format
;9000(266) zack,9001(269) hector,9002(5067) momogo,9005~9009(5068) soldier
;9010(5069) wakaka,9011~9015(3201) momostarppl,9016~9020(3198) boxing_soldier
;9021() 長老,9022~9025(3200) knife_soldier
;effect
;9050(820->2400) electic,9055~9059(800->2730) bomb,

begin

background 4
playmusic "BGM\lfo_p75s1_2.ogg"
camera_pos 100

addex 9002 5067 55 0 435 0
moveex 9002 525 435 0
addex 9000 266 75 0 420 0
moveex 9000 375 420 0
addex 9001 269 0 0 440 0
moveex 9001 300 440 0

addex 9010 5069 55 0 415 0
moveex 9010 455 415 0

wait_timeex 500
addex 9005 5068 25 0 400 0
addex 9007 5068 40 0 440 0
addex 9009 5068 85 0 420 0

moveex 9005 225 400 0
moveex 9007 140 440 0
moveex 9009 185 420 0

wait_complete
talkex 1000 50 52
wait_complete

doaction 9000 9500
doaction 9010 8000
wait_complete
addex 9050 820 455 -50 415 0
doaction 9050 2400
wait_complete

talkex 1000 53 53
wait_complete

addex 9021 5071 1050 0 420 0
addex 9022 3200 1130 0 380 0
addex 9023 3200 1180 0 400 0
addex 9024 3200 1230 0 420 0

moveex 9021 650 420 1
moveex 9022 730 380 1
moveex 9023 780 400 1
moveex 9024 830 420 1

wait_complete

talkex 1000 54 75
wait_complete

doaction 9000 9520
doaction 9010 0
wait_complete

wait_time 1

addex 9055 800 830 -50 430 0
addex 9056 800 450 -250 350 0
addex 9057 800 200 -100 150 0
addex 9058 800 600 0 250 0

doaction 9055 2730
doaction 9056 2730
doaction 9057 2730
doaction 9058 2730

wait_complete

wait_timeex 800

fadein 1 1
background 20
camera_pos 100

addex 9002 5067 525 0 435 0
addex 9000 266 375 0 420 0
addex 9001 269 300 0 440 0
addex 9010 5069 455 0 415 0
addex 9005 5068 225 0 400 0
addex 9007 5068 140 0 440 0
addex 9009 5068 185 0 420 0

addex 9021 5071 650 0 420 0
addex 9022 3200 730 0 380 0
addex 9023 3200 780 0 400 0
addex 9024 3200 830 0 420 0

faceleft 9021
faceleft 9022
faceleft 9023
faceleft 9024


fadeout 1 1

talkex 1000 76 77
wait_complete

end


