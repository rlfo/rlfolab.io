; this file must be in UNICODE format
begin
background 3

camera_pos 880
PlayMusic "BGM/lfo_egypt3.ogg"
; judo a's
addEX 9600 160 700 0 550  7
faceleft 9600            
addEX 9601 159 250 0 450  7
addEX 9602 60 1030 0 280 7
faceleft 9602

; judo b's 
addEX 9611 173 330 0 450  7
faceleft 9611          
;addEX 9612 61 900 0 420  7
;faceleft 9612          
addEX 9613 153 1050 0 520 7
faceleft 9613

; judo c's
addEX 9621 74 690 0 330 7
faceleft 9621
addEX 9622 152 450 0 330  7
addEX 9623 172 1160 0 260 7

addEX 9531 159 410 0 450 7
faceleft 9531           
addEX 9532 3020 950 0 450 7
addEX 9533 3020 900 0 300 7

addEX 9541 3023 950 0 250  7
addEX 9542 3022 1100 0 480 7
faceleft 9542

addEX 9551 3023 500 0 500 7
addEX 9552 3022 620 0 550 7


;================================
; thievesv
addex 9901 159 1600 0 360 1
addex 9902 151 1000 0 370 1
addex 9903 159 1650 0 380 1
;================================

addex 9100 194 1600 0 385 0
faceleft 9100

; john
addex 9006 7 1280 0 380 0
faceleft 9006


wait_complete

runex 9901 1400 360 1
runex 9903 1430 380 1
runex 9902 1150 370 0
wait_complete

doaction 9006 8000
wait_complete

talkex 1000 31 32
wait_complete

doaction 9902 8100
doaction 9006 8000
wait_complete
doaction 9006 8300
wait_complete

changeteam 9901 0
changeteam 9903 0

talkex 1000 33 33
wait_Complete



moveex 9006 1500 380 0
runex 9903 1100 380 1
runex 9901 1150 360 1
wait_complete
doaction 9902 0
talkex 1000 34 34
wait_complete
runex 9902 1280 380 0
wait_timeex 200
faceright 9901
faceright 9903
wait_complete
changeteam 9903 0
changeteam 9901 0
faceleft 9006
wait_Complete
doaction 9100 8000
wait_complete
talkex 1000 35 35
wait_Complete
doaction 9902 0
wait_complete
runex 9901 700 360 0
runex 9902 700 370 0
runex 9903 700 380 0
wait_timeex 500
faceright 9006
camera_pan 1200
wait_complete
talkex 1000 36 39
wait_complete
doaction 9006 8400
wait_complete
talkex 1000 40 48
wait_complete

moveex 9006 2200 380 0
moveex 9100 2200 385 0

wait_timeex 500

end