; this file must be in UNICODE format
;9000(266) zack,9001(5039) zaba,9005~9009(3067,3092,3093) swat
;9010(4002) bestial_2,9011(4003) bestial_3,9012(4005) bestial_5
;9050~9054(800->730) bomb,9055(815->40) electic,9056(819->1020) transform
;9060(3195) earthwolfboss,9061~9065(3194) earthwolf,9066(5066) kuro_knife

begin
background 3
playMusic "BGM\lfo_p7s5_2.ogg"
camera_pos 1200

addex 9000 266 950 0 420 0
addex 9060 3195 1100 0 405 0

moveex 9000 1400 420 0
moveex 9060 1600 405 0

wait_complete

talkex 1000 79 81
wait_complete

addex 9066 5066 1800 -500 400 1
doaction 9066 8050

wait_complete
wait_timeex 500
talkex 1000 82 84
wait_complete

;addex 9066 5066 1900 0 420 1
moveex 9066 1550 400 1
;wait_timeex 100
move 9060 1500 405 

wait_complete

talkex 1000 85 86
wait_complete


;moveex 9066 1300 420 1
doaction 9066 8000
;wait_timeex 500
doaction 9000 8350
wait_timeex 1000
doaction 9000 0
faceleft 9000
doaction 9000 8100
 
wait_complete 
 
talkex 1000 87 88
wait_complete

faceright 9000
talkex 1000 89 89
wait_complete

wait_complete




end


