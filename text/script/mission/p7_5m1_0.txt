; this file must be in UNICODE format
;9000(266) zack,9001(5039) zaba,9005~9009(3067,3092,3093) swat
;9010(4002) bestial_2,9011(4003) bestial_3,9012(4005) bestial_5,9013(4006) bestial_6
;9050~9054(800->730) bomb,9055(815->40) electic

begin

fadein 0 1
background 99
talkex 1000 1 2
wait_complete
fadeout 0 1
wait_time 1

background 98

playMusic "BGM\lfo_beast_2.ogg"
camera_pos 350

background_offsetx 100

addex 9005 5039 740 0 400 0
addex 9006 3093 920 0 420 1
addex 9007 3092 860 0 440 1
addex 9008 3067 720 0 450 1

addex 9001 3092 660 0 420 1
addex 9000 266 800 0 430 0

addex 9010 4006 0 0 440 0
addex 9012 4002 1160 0 435 0

runex 9010 450 440 0
runex 9012 1000 435 1
doaction 9005 9000
wait_complete
faceleft 9008
faceleft 9001
wait_complete
talkex 1000 3 3
wait_complete
doaction 9012 8000
wait_timeex 1000

;camera_pan 100
faceleft 9000
faceleft 9005
talkex 1000 4 4
addex 9015 4002 1160 0 425 0
runex 9015 1000 425 1

wait_complete
faceright 9012
changeteam 9005 1
faceright 9005
talkex 1000 5 5
wait_complete


doaction 9015 8000
moveex 9012 420 435 0



talkex 1000 6 7
wait_complete
faceright 9015
moveex 9010 550 440 0
talkex 1000 8 8

wait_complete


talkex 1000 9 10
wait_complete 

doaction 9010 8000
doaction 9012 8000
doaction 9015 8000

changeteam 9000 1

doaction 9000 8100
wait_timeex 1300
doaction 9000 8100

wait_complete
faceright 9000

doaction 9000 8000
camera_follow 9000
remove 9005
remove 9006
remove 9007
remove 9008           
remove 9001

wait_complete
;doaction 9000 8350

faceleft 9012
doaction 9012 8000
;doaction 9015 8000
doaction 9010 8100

doaction 9000 8300

wait_timeex 1500
faceleft 9000
doaction 9000 8350

wait_timeex 1000



doaction 9000 0

wait_complete

doaction 9000 8400

wait_complete

doaction 9010 733
doaction 9012 733

wait_complete

talkex 1000 11 11

wait_complete 

addex 9011 4003 1150 0 430 0

runex 9011 850 430 1

camera_pan 300
wait_complete
talkex 1000 12 12
wait_complete
faceright 9000
talkex 1000 13 13
wait_complete

doaction 9011 8000
wait_timeex 500
remove 9000
wait_complete

talkex 1000 14 16

wait_complete

camera_shake 300

addex 9050 800 1000 0 430 0
doaction 9050 730
wait_complete

addex 9051 800 800 -200 400 0
doaction 9051 730
wait_complete

addex 9052 800 550 -200 450 0
doaction 9052 730
wait_complete

addex 9053 800 650 -200 410 0
doaction 9053 730
wait_complete

addex 9054 800 700 -100 440 0
doaction 9054 730
wait_complete

addex 9055 819 830 -150 450 0
doaction 9055 3000
wait_timeex 200
addex 9050 800 1000 0 430 0
doaction 9050 730
wait_complete
wait_timeex 200
addex 9051 800 800 -200 400 0
doaction 9051 730
wait_complete
wait_timeex 200
addex 9052 800 550 -200 450 0
doaction 9052 730
wait_complete
wait_timeex 200
addex 9053 800 650 -200 410 0
doaction 9053 730
wait_complete
wait_timeex 200
addex 9054 800 700 -100 440 0
doaction 9054 730
wait_complete

wait_timeex 2000

fadein 1 3
fadeout 1 3

background 0

playMusic "BGM\lfo_p7s5_2.ogg"
camera_pos 400

addex 9000 266 800 0 420 0
addex 9060 269 1500 0 450 0
doaction 9060 8000
doaction 9000 733

talkex 1000 17 17
wait_complete

doaction 9000 0
wait_complete

talkex 1000 18 18
wait_complete
talkex 1000 19 21
doaction 9000 9000
doaction 9060 8100
wait_complete
doaction 9000 8431
talkex 1000 22 25
doaction 9060 8200
wait_complete


end


