; this file must be in UNICODE format
begin
background 99
camera_pos 200
playmusic "bgm\lfo_devil6.ogg"

talkex 1000 144 144
wait_complete
fadeout 0 1
wait_time 1

background 30
camera_pos 600

addex 9001 266 850 0 400 1
addex 9002 3143 1150 0 400 0
faceleft 9002

fadein 1 1
wait_time 1

talkex 1000 0 0
wait_complete
;move 9001 900 400
wait_complete
talkex 1000 1 2
wait_complete
;move 9001 960 400
talkex 1000 3 3
wait_complete
;move 9002 1100 400
wait_complete
talkex 1000 4 6
wait_complete
run 9001 1000 400
move 9002 1100 400
wait_complete
doaction 9001 500
wait_complete
doaction 9001 510
wait_complete
doaction 9001 8000
talkex 1000 7 7
wait_complete
camera_pan 800
run 9001  1050 400
talkex 1000 8 12
wait_complete
doaction 9002 660
wait_complete
talkex 1000 13 13
wait_complete
doaction 9002 800
talkex 1000 14 15
wait_complete
doaction  9002 840
wait_complete
playmusic "bgm/lfo_p5s6_2.ogg"
move 9002 1170 400
wait_complete
talkex 1000 16 16
wait_complete
faceleft 9001
wait_complete
talkex 1000 17 17
wait_complete

fadeout 0 1
wait_time 1

background 31
camera_pos 100
fadein 0 1
;playmusic "bgm\lfo_normal.ogg"

addex 9003 3143 50 0 250 0
addex 9004 60 1000 0 400 0
addex 9005 60 1100 0 400 0
addex 9006 61 1000 0 450 0
addex 9007 62 1100 0 400 0 
addex 9008 62 1100 0 480 0 
faceleft 9004
faceleft 9005
faceleft 9006
faceleft 9007
faceleft 9008
move 9004 400 350
move 9005 500 400
move 9006 470 420
move 9007 480 340
move 9008 400 450
move 9003 350 400
wait_complete
talkex 1000 18 20
wait_complete
move 9003 400 400
talkex 1000 21 21
wait_complete
move 9003 500 400
move 9004 480 350
move 9005 600 400
move 9006 550 420
move 9007 570 340
move 9008 500 450
addex 9010 266 50 0 350 0
move 9010  250 400
talkex 1000 22 22
wait_complete
faceleft 9003
faceleft 9004
faceleft 9005
faceleft 9006
faceleft 9007
faceleft 9008
wait_complete
talkex 1000 23 23
wait_complete
move 9010 350 400
wait_complete
talkex 1000 24 26
wait_complete
move 9010 400 400
talkex 1000 27 30 
wait_complete

faceright 9003
faceright 9004
faceright 9005
faceright 9006
faceright 9007
faceright 9008
run 9003 1000 400
move 9004 1000 350
move 9005 1000 410
move 9006 1000 420
move 9007 1000 340
move 9008 1000 450
wait_complete
run 9010 600 400
talkex 1000 31 31 
wait_complete

fadeout 0 2
wait_time 2
background 1
camera_pos 600
fadein 0 1
playmusic "bgm\lfo_greek2.ogg"

addex 9001 5037 550 0 430 1
addex 9002 3143 500 0 400 0
move 9001 1050 430
move 9002 900  400
wait_complete
talkex 1000 32 33 
wait_complete
faceleft 9001
talkex 1000 34 35 
wait_complete
move 9002 970 420
talkex 1000 36 37 
wait_complete

fadein 0 1


end










