; this file must be in UNICODE format
begin
background 35
playMusic "BGM\lfo_courage.ogg"
camera_pos 2200

addEX 9001 54 2050 0 335 1
addEX 9002 51 2100 0 350 1
addEX 9003 55 2200 0 380 1
addEX 9004 14 2100 0 450 1
addEX 9005 57 800 0 500 4
addEX 9006 57 650 0 320 4
addEx 9007 57 600 0 380 4
addEX 9008 57 700 0 450 4
faceleft 9001
faceleft 9002
faceleft 9003
faceleft 9004
wait_complete

camera_pan 500
wait_time 1

run 9008 0  450
wait_time 1

camera_pan 2100
wait_time 1

end


