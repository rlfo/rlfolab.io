; this file must be in UNICODE format
begin
background 3

camera_pos 0
PlayMusic "BGM/lfo_egypt3.ogg"
; judo a's
addEX 9600 160 700 0 550  7
faceleft 9600            
addEX 9601 159 250 0 450  7
addEX 9602 60 1030 0 280 7
faceleft 9602

; judo b's 
addEX 9611 173 330 0 450  7
faceleft 9611          
addEX 9612 61 900 0 420  7
faceleft 9612          
addEX 9613 153 1050 0 520 7
faceleft 9613

; judo c's
addEX 9622 152 450 0 330  7
addEX 9623 172 1160 0 260 7

addEX 9531 159 410 0 450 7
faceleft 9531           
addEX 9532 3020 950 0 450 7
addEX 9533 3020 900 0 300 7

addEX 9541 3023 950 0 250  7
addEX 9542 3022 1100 0 480 7
faceleft 9542

addEX 9551 3023 500 0 500 7
addEX 9552 3022 620 0 550 7

;================================
; thievesv
add 9901 159 1150 0 360
add 9902 151 1200 0 370
add 9903 159 1100 0 380
;================================

addex 9100 194 1350 0 370 0
faceleft 9100

; john
add 9006 7 400 0 380

talkex 1000 17 17
wait_complete


;start walking around 
moveex 9006 800 380 0
wait_timeex 600
camera_pan 700

;background character move
move 9612 400 420
move 9532 450 450
move 9622 610 330
move 9551 980 500
wait_complete

talkex 1000 18 23
wait_complete

runex 9901 1400 360 1
runex 9903 1450 380 1
wait_complete

talkex 1000 24 24
runex 9006 1280 380 1
camera_follow 9006
wait_complete
talkex 1000 25 30
wait_complete

end