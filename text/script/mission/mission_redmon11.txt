; this file must be in UNICODE format
begin
background 32
playMusic "BGM\lfo_devil3.ogg"
camera_pos 0

addEX 9001 55 400 0 400 1
addEX 9002 51 150 0 350 1
addEX 9003 54 350 0 365 1
addEX 9004 14 450 0 350 1
addEX 9005 57 100 0 350 2
doaction 9005 733
faceleft 9001
faceleft 9004
faceleft 9002
faceleft 9003
wait_complete

talkEX 1000 41 41
doaction 9002 310
remove 9005
wait_complete

faceright 9002
talkEX 1000 42 42
wait_complete

talkEX 1000 43 45
wait_complete

end


