; this file must be in UNICODE format
begin
background 64

playmusic "bgm/lfo_rage.ogg"
; Bat = 9001, rudolf = 9002
; ninja = 9006-8

camera_pos 700

background_offsetx 400

addEX 9001 17 900 0 390 0

addEX 9002 9 660 0 390 1
addEX 9006 3068 630 0 300 1
addEX 9007 3068 540 0 360 1
addEX 9008 3068 450 0 360 1

Faceleft 9001

talkEX 900 49 50
wait_complete

Doaction 9002 8200
Doaction 9001 200
wait_complete

Doaction 9002 470
wait_complete

talkEX 900 51 51
wait_complete

RunEX 9001 850 390 1
wait_complete

Doaction 9001 8000
wait_complete

wait_time 2

talkEX 900 52 52
wait_complete

Faceleft 9006
Faceleft 9007
Faceleft 9008

talkEX 900 53 53
wait_complete

Doaction 9002 311
wait_complete

camera_pan 200

RunEX 9006 600 300 1
RunEX 9007 510 360 1
RunEX 9008 420 350 1
wait_complete

RunEX 9002 430 390 1
wait_complete

talkEX 900 54 55
wait_complete

RunEX 9006 580 384 0
RunEX 9007 550 382 0
RunEX 9008 480 380 0
wait_complete

Faceright 9002
wait_complete

Doaction 9001 8300
Doaction 9002 200
Doaction 9006 200
Doaction 9007 200
Doaction 9008 200
wait_complete

Doaction 9002 700
Doaction 9006 700
Doaction 9007 700
Doaction 9008 700
wait_complete

camera_pan -200
wait_complete

MoveEX 9001 500 370 1
talkEX 900 56 56
wait_complete

talkEX 900 57 57
wait_complete

MoveEX 9001 800 370 0
wait_complete

Fadeout 1 3
wait_time 3

end

