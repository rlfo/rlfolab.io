; this file must be in UNICODE format
begin
background 25
playmusic "bgm/lfo_rage.ogg"
camera_pos 250

;9001 迪倫 , 9023 K仔 , 9024 9025 狂旺獸

addex 9001 3143 670 0 390 1
addex 9023 5060 630 0 450 1
addex 9024 3144 800 0 380 2
addex 9025 3144 550 0 300 1

faceleft 9024

wait_complete

doaction 9001 500
wait_complete
doaction 9001 510
wait_complete
moveex 9025 650 400 0
doaction 9001 520
wait_complete
doaction 9024 733
moveex 9001 800 400 1
addex 9025 3144 650 0 400 2
wait_complete
doaction 9001 510
wait_complete
doaction 9001 520
wait_complete
doaction 9025 733
wait_complete
wait_timeex 1200
moveex 9001 750 400 1
moveex 9023 710 385 0
wait_complete
talkex 1000 57 58
wait_complete




doaction 9024 0
moveex 9023 770 385 0
faceright 9001
wait_complete




doaction 9024 8200
doaction 9001 200
wait_complete
talkex 1000 59 59

character_pos 9023 350 400

wait_complete
wait_timeex 50
doaction 9001 0
faceleft 9001


talkex 1000 60 60
wait_complete
faceright 9001

talkex 1000 61 61
wait_complete



runex 9001 750 400 0

wait_complete
doaction 9001 8000
wait_complete
doaction 9024 730
wait_complete

end


