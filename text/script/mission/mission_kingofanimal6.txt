; this file must be in UNICODE format
begin

background 6
camera_pos 100

playmusic "bgm/lfo_devil4.ogg"

;9000 rova,9001 tobi,9002 meju,9005 stone,9006 bomb,9051~60 dinosaur,9041~50 lionking,9031~9040 indian

addex 9000 198 50 0 420 0
addex 9001 260 0 0 450 0

moveex 9000 350 420 0
moveex 9001 300 450 0 

wait_complete

talkex 1000 80 81

wait_complete

moveex 9000 645 330 0
moveex 9001 700 360 1
wait_timeex 2000
;doaction 9000 0
wait_complete
talkex 1000 82 91
wait_complete
addex 9041 350 1300 0 400 1
addex 9042 350 1320 0 420 1
addex 9043 350 1360 0 350 1 
moveex 9041 900 400 1
moveex 9042 1020 420 1
moveex 9043 1060 350 1
wait_timeex 100
camera_pan 400
faceright 9001
wait_complete
doaction 9000 0
talkex 9000 92 92
wait_complete
talkex 1000 93 93
wait_complete
end